﻿Imports ClassLibrary 'On importe les méthodes de la partie c# du projet
Imports Qwirkle.Parametre 'On réutilise des variables du formulaire Paramètre
Imports Qwirkle.Fenetre_Menu

Public Class Plateau

    Public pioche As Pioche

    Public cptjoueur As Integer = 0

    Public plateautransi As ClassLibrary.Plateau
    Public plateau As ClassLibrary.Plateau
    Public tuileboug As Tuile
    Public chaine As String

    Public Mainj1 As New MainJ(mainset)
    Public Mainj2 As New MainJ(mainset)
    Public Mainj3 As New MainJ(mainset)
    Public Mainj4 As New MainJ(mainset)
    Public MainDuTour As MainJ

    Public tuiletransi As New Tuile(0, 0, 0, 0)

    Public Joueur1 As Joueur
    Public Joueur2 As Joueur
    Public Joueur3 As Joueur
    Public Joueur4 As Joueur
    Public JoueurDuTour As Joueur

    Public ListJoueur As New List(Of Joueur)

    Private ligne As Integer
    Private colonne As Integer

    Dim ligneEx As Integer
    Dim colonneEx As Integer

    Dim debpartie As New Partie

    'Pour les boutons quitter et de retour au menu on utilise les même méthode que sur le formulaire Fenetre_Menu en rajoutant uniquement la méthode close pour la Fenetre_Menu qui est toujours cachée
    Private Sub Btn_Quitter_Click(sender As Object, e As EventArgs) Handles Btn_Quitter.Click
        Close()
        Fenetre_Menu.Close()
    End Sub

    Private Sub Btn_Menu_Click(sender As Object, e As EventArgs) Handles Btn_Menu.Click
        Fenetre_Menu.Show()
        Close()
    End Sub

    Private Sub Plateau_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim plateautransi As New Plateau
        Dim labo As New Laboratoire
        pioche = New Pioche(labo.CreateAll())

        'ici, en fonction du nombre de joueur, le jeu va attribuer des tuiles aux mains de joueurs et créer chaque joueur. ensuite, le jeu va vérifier
        'quel joueur a le plus de "combo" et va attribuer, en fonction de cela, une priorité à chaque joueur

        'On affiche au minimum le nom et le score de deux joueurs
        Lbl_NomJ1.Text = j1.Getnom()
        Lbl_ScJ1.Text = j1.GetPtJoueur()
        Lbl_NomJ2.Text = j2.Getnom()
        Lbl_ScJ2.Text = j2.GetPtJoueur()
        Pnl_J3.Hide()
        Pnl_J4.Hide()

        If NbreJ = 2 Then
            Mainj1.Recup_tuiles(pioche)
            Mainj2.Recup_tuiles(pioche)
            Joueur1 = New Joueur(0, 0, "", Mainj1.Getlist())
            Joueur2 = New Joueur(0, 0, "", Mainj2.Getlist())
            debpartie.Quicommence2j(Joueur1, Joueur2)
            ListJoueur.Add(Joueur1)
            ListJoueur.Add(Joueur2)
            If Joueur2.GetPriorite() = 1 Then
                ListJoueur(0) = Joueur2
                ListJoueur(1) = Joueur1
            End If
            JoueurDuTour = ListJoueur(0)
            MainDuTour = New MainJ(JoueurDuTour.GetMain())
        End If

        If NbreJ = 3 Then
            Mainj1.Recup_tuiles(pioche)
            Mainj2.Recup_tuiles(pioche)
            Mainj3.Recup_tuiles(pioche)
            Joueur1 = New Joueur(0, 0, "", Mainj1.Getlist())
            Joueur2 = New Joueur(0, 0, "", Mainj2.Getlist())
            Joueur3 = New Joueur(0, 0, "", Mainj3.Getlist())
            Dim ListJ As New List(Of Joueur)
            ListJ.Add(Joueur1)
            ListJ.Add(Joueur2)
            ListJ.Add(Joueur3)
            For Each joueurs As Joueur In ListJ
                If joueurs.GetPriorite() = 1 Then
                    ListJoueur(0) = joueurs
                End If
                If joueurs.GetPriorite() = 2 Then
                    ListJoueur(1) = joueurs
                End If
                If joueurs.GetPriorite() = 3 Then
                    ListJoueur(2) = joueurs
                End If
            Next
            JoueurDuTour = ListJoueur(0)
            MainDuTour.Setlist(JoueurDuTour.GetMain())
        End If

        If NbreJ = 4 Then
            Mainj1.Recup_tuiles(pioche)
            Mainj2.Recup_tuiles(pioche)
            Mainj3.Recup_tuiles(pioche)
            Mainj4.Recup_tuiles(pioche)
            Joueur1 = New Joueur(0, 0, "", Mainj1.Getlist())
            Joueur2 = New Joueur(0, 0, "", Mainj2.Getlist())
            Joueur3 = New Joueur(0, 0, "", Mainj3.Getlist())
            Joueur4 = New Joueur(0, 0, "", Mainj4.Getlist())
            debpartie.Quicommence4j(Joueur1, Joueur2, Joueur3, Joueur4)
            Dim ListJ As New List(Of Joueur)
            ListJ.Add(Joueur1)
            ListJ.Add(Joueur2)
            ListJ.Add(Joueur3)
            ListJ.Add(Joueur4)
            For Each joueurs As Joueur In ListJ
                If joueurs.GetPriorite() = 1 Then
                    ListJoueur(0) = joueurs
                End If
                If joueurs.GetPriorite() = 2 Then
                    ListJoueur(1) = joueurs
                End If
                If joueurs.GetPriorite() = 3 Then
                    ListJoueur(2) = joueurs
                End If
                If joueurs.GetPriorite() = 4 Then
                    ListJoueur(3) = joueurs
                End If
            Next
            JoueurDuTour = ListJoueur(0)
            MainDuTour.Setlist(JoueurDuTour.GetMain())
        End If



        Me.WindowState = FormWindowState.Maximized 'On affiche le formulaire en plein écran pour permmettre au joueur de voir correctement le plateau 
        'On autorise la méthode Drop pour chacune des pictures du GroupBox du plateau ainsi que pour la pioche mais pas pour la main pour éviter au joueur de perdre des tuiles involontairement
        For Each PicBox As PictureBox In GpBx_Plateau.Controls.OfType(Of PictureBox)()
            PicBox.AllowDrop = True
        Next
        PctBx_Pioche.AllowDrop = True
    End Sub

    Private Sub PctBx_TuileMJ1_DragDrop(sender As Object, e As DragEventArgs) Handles PctBx_TuileMJ1.DragDrop, PctBx_TuileMJ2.DragDrop, PctBx_TuileMJ3.DragDrop, PctBx_TuileMJ4.DragDrop, PctBx_TuileMJ5.DragDrop, PctBx_TuileMJ6.DragDrop
        Dim pic As PictureBox = sender
        'On créée ici une boucle if pour vérifier que la cible du drag and drop ne contient pas déjà une image si oui on envoi un message d'erreur et le drag and drop n'as pas lieu
        If pic.Image Is Nothing Then
            pic.Image = e.Data.GetData(DataFormats.Bitmap)
        Else
            MsgBox("Impossible de placer une tuile ici !")
        End If
    End Sub


    Private Sub PicBxPlat1_1_DragDrop(sender As Object, e As DragEventArgs) Handles PicBxPlat1_1.DragDrop, PicBxPlat4_9.DragDrop, PicBxPlat4_8.DragDrop, PicBxPlat4_7.DragDrop, PicBxPlat4_6.DragDrop, PicBxPlat4_5.DragDrop, PicBxPlat4_4.DragDrop, PicBxPlat4_3.DragDrop, PicBxPlat4_20.DragDrop, PicBxPlat4_2.DragDrop, PicBxPlat4_19.DragDrop, PicBxPlat4_18.DragDrop, PicBxPlat4_17.DragDrop, PicBxPlat4_16.DragDrop, PicBxPlat4_15.DragDrop, PicBxPlat4_14.DragDrop, PicBxPlat4_13.DragDrop, PicBxPlat4_12.DragDrop, PicBxPlat4_11.DragDrop, PicBxPlat4_10.DragDrop, PicBxPlat4_1.DragDrop, PicBxPlat3_9.DragDrop, PicBxPlat3_8.DragDrop, PicBxPlat3_7.DragDrop, PicBxPlat3_6.DragDrop, PicBxPlat3_5.DragDrop, PicBxPlat3_4.DragDrop, PicBxPlat3_3.DragDrop, PicBxPlat3_20.DragDrop, PicBxPlat3_2.DragDrop, PicBxPlat3_19.DragDrop, PicBxPlat3_18.DragDrop, PicBxPlat3_17.DragDrop, PicBxPlat3_16.DragDrop, PicBxPlat3_15.DragDrop, PicBxPlat3_14.DragDrop, PicBxPlat3_13.DragDrop, PicBxPlat3_12.DragDrop, PicBxPlat3_11.DragDrop, PicBxPlat3_10.DragDrop, PicBxPlat3_1.DragDrop, PicBxPlat2_9.DragDrop, PicBxPlat2_8.DragDrop, PicBxPlat2_7.DragDrop, PicBxPlat2_6.DragDrop, PicBxPlat2_5.DragDrop, PicBxPlat2_4.DragDrop, PicBxPlat2_3.DragDrop, PicBxPlat2_20.DragDrop, PicBxPlat2_2.DragDrop, PicBxPlat2_19.DragDrop, PicBxPlat2_18.DragDrop, PicBxPlat2_17.DragDrop, PicBxPlat2_16.DragDrop, PicBxPlat2_15.DragDrop, PicBxPlat2_14.DragDrop, PicBxPlat2_13.DragDrop, PicBxPlat2_12.DragDrop, PicBxPlat2_11.DragDrop, PicBxPlat2_10.DragDrop, PicBxPlat2_1.DragDrop, PicBxPlat1_9.DragDrop, PicBxPlat1_8.DragDrop, PicBxPlat1_7.DragDrop, PicBxPlat1_6.DragDrop, PicBxPlat1_5.DragDrop, PicBxPlat1_4.DragDrop, PicBxPlat1_3.DragDrop, PicBxPlat1_20.DragDrop, PicBxPlat1_2.DragDrop, PicBxPlat1_19.DragDrop, PicBxPlat1_18.DragDrop, PicBxPlat1_17.DragDrop, PicBxPlat1_16.DragDrop, PicBxPlat1_15.DragDrop, PicBxPlat1_14.DragDrop, PicBxPlat1_13.DragDrop, PicBxPlat1_12.DragDrop, PicBxPlat1_11.DragDrop, PicBxPlat1_10.DragDrop, PicBxPlat20_9.DragDrop, PicBxPlat20_8.DragDrop, PicBxPlat20_7.DragDrop, PicBxPlat20_6.DragDrop, PicBxPlat20_5.DragDrop, PicBxPlat20_4.DragDrop, PicBxPlat20_3.DragDrop, PicBxPlat20_20.DragDrop, PicBxPlat20_2.DragDrop, PicBxPlat20_19.DragDrop, PicBxPlat20_18.DragDrop, PicBxPlat20_17.DragDrop, PicBxPlat20_16.DragDrop, PicBxPlat20_15.DragDrop, PicBxPlat20_14.DragDrop, PicBxPlat20_13.DragDrop, PicBxPlat20_12.DragDrop, PicBxPlat20_11.DragDrop, PicBxPlat20_10.DragDrop, PicBxPlat20_1.DragDrop, PicBxPlat19_9.DragDrop, PicBxPlat19_8.DragDrop, PicBxPlat19_7.DragDrop, PicBxPlat19_6.DragDrop, PicBxPlat19_5.DragDrop, PicBxPlat19_4.DragDrop, PicBxPlat19_3.DragDrop, PicBxPlat19_20.DragDrop, PicBxPlat19_2.DragDrop, PicBxPlat19_19.DragDrop, PicBxPlat19_18.DragDrop, PicBxPlat19_17.DragDrop, PicBxPlat19_16.DragDrop, PicBxPlat19_15.DragDrop, PicBxPlat19_14.DragDrop, PicBxPlat19_13.DragDrop, PicBxPlat19_12.DragDrop, PicBxPlat19_11.DragDrop, PicBxPlat19_10.DragDrop, PicBxPlat19_1.DragDrop, PicBxPlat18_9.DragDrop, PicBxPlat18_8.DragDrop, PicBxPlat18_7.DragDrop, PicBxPlat18_6.DragDrop, PicBxPlat18_5.DragDrop, PicBxPlat18_4.DragDrop, PicBxPlat18_3.DragDrop, PicBxPlat18_20.DragDrop, PicBxPlat18_2.DragDrop, PicBxPlat18_19.DragDrop, PicBxPlat18_18.DragDrop, PicBxPlat18_17.DragDrop, PicBxPlat18_16.DragDrop, PicBxPlat18_15.DragDrop, PicBxPlat18_14.DragDrop, PicBxPlat18_13.DragDrop, PicBxPlat18_12.DragDrop, PicBxPlat18_11.DragDrop, PicBxPlat18_10.DragDrop, PicBxPlat18_1.DragDrop, PicBxPlat17_9.DragDrop, PicBxPlat17_8.DragDrop, PicBxPlat17_7.DragDrop, PicBxPlat17_6.DragDrop, PicBxPlat17_5.DragDrop, PicBxPlat17_4.DragDrop, PicBxPlat17_3.DragDrop, PicBxPlat17_20.DragDrop, PicBxPlat17_2.DragDrop, PicBxPlat17_19.DragDrop, PicBxPlat17_18.DragDrop, PicBxPlat17_17.DragDrop, PicBxPlat17_16.DragDrop, PicBxPlat17_15.DragDrop, PicBxPlat17_14.DragDrop, PicBxPlat17_13.DragDrop, PicBxPlat17_12.DragDrop, PicBxPlat17_11.DragDrop, PicBxPlat17_10.DragDrop, PicBxPlat17_1.DragDrop, PicBxPlat16_9.DragDrop, PicBxPlat16_8.DragDrop, PicBxPlat16_7.DragDrop, PicBxPlat16_6.DragDrop, PicBxPlat16_5.DragDrop, PicBxPlat16_4.DragDrop, PicBxPlat16_3.DragDrop, PicBxPlat16_20.DragDrop, PicBxPlat16_2.DragDrop, PicBxPlat16_19.DragDrop, PicBxPlat16_18.DragDrop, PicBxPlat16_17.DragDrop, PicBxPlat16_16.DragDrop, PicBxPlat16_15.DragDrop, PicBxPlat16_14.DragDrop, PicBxPlat16_13.DragDrop, PicBxPlat16_12.DragDrop, PicBxPlat16_11.DragDrop, PicBxPlat16_10.DragDrop, PicBxPlat16_1.DragDrop, PicBxPlat15_9.DragDrop, PicBxPlat15_8.DragDrop, PicBxPlat15_7.DragDrop, PicBxPlat15_6.DragDrop, PicBxPlat15_5.DragDrop, PicBxPlat15_4.DragDrop, PicBxPlat15_3.DragDrop, PicBxPlat15_20.DragDrop, PicBxPlat15_2.DragDrop, PicBxPlat15_19.DragDrop, PicBxPlat15_18.DragDrop, PicBxPlat15_17.DragDrop, PicBxPlat15_16.DragDrop, PicBxPlat15_15.DragDrop, PicBxPlat15_14.DragDrop, PicBxPlat15_13.DragDrop, PicBxPlat15_12.DragDrop, PicBxPlat15_11.DragDrop, PicBxPlat15_10.DragDrop, PicBxPlat15_1.DragDrop, PicBxPlat14_9.DragDrop, PicBxPlat14_8.DragDrop, PicBxPlat14_7.DragDrop, PicBxPlat14_6.DragDrop, PicBxPlat14_5.DragDrop, PicBxPlat14_4.DragDrop, PicBxPlat14_3.DragDrop, PicBxPlat14_20.DragDrop, PicBxPlat14_2.DragDrop, PicBxPlat14_19.DragDrop, PicBxPlat14_18.DragDrop, PicBxPlat14_17.DragDrop, PicBxPlat14_16.DragDrop, PicBxPlat14_15.DragDrop, PicBxPlat14_14.DragDrop, PicBxPlat14_13.DragDrop, PicBxPlat14_12.DragDrop, PicBxPlat14_11.DragDrop, PicBxPlat14_10.DragDrop, PicBxPlat14_1.DragDrop, PicBxPlat13_9.DragDrop, PicBxPlat13_8.DragDrop, PicBxPlat13_7.DragDrop, PicBxPlat13_6.DragDrop, PicBxPlat13_5.DragDrop, PicBxPlat13_4.DragDrop, PicBxPlat13_3.DragDrop, PicBxPlat13_20.DragDrop, PicBxPlat13_2.DragDrop, PicBxPlat13_19.DragDrop, PicBxPlat13_18.DragDrop, PicBxPlat13_17.DragDrop, PicBxPlat13_16.DragDrop, PicBxPlat13_15.DragDrop, PicBxPlat13_14.DragDrop, PicBxPlat13_13.DragDrop, PicBxPlat13_12.DragDrop, PicBxPlat13_11.DragDrop, PicBxPlat13_10.DragDrop, PicBxPlat13_1.DragDrop, PicBxPlat12_9.DragDrop, PicBxPlat12_8.DragDrop, PicBxPlat12_7.DragDrop, PicBxPlat12_6.DragDrop, PicBxPlat12_5.DragDrop, PicBxPlat12_4.DragDrop, PicBxPlat12_3.DragDrop, PicBxPlat12_20.DragDrop, PicBxPlat12_2.DragDrop, PicBxPlat12_19.DragDrop, PicBxPlat12_18.DragDrop, PicBxPlat12_17.DragDrop, PicBxPlat12_16.DragDrop, PicBxPlat12_15.DragDrop, PicBxPlat12_14.DragDrop, PicBxPlat12_13.DragDrop, PicBxPlat12_12.DragDrop, PicBxPlat12_11.DragDrop, PicBxPlat12_10.DragDrop, PicBxPlat12_1.DragDrop, PicBxPlat11_9.DragDrop, PicBxPlat11_8.DragDrop, PicBxPlat11_7.DragDrop, PicBxPlat11_6.DragDrop, PicBxPlat11_5.DragDrop, PicBxPlat11_4.DragDrop, PicBxPlat11_3.DragDrop, PicBxPlat11_20.DragDrop, PicBxPlat11_2.DragDrop, PicBxPlat11_19.DragDrop, PicBxPlat11_18.DragDrop, PicBxPlat11_17.DragDrop, PicBxPlat11_16.DragDrop, PicBxPlat11_15.DragDrop, PicBxPlat11_14.DragDrop, PicBxPlat11_13.DragDrop, PicBxPlat11_12.DragDrop, PicBxPlat11_11.DragDrop, PicBxPlat11_10.DragDrop, PicBxPlat11_1.DragDrop, PicBxPlat10_9.DragDrop, PicBxPlat10_8.DragDrop, PicBxPlat10_7.DragDrop, PicBxPlat10_6.DragDrop, PicBxPlat10_5.DragDrop, PicBxPlat10_4.DragDrop, PicBxPlat10_3.DragDrop, PicBxPlat10_20.DragDrop, PicBxPlat10_2.DragDrop, PicBxPlat10_19.DragDrop, PicBxPlat10_18.DragDrop, PicBxPlat10_17.DragDrop, PicBxPlat10_16.DragDrop, PicBxPlat10_15.DragDrop, PicBxPlat10_14.DragDrop, PicBxPlat10_13.DragDrop, PicBxPlat10_12.DragDrop, PicBxPlat10_11.DragDrop, PicBxPlat10_10.DragDrop, PicBxPlat10_1.DragDrop, PicBxPlat9_9.DragDrop, PicBxPlat9_8.DragDrop, PicBxPlat9_7.DragDrop, PicBxPlat9_6.DragDrop, PicBxPlat9_5.DragDrop, PicBxPlat9_4.DragDrop, PicBxPlat9_3.DragDrop, PicBxPlat9_20.DragDrop, PicBxPlat9_2.DragDrop, PicBxPlat9_19.DragDrop, PicBxPlat9_18.DragDrop, PicBxPlat9_17.DragDrop, PicBxPlat9_16.DragDrop, PicBxPlat9_15.DragDrop, PicBxPlat9_14.DragDrop, PicBxPlat9_13.DragDrop, PicBxPlat9_12.DragDrop, PicBxPlat9_11.DragDrop, PicBxPlat9_10.DragDrop, PicBxPlat9_1.DragDrop, PicBxPlat8_9.DragDrop, PicBxPlat8_8.DragDrop, PicBxPlat8_7.DragDrop, PicBxPlat8_6.DragDrop, PicBxPlat8_5.DragDrop, PicBxPlat8_4.DragDrop, PicBxPlat8_3.DragDrop, PicBxPlat8_20.DragDrop, PicBxPlat8_2.DragDrop, PicBxPlat8_19.DragDrop, PicBxPlat8_18.DragDrop, PicBxPlat8_17.DragDrop, PicBxPlat8_16.DragDrop, PicBxPlat8_15.DragDrop, PicBxPlat8_14.DragDrop, PicBxPlat8_13.DragDrop, PicBxPlat8_12.DragDrop, PicBxPlat8_11.DragDrop, PicBxPlat8_10.DragDrop, PicBxPlat8_1.DragDrop, PicBxPlat7_9.DragDrop, PicBxPlat7_8.DragDrop, PicBxPlat7_7.DragDrop, PicBxPlat7_6.DragDrop, PicBxPlat7_5.DragDrop, PicBxPlat7_4.DragDrop, PicBxPlat7_3.DragDrop, PicBxPlat7_20.DragDrop, PicBxPlat7_2.DragDrop, PicBxPlat7_19.DragDrop, PicBxPlat7_18.DragDrop, PicBxPlat7_17.DragDrop, PicBxPlat7_16.DragDrop, PicBxPlat7_15.DragDrop, PicBxPlat7_14.DragDrop, PicBxPlat7_13.DragDrop, PicBxPlat7_12.DragDrop, PicBxPlat7_11.DragDrop, PicBxPlat7_10.DragDrop, PicBxPlat7_1.DragDrop, PicBxPlat6_9.DragDrop, PicBxPlat6_8.DragDrop, PicBxPlat6_7.DragDrop, PicBxPlat6_6.DragDrop, PicBxPlat6_5.DragDrop, PicBxPlat6_4.DragDrop, PicBxPlat6_3.DragDrop, PicBxPlat6_20.DragDrop, PicBxPlat6_2.DragDrop, PicBxPlat6_19.DragDrop, PicBxPlat6_18.DragDrop, PicBxPlat6_17.DragDrop, PicBxPlat6_16.DragDrop, PicBxPlat6_15.DragDrop, PicBxPlat6_14.DragDrop, PicBxPlat6_13.DragDrop, PicBxPlat6_12.DragDrop, PicBxPlat6_11.DragDrop, PicBxPlat6_10.DragDrop, PicBxPlat6_1.DragDrop, PicBxPlat5_9.DragDrop, PicBxPlat5_8.DragDrop, PicBxPlat5_7.DragDrop, PicBxPlat5_6.DragDrop, PicBxPlat5_5.DragDrop, PicBxPlat5_4.DragDrop, PicBxPlat5_3.DragDrop, PicBxPlat5_20.DragDrop, PicBxPlat5_2.DragDrop, PicBxPlat5_19.DragDrop, PicBxPlat5_18.DragDrop, PicBxPlat5_17.DragDrop, PicBxPlat5_16.DragDrop, PicBxPlat5_15.DragDrop, PicBxPlat5_14.DragDrop, PicBxPlat5_13.DragDrop, PicBxPlat5_12.DragDrop, PicBxPlat5_11.DragDrop, PicBxPlat5_10.DragDrop, PicBxPlat5_1.DragDrop, PctBx_Pioche.DragDrop
        Dim pic As PictureBox = sender
        'On créée ici une boucle if pour vérifier que la cible du drag and drop ne contient pas déjà une image si oui on envoi un message d'erreur et le drag and drop n'as pas lieu
        If pic.Image Is Nothing Then
            pic.Image = e.Data.GetData(DataFormats.Bitmap)


        Else
            MsgBox("Impossible de placer une tuile ici !")
        End If
    End Sub

    'Puisque que toutes les Picture box vont avoir le même comportement lors de l'événement drag enter, une seule méthode est suffisante  
    Private Sub PicBxPlat20_20_DragEnter(sender As Object, e As DragEventArgs) Handles PicBxPlat9_9.DragEnter, PicBxPlat9_8.DragEnter, PicBxPlat9_7.DragEnter, PicBxPlat9_6.DragEnter, PicBxPlat9_5.DragEnter, PicBxPlat9_4.DragEnter, PicBxPlat9_3.DragEnter, PicBxPlat9_20.DragEnter, PicBxPlat9_2.DragEnter, PicBxPlat9_19.DragEnter, PicBxPlat9_18.DragEnter, PicBxPlat9_17.DragEnter, PicBxPlat9_16.DragEnter, PicBxPlat9_15.DragEnter, PicBxPlat9_14.DragEnter, PicBxPlat9_13.DragEnter, PicBxPlat9_12.DragEnter, PicBxPlat9_11.DragEnter, PicBxPlat9_10.DragEnter, PicBxPlat9_1.DragEnter, PicBxPlat8_9.DragEnter, PicBxPlat8_8.DragEnter, PicBxPlat8_7.DragEnter, PicBxPlat8_6.DragEnter, PicBxPlat8_5.DragEnter, PicBxPlat8_4.DragEnter, PicBxPlat8_3.DragEnter, PicBxPlat8_20.DragEnter, PicBxPlat8_2.DragEnter, PicBxPlat8_19.DragEnter, PicBxPlat8_18.DragEnter, PicBxPlat8_17.DragEnter, PicBxPlat8_16.DragEnter, PicBxPlat8_15.DragEnter, PicBxPlat8_14.DragEnter, PicBxPlat8_13.DragEnter, PicBxPlat8_12.DragEnter, PicBxPlat8_11.DragEnter, PicBxPlat8_10.DragEnter, PicBxPlat8_1.DragEnter, PicBxPlat7_9.DragEnter, PicBxPlat7_8.DragEnter, PicBxPlat7_7.DragEnter, PicBxPlat7_6.DragEnter, PicBxPlat7_5.DragEnter, PicBxPlat7_4.DragEnter, PicBxPlat7_3.DragEnter, PicBxPlat7_20.DragEnter, PicBxPlat7_2.DragEnter, PicBxPlat7_19.DragEnter, PicBxPlat7_18.DragEnter, PicBxPlat7_17.DragEnter, PicBxPlat7_16.DragEnter, PicBxPlat7_15.DragEnter, PicBxPlat7_14.DragEnter, PicBxPlat7_13.DragEnter, PicBxPlat7_12.DragEnter, PicBxPlat7_11.DragEnter, PicBxPlat7_10.DragEnter, PicBxPlat7_1.DragEnter, PicBxPlat6_9.DragEnter, PicBxPlat6_8.DragEnter, PicBxPlat6_7.DragEnter, PicBxPlat6_6.DragEnter, PicBxPlat6_5.DragEnter, PicBxPlat6_4.DragEnter, PicBxPlat6_3.DragEnter, PicBxPlat6_20.DragEnter, PicBxPlat6_2.DragEnter, PicBxPlat6_19.DragEnter, PicBxPlat6_18.DragEnter, PicBxPlat6_17.DragEnter, PicBxPlat6_16.DragEnter, PicBxPlat6_15.DragEnter, PicBxPlat6_14.DragEnter, PicBxPlat6_13.DragEnter, PicBxPlat6_12.DragEnter, PicBxPlat6_11.DragEnter, PicBxPlat6_10.DragEnter, PicBxPlat6_1.DragEnter, PicBxPlat5_9.DragEnter, PicBxPlat5_8.DragEnter, PicBxPlat5_7.DragEnter, PicBxPlat5_6.DragEnter, PicBxPlat5_5.DragEnter, PicBxPlat5_4.DragEnter, PicBxPlat5_3.DragEnter, PicBxPlat5_20.DragEnter, PicBxPlat5_2.DragEnter, PicBxPlat5_19.DragEnter, PicBxPlat5_18.DragEnter, PicBxPlat5_17.DragEnter, PicBxPlat5_16.DragEnter, PicBxPlat5_15.DragEnter, PicBxPlat5_14.DragEnter, PicBxPlat5_13.DragEnter, PicBxPlat5_12.DragEnter, PicBxPlat5_11.DragEnter, PicBxPlat5_10.DragEnter, PicBxPlat5_1.DragEnter, PicBxPlat4_9.DragEnter, PicBxPlat4_8.DragEnter, PicBxPlat4_7.DragEnter, PicBxPlat4_6.DragEnter, PicBxPlat4_5.DragEnter, PicBxPlat4_4.DragEnter, PicBxPlat4_3.DragEnter, PicBxPlat4_20.DragEnter, PicBxPlat4_2.DragEnter, PicBxPlat4_19.DragEnter, PicBxPlat4_18.DragEnter, PicBxPlat4_17.DragEnter, PicBxPlat4_16.DragEnter, PicBxPlat4_15.DragEnter, PicBxPlat4_14.DragEnter, PicBxPlat4_13.DragEnter, PicBxPlat4_12.DragEnter, PicBxPlat4_11.DragEnter, PicBxPlat4_10.DragEnter, PicBxPlat4_1.DragEnter, PicBxPlat3_9.DragEnter, PicBxPlat3_8.DragEnter, PicBxPlat3_7.DragEnter, PicBxPlat3_6.DragEnter, PicBxPlat3_5.DragEnter, PicBxPlat3_4.DragEnter, PicBxPlat3_3.DragEnter, PicBxPlat3_20.DragEnter, PicBxPlat3_2.DragEnter, PicBxPlat3_19.DragEnter, PicBxPlat3_18.DragEnter, PicBxPlat3_17.DragEnter, PicBxPlat3_16.DragEnter, PicBxPlat3_15.DragEnter, PicBxPlat3_14.DragEnter, PicBxPlat3_13.DragEnter, PicBxPlat3_12.DragEnter, PicBxPlat3_11.DragEnter, PicBxPlat3_10.DragEnter, PicBxPlat3_1.DragEnter, PicBxPlat20_9.DragEnter, PicBxPlat20_8.DragEnter, PicBxPlat20_7.DragEnter, PicBxPlat20_6.DragEnter, PicBxPlat20_5.DragEnter, PicBxPlat20_4.DragEnter, PicBxPlat20_3.DragEnter, PicBxPlat20_20.DragEnter, PicBxPlat20_2.DragEnter, PicBxPlat20_19.DragEnter, PicBxPlat20_18.DragEnter, PicBxPlat20_17.DragEnter, PicBxPlat20_16.DragEnter, PicBxPlat20_15.DragEnter, PicBxPlat20_14.DragEnter, PicBxPlat20_13.DragEnter, PicBxPlat20_12.DragEnter, PicBxPlat20_11.DragEnter, PicBxPlat20_10.DragEnter, PicBxPlat20_1.DragEnter, PicBxPlat2_9.DragEnter, PicBxPlat2_8.DragEnter, PicBxPlat2_7.DragEnter, PicBxPlat2_6.DragEnter, PicBxPlat2_5.DragEnter, PicBxPlat2_4.DragEnter, PicBxPlat2_3.DragEnter, PicBxPlat2_20.DragEnter, PicBxPlat2_2.DragEnter, PicBxPlat2_19.DragEnter, PicBxPlat2_18.DragEnter, PicBxPlat2_17.DragEnter, PicBxPlat2_16.DragEnter, PicBxPlat2_15.DragEnter, PicBxPlat2_14.DragEnter, PicBxPlat2_13.DragEnter, PicBxPlat2_12.DragEnter, PicBxPlat2_11.DragEnter, PicBxPlat2_10.DragEnter, PicBxPlat2_1.DragEnter, PicBxPlat19_9.DragEnter, PicBxPlat19_8.DragEnter, PicBxPlat19_7.DragEnter, PicBxPlat19_6.DragEnter, PicBxPlat19_5.DragEnter, PicBxPlat19_4.DragEnter, PicBxPlat19_3.DragEnter, PicBxPlat19_20.DragEnter, PicBxPlat19_2.DragEnter, PicBxPlat19_19.DragEnter, PicBxPlat19_18.DragEnter, PicBxPlat19_17.DragEnter, PicBxPlat19_16.DragEnter, PicBxPlat19_15.DragEnter, PicBxPlat19_14.DragEnter, PicBxPlat19_13.DragEnter, PicBxPlat19_12.DragEnter, PicBxPlat19_11.DragEnter, PicBxPlat19_10.DragEnter, PicBxPlat19_1.DragEnter, PicBxPlat18_9.DragEnter, PicBxPlat18_8.DragEnter, PicBxPlat18_7.DragEnter, PicBxPlat18_6.DragEnter, PicBxPlat18_5.DragEnter, PicBxPlat18_4.DragEnter, PicBxPlat18_3.DragEnter, PicBxPlat18_20.DragEnter, PicBxPlat18_2.DragEnter, PicBxPlat18_19.DragEnter, PicBxPlat18_18.DragEnter, PicBxPlat18_17.DragEnter, PicBxPlat18_16.DragEnter, PicBxPlat18_15.DragEnter, PicBxPlat18_14.DragEnter, PicBxPlat18_13.DragEnter, PicBxPlat18_12.DragEnter, PicBxPlat18_11.DragEnter, PicBxPlat18_10.DragEnter, PicBxPlat18_1.DragEnter, PicBxPlat17_9.DragEnter, PicBxPlat17_8.DragEnter, PicBxPlat17_7.DragEnter, PicBxPlat17_6.DragEnter, PicBxPlat17_5.DragEnter, PicBxPlat17_4.DragEnter, PicBxPlat17_3.DragEnter, PicBxPlat17_20.DragEnter, PicBxPlat17_2.DragEnter, PicBxPlat17_19.DragEnter, PicBxPlat17_18.DragEnter, PicBxPlat17_17.DragEnter, PicBxPlat17_16.DragEnter, PicBxPlat17_15.DragEnter, PicBxPlat17_14.DragEnter, PicBxPlat17_13.DragEnter, PicBxPlat17_12.DragEnter, PicBxPlat17_11.DragEnter, PicBxPlat17_10.DragEnter, PicBxPlat17_1.DragEnter, PicBxPlat16_9.DragEnter, PicBxPlat16_8.DragEnter, PicBxPlat16_7.DragEnter, PicBxPlat16_6.DragEnter, PicBxPlat16_5.DragEnter, PicBxPlat16_4.DragEnter, PicBxPlat16_3.DragEnter, PicBxPlat16_20.DragEnter, PicBxPlat16_2.DragEnter, PicBxPlat16_19.DragEnter, PicBxPlat16_18.DragEnter, PicBxPlat16_17.DragEnter, PicBxPlat16_16.DragEnter, PicBxPlat16_15.DragEnter, PicBxPlat16_14.DragEnter, PicBxPlat16_13.DragEnter, PicBxPlat16_12.DragEnter, PicBxPlat16_11.DragEnter, PicBxPlat16_10.DragEnter, PicBxPlat16_1.DragEnter, PicBxPlat15_9.DragEnter, PicBxPlat15_8.DragEnter, PicBxPlat15_7.DragEnter, PicBxPlat15_6.DragEnter, PicBxPlat15_5.DragEnter, PicBxPlat15_4.DragEnter, PicBxPlat15_3.DragEnter, PicBxPlat15_20.DragEnter, PicBxPlat15_2.DragEnter, PicBxPlat15_19.DragEnter, PicBxPlat15_18.DragEnter, PicBxPlat15_17.DragEnter, PicBxPlat15_16.DragEnter, PicBxPlat15_15.DragEnter, PicBxPlat15_14.DragEnter, PicBxPlat15_13.DragEnter, PicBxPlat15_12.DragEnter, PicBxPlat15_11.DragEnter, PicBxPlat15_10.DragEnter, PicBxPlat15_1.DragEnter, PicBxPlat14_9.DragEnter, PicBxPlat14_8.DragEnter, PicBxPlat14_7.DragEnter, PicBxPlat14_6.DragEnter, PicBxPlat14_5.DragEnter, PicBxPlat14_4.DragEnter, PicBxPlat14_3.DragEnter, PicBxPlat14_20.DragEnter, PicBxPlat14_2.DragEnter, PicBxPlat14_19.DragEnter, PicBxPlat14_18.DragEnter, PicBxPlat14_17.DragEnter, PicBxPlat14_16.DragEnter, PicBxPlat14_15.DragEnter, PicBxPlat14_14.DragEnter, PicBxPlat14_13.DragEnter, PicBxPlat14_12.DragEnter, PicBxPlat14_11.DragEnter, PicBxPlat14_10.DragEnter, PicBxPlat14_1.DragEnter, PicBxPlat13_9.DragEnter, PicBxPlat13_8.DragEnter, PicBxPlat13_7.DragEnter, PicBxPlat13_6.DragEnter, PicBxPlat13_5.DragEnter, PicBxPlat13_4.DragEnter, PicBxPlat13_3.DragEnter, PicBxPlat13_20.DragEnter, PicBxPlat13_2.DragEnter, PicBxPlat13_19.DragEnter, PicBxPlat13_18.DragEnter, PicBxPlat13_17.DragEnter, PicBxPlat13_16.DragEnter, PicBxPlat13_15.DragEnter, PicBxPlat13_14.DragEnter, PicBxPlat13_13.DragEnter, PicBxPlat13_12.DragEnter, PicBxPlat13_11.DragEnter, PicBxPlat13_10.DragEnter, PicBxPlat13_1.DragEnter, PicBxPlat12_9.DragEnter, PicBxPlat12_8.DragEnter, PicBxPlat12_7.DragEnter, PicBxPlat12_6.DragEnter, PicBxPlat12_5.DragEnter, PicBxPlat12_4.DragEnter, PicBxPlat12_3.DragEnter, PicBxPlat12_20.DragEnter, PicBxPlat12_2.DragEnter, PicBxPlat12_19.DragEnter, PicBxPlat12_18.DragEnter, PicBxPlat12_17.DragEnter, PicBxPlat12_16.DragEnter, PicBxPlat12_15.DragEnter, PicBxPlat12_14.DragEnter, PicBxPlat12_13.DragEnter, PicBxPlat12_12.DragEnter, PicBxPlat12_11.DragEnter, PicBxPlat12_10.DragEnter, PicBxPlat12_1.DragEnter, PicBxPlat11_9.DragEnter, PicBxPlat11_8.DragEnter, PicBxPlat11_7.DragEnter, PicBxPlat11_6.DragEnter, PicBxPlat11_5.DragEnter, PicBxPlat11_4.DragEnter, PicBxPlat11_3.DragEnter, PicBxPlat11_20.DragEnter, PicBxPlat11_2.DragEnter, PicBxPlat11_19.DragEnter, PicBxPlat11_18.DragEnter, PicBxPlat11_17.DragEnter, PicBxPlat11_16.DragEnter, PicBxPlat11_15.DragEnter, PicBxPlat11_14.DragEnter, PicBxPlat11_13.DragEnter, PicBxPlat11_12.DragEnter, PicBxPlat11_11.DragEnter, PicBxPlat11_10.DragEnter, PicBxPlat11_1.DragEnter, PicBxPlat10_9.DragEnter, PicBxPlat10_8.DragEnter, PicBxPlat10_7.DragEnter, PicBxPlat10_6.DragEnter, PicBxPlat10_5.DragEnter, PicBxPlat10_4.DragEnter, PicBxPlat10_3.DragEnter, PicBxPlat10_20.DragEnter, PicBxPlat10_2.DragEnter, PicBxPlat10_19.DragEnter, PicBxPlat10_18.DragEnter, PicBxPlat10_17.DragEnter, PicBxPlat10_16.DragEnter, PicBxPlat10_15.DragEnter, PicBxPlat10_14.DragEnter, PicBxPlat10_13.DragEnter, PicBxPlat10_12.DragEnter, PicBxPlat10_11.DragEnter, PicBxPlat10_10.DragEnter, PicBxPlat10_1.DragEnter, PicBxPlat1_9.DragEnter, PicBxPlat1_8.DragEnter, PicBxPlat1_7.DragEnter, PicBxPlat1_6.DragEnter, PicBxPlat1_5.DragEnter, PicBxPlat1_4.DragEnter, PicBxPlat1_3.DragEnter, PicBxPlat1_20.DragEnter, PicBxPlat1_2.DragEnter, PicBxPlat1_19.DragEnter, PicBxPlat1_18.DragEnter, PicBxPlat1_17.DragEnter, PicBxPlat1_16.DragEnter, PicBxPlat1_15.DragEnter, PicBxPlat1_14.DragEnter, PicBxPlat1_13.DragEnter, PicBxPlat1_12.DragEnter, PicBxPlat1_11.DragEnter, PicBxPlat1_10.DragEnter, PicBxPlat1_1.DragEnter, PctBx_TuileMJ6.DragEnter, PctBx_TuileMJ5.DragEnter, PctBx_TuileMJ4.DragEnter, PctBx_TuileMJ3.DragEnter, PctBx_TuileMJ2.DragEnter, PctBx_TuileMJ1.DragEnter, PctBx_Pioche.DragEnter
        If e.Data.GetDataPresent(DataFormats.Bitmap) = True Then 'on vérifie ici si ce que l'on souhaite déplacer est bien une tuile, si ce n'est pas le cas on annule drag and drop
            e.Effect = DragDropEffects.Move
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    'On sépare l'événements mousemove en deux méthodes pour pouvoir gérer séparement les picture box de la main et du plateau
    Private Sub PctBx_TuileMJ1_MouseMove(sender As Object, e As MouseEventArgs) Handles PctBx_TuileMJ2.MouseMove, PctBx_TuileMJ3.MouseMove, PctBx_TuileMJ4.MouseMove, PctBx_TuileMJ5.MouseMove, PctBx_TuileMJ6.MouseMove
        'On déclare la variable EfReal afin d'éviter de réutiliser DragDropEffects plusieurs fois
        Dim EfReal As DragDropEffects
        Dim pic As PictureBox = sender
        Dim cpt As Integer = 0
        Dim tabnbr(1) As Integer


        chaine = pic.Name.Substring(14)
        Int32.TryParse(chaine, tabnbr(cpt))

        colonne = tabnbr(0)

        If e.Button = MouseButtons.Left AndAlso pic.Image IsNot Nothing Then 'On vérifie que le bouton gauche de la souris a bien été enfoncé
            tuileboug = MainDuTour.GetTuile(colonne) 'On met à jour la partie c# en fonction de la partie visualbasic
            pic.AllowDrop = False
            EfReal = pic.DoDragDrop(pic.Image, DragDropEffects.Move)
            If EfReal = DragDropEffects.Move Then
                pic.Image = Nothing
            End If
            pic.AllowDrop = True 'On autorise le joueur à réplacer la tuile dans la picture box de sa main
        End If
    End Sub

    Private Sub PicBxPlat20_20_MouseMove(sender As Object, e As MouseEventArgs) Handles PicBxPlat9_9.MouseMove, PicBxPlat9_8.MouseMove, PicBxPlat9_7.MouseMove, PicBxPlat9_6.MouseMove, PicBxPlat9_5.MouseMove, PicBxPlat9_4.MouseMove, PicBxPlat9_3.MouseMove, PicBxPlat9_20.MouseMove, PicBxPlat9_2.MouseMove, PicBxPlat9_19.MouseMove, PicBxPlat9_18.MouseMove, PicBxPlat9_17.MouseMove, PicBxPlat9_16.MouseMove, PicBxPlat9_15.MouseMove, PicBxPlat9_14.MouseMove, PicBxPlat9_13.MouseMove, PicBxPlat9_12.MouseMove, PicBxPlat9_11.MouseMove, PicBxPlat9_10.MouseMove, PicBxPlat9_1.MouseMove, PicBxPlat8_9.MouseMove, PicBxPlat8_8.MouseMove, PicBxPlat8_7.MouseMove, PicBxPlat8_6.MouseMove, PicBxPlat8_5.MouseMove, PicBxPlat8_4.MouseMove, PicBxPlat8_3.MouseMove, PicBxPlat8_20.MouseMove, PicBxPlat8_2.MouseMove, PicBxPlat8_19.MouseMove, PicBxPlat8_18.MouseMove, PicBxPlat8_17.MouseMove, PicBxPlat8_16.MouseMove, PicBxPlat8_15.MouseMove, PicBxPlat8_14.MouseMove, PicBxPlat8_13.MouseMove, PicBxPlat8_12.MouseMove, PicBxPlat8_11.MouseMove, PicBxPlat8_10.MouseMove, PicBxPlat8_1.MouseMove, PicBxPlat7_9.MouseMove, PicBxPlat7_8.MouseMove, PicBxPlat7_7.MouseMove, PicBxPlat7_6.MouseMove, PicBxPlat7_5.MouseMove, PicBxPlat7_4.MouseMove, PicBxPlat7_3.MouseMove, PicBxPlat7_20.MouseMove, PicBxPlat7_2.MouseMove, PicBxPlat7_19.MouseMove, PicBxPlat7_18.MouseMove, PicBxPlat7_17.MouseMove, PicBxPlat7_16.MouseMove, PicBxPlat7_15.MouseMove, PicBxPlat7_14.MouseMove, PicBxPlat7_13.MouseMove, PicBxPlat7_12.MouseMove, PicBxPlat7_11.MouseMove, PicBxPlat7_10.MouseMove, PicBxPlat7_1.MouseMove, PicBxPlat6_9.MouseMove, PicBxPlat6_8.MouseMove, PicBxPlat6_7.MouseMove, PicBxPlat6_6.MouseMove, PicBxPlat6_5.MouseMove, PicBxPlat6_4.MouseMove, PicBxPlat6_3.MouseMove, PicBxPlat6_20.MouseMove, PicBxPlat6_2.MouseMove, PicBxPlat6_19.MouseMove, PicBxPlat6_18.MouseMove, PicBxPlat6_17.MouseMove, PicBxPlat6_16.MouseMove, PicBxPlat6_15.MouseMove, PicBxPlat6_14.MouseMove, PicBxPlat6_13.MouseMove, PicBxPlat6_12.MouseMove, PicBxPlat6_11.MouseMove, PicBxPlat6_10.MouseMove, PicBxPlat6_1.MouseMove, PicBxPlat5_9.MouseMove, PicBxPlat5_8.MouseMove, PicBxPlat5_7.MouseMove, PicBxPlat5_6.MouseMove, PicBxPlat5_5.MouseMove, PicBxPlat5_4.MouseMove, PicBxPlat5_3.MouseMove, PicBxPlat5_20.MouseMove, PicBxPlat5_2.MouseMove, PicBxPlat5_19.MouseMove, PicBxPlat5_18.MouseMove, PicBxPlat5_17.MouseMove, PicBxPlat5_16.MouseMove, PicBxPlat5_15.MouseMove, PicBxPlat5_14.MouseMove, PicBxPlat5_13.MouseMove, PicBxPlat5_12.MouseMove, PicBxPlat5_11.MouseMove, PicBxPlat5_10.MouseMove, PicBxPlat5_1.MouseMove, PicBxPlat4_9.MouseMove, PicBxPlat4_8.MouseMove, PicBxPlat4_7.MouseMove, PicBxPlat4_6.MouseMove, PicBxPlat4_5.MouseMove, PicBxPlat4_4.MouseMove, PicBxPlat4_3.MouseMove, PicBxPlat4_20.MouseMove, PicBxPlat4_2.MouseMove, PicBxPlat4_19.MouseMove, PicBxPlat4_18.MouseMove, PicBxPlat4_17.MouseMove, PicBxPlat4_16.MouseMove, PicBxPlat4_15.MouseMove, PicBxPlat4_14.MouseMove, PicBxPlat4_13.MouseMove, PicBxPlat4_12.MouseMove, PicBxPlat4_11.MouseMove, PicBxPlat4_10.MouseMove, PicBxPlat4_1.MouseMove, PicBxPlat3_9.MouseMove, PicBxPlat3_8.MouseMove, PicBxPlat3_7.MouseMove, PicBxPlat3_6.MouseMove, PicBxPlat3_5.MouseMove, PicBxPlat3_4.MouseMove, PicBxPlat3_3.MouseMove, PicBxPlat3_20.MouseMove, PicBxPlat3_2.MouseMove, PicBxPlat3_19.MouseMove, PicBxPlat3_18.MouseMove, PicBxPlat3_17.MouseMove, PicBxPlat3_16.MouseMove, PicBxPlat3_15.MouseMove, PicBxPlat3_14.MouseMove, PicBxPlat3_13.MouseMove, PicBxPlat3_12.MouseMove, PicBxPlat3_11.MouseMove, PicBxPlat3_10.MouseMove, PicBxPlat3_1.MouseMove, PicBxPlat20_9.MouseMove, PicBxPlat20_8.MouseMove, PicBxPlat20_7.MouseMove, PicBxPlat20_6.MouseMove, PicBxPlat20_5.MouseMove, PicBxPlat20_4.MouseMove, PicBxPlat20_3.MouseMove, PicBxPlat20_20.MouseMove, PicBxPlat20_2.MouseMove, PicBxPlat20_19.MouseMove, PicBxPlat20_18.MouseMove, PicBxPlat20_17.MouseMove, PicBxPlat20_16.MouseMove, PicBxPlat20_15.MouseMove, PicBxPlat20_14.MouseMove, PicBxPlat20_13.MouseMove, PicBxPlat20_12.MouseMove, PicBxPlat20_11.MouseMove, PicBxPlat20_10.MouseMove, PicBxPlat20_1.MouseMove, PicBxPlat2_9.MouseMove, PicBxPlat2_8.MouseMove, PicBxPlat2_7.MouseMove, PicBxPlat2_6.MouseMove, PicBxPlat2_5.MouseMove, PicBxPlat2_4.MouseMove, PicBxPlat2_3.MouseMove, PicBxPlat2_20.MouseMove, PicBxPlat2_2.MouseMove, PicBxPlat2_19.MouseMove, PicBxPlat2_18.MouseMove, PicBxPlat2_17.MouseMove, PicBxPlat2_16.MouseMove, PicBxPlat2_15.MouseMove, PicBxPlat2_14.MouseMove, PicBxPlat2_13.MouseMove, PicBxPlat2_12.MouseMove, PicBxPlat2_11.MouseMove, PicBxPlat2_10.MouseMove, PicBxPlat2_1.MouseMove, PicBxPlat19_9.MouseMove, PicBxPlat19_8.MouseMove, PicBxPlat19_7.MouseMove, PicBxPlat19_6.MouseMove, PicBxPlat19_5.MouseMove, PicBxPlat19_4.MouseMove, PicBxPlat19_3.MouseMove, PicBxPlat19_20.MouseMove, PicBxPlat19_2.MouseMove, PicBxPlat19_19.MouseMove, PicBxPlat19_18.MouseMove, PicBxPlat19_17.MouseMove, PicBxPlat19_16.MouseMove, PicBxPlat19_15.MouseMove, PicBxPlat19_14.MouseMove, PicBxPlat19_13.MouseMove, PicBxPlat19_12.MouseMove, PicBxPlat19_11.MouseMove, PicBxPlat19_10.MouseMove, PicBxPlat19_1.MouseMove, PicBxPlat18_9.MouseMove, PicBxPlat18_8.MouseMove, PicBxPlat18_7.MouseMove, PicBxPlat18_6.MouseMove, PicBxPlat18_5.MouseMove, PicBxPlat18_4.MouseMove, PicBxPlat18_3.MouseMove, PicBxPlat18_20.MouseMove, PicBxPlat18_2.MouseMove, PicBxPlat18_19.MouseMove, PicBxPlat18_18.MouseMove, PicBxPlat18_17.MouseMove, PicBxPlat18_16.MouseMove, PicBxPlat18_15.MouseMove, PicBxPlat18_14.MouseMove, PicBxPlat18_13.MouseMove, PicBxPlat18_12.MouseMove, PicBxPlat18_11.MouseMove, PicBxPlat18_10.MouseMove, PicBxPlat18_1.MouseMove, PicBxPlat17_9.MouseMove, PicBxPlat17_8.MouseMove, PicBxPlat17_7.MouseMove, PicBxPlat17_6.MouseMove, PicBxPlat17_5.MouseMove, PicBxPlat17_4.MouseMove, PicBxPlat17_3.MouseMove, PicBxPlat17_20.MouseMove, PicBxPlat17_2.MouseMove, PicBxPlat17_19.MouseMove, PicBxPlat17_18.MouseMove, PicBxPlat17_17.MouseMove, PicBxPlat17_16.MouseMove, PicBxPlat17_15.MouseMove, PicBxPlat17_14.MouseMove, PicBxPlat17_13.MouseMove, PicBxPlat17_12.MouseMove, PicBxPlat17_11.MouseMove, PicBxPlat17_10.MouseMove, PicBxPlat17_1.MouseMove, PicBxPlat16_9.MouseMove, PicBxPlat16_8.MouseMove, PicBxPlat16_7.MouseMove, PicBxPlat16_6.MouseMove, PicBxPlat16_5.MouseMove, PicBxPlat16_4.MouseMove, PicBxPlat16_3.MouseMove, PicBxPlat16_20.MouseMove, PicBxPlat16_2.MouseMove, PicBxPlat16_19.MouseMove, PicBxPlat16_18.MouseMove, PicBxPlat16_17.MouseMove, PicBxPlat16_16.MouseMove, PicBxPlat16_15.MouseMove, PicBxPlat16_14.MouseMove, PicBxPlat16_13.MouseMove, PicBxPlat16_12.MouseMove, PicBxPlat16_11.MouseMove, PicBxPlat16_10.MouseMove, PicBxPlat16_1.MouseMove, PicBxPlat15_9.MouseMove, PicBxPlat15_8.MouseMove, PicBxPlat15_7.MouseMove, PicBxPlat15_6.MouseMove, PicBxPlat15_5.MouseMove, PicBxPlat15_4.MouseMove, PicBxPlat15_3.MouseMove, PicBxPlat15_20.MouseMove, PicBxPlat15_2.MouseMove, PicBxPlat15_19.MouseMove, PicBxPlat15_18.MouseMove, PicBxPlat15_17.MouseMove, PicBxPlat15_16.MouseMove, PicBxPlat15_15.MouseMove, PicBxPlat15_14.MouseMove, PicBxPlat15_13.MouseMove, PicBxPlat15_12.MouseMove, PicBxPlat15_11.MouseMove, PicBxPlat15_10.MouseMove, PicBxPlat15_1.MouseMove, PicBxPlat14_9.MouseMove, PicBxPlat14_8.MouseMove, PicBxPlat14_7.MouseMove, PicBxPlat14_6.MouseMove, PicBxPlat14_5.MouseMove, PicBxPlat14_4.MouseMove, PicBxPlat14_3.MouseMove, PicBxPlat14_20.MouseMove, PicBxPlat14_2.MouseMove, PicBxPlat14_19.MouseMove, PicBxPlat14_18.MouseMove, PicBxPlat14_17.MouseMove, PicBxPlat14_16.MouseMove, PicBxPlat14_15.MouseMove, PicBxPlat14_14.MouseMove, PicBxPlat14_13.MouseMove, PicBxPlat14_12.MouseMove, PicBxPlat14_11.MouseMove, PicBxPlat14_10.MouseMove, PicBxPlat14_1.MouseMove, PicBxPlat13_9.MouseMove, PicBxPlat13_8.MouseMove, PicBxPlat13_7.MouseMove, PicBxPlat13_6.MouseMove, PicBxPlat13_5.MouseMove, PicBxPlat13_4.MouseMove, PicBxPlat13_3.MouseMove, PicBxPlat13_20.MouseMove, PicBxPlat13_2.MouseMove, PicBxPlat13_19.MouseMove, PicBxPlat13_18.MouseMove, PicBxPlat13_17.MouseMove, PicBxPlat13_16.MouseMove, PicBxPlat13_15.MouseMove, PicBxPlat13_14.MouseMove, PicBxPlat13_13.MouseMove, PicBxPlat13_12.MouseMove, PicBxPlat13_11.MouseMove, PicBxPlat13_10.MouseMove, PicBxPlat13_1.MouseMove, PicBxPlat12_9.MouseMove, PicBxPlat12_8.MouseMove, PicBxPlat12_7.MouseMove, PicBxPlat12_6.MouseMove, PicBxPlat12_5.MouseMove, PicBxPlat12_4.MouseMove, PicBxPlat12_3.MouseMove, PicBxPlat12_20.MouseMove, PicBxPlat12_2.MouseMove, PicBxPlat12_19.MouseMove, PicBxPlat12_18.MouseMove, PicBxPlat12_17.MouseMove, PicBxPlat12_16.MouseMove, PicBxPlat12_15.MouseMove, PicBxPlat12_14.MouseMove, PicBxPlat12_13.MouseMove, PicBxPlat12_12.MouseMove, PicBxPlat12_11.MouseMove, PicBxPlat12_10.MouseMove, PicBxPlat12_1.MouseMove, PicBxPlat11_9.MouseMove, PicBxPlat11_8.MouseMove, PicBxPlat11_7.MouseMove, PicBxPlat11_6.MouseMove, PicBxPlat11_5.MouseMove, PicBxPlat11_4.MouseMove, PicBxPlat11_3.MouseMove, PicBxPlat11_20.MouseMove, PicBxPlat11_2.MouseMove, PicBxPlat11_19.MouseMove, PicBxPlat11_18.MouseMove, PicBxPlat11_17.MouseMove, PicBxPlat11_16.MouseMove, PicBxPlat11_15.MouseMove, PicBxPlat11_14.MouseMove, PicBxPlat11_13.MouseMove, PicBxPlat11_12.MouseMove, PicBxPlat11_11.MouseMove, PicBxPlat11_10.MouseMove, PicBxPlat11_1.MouseMove, PicBxPlat10_9.MouseMove, PicBxPlat10_8.MouseMove, PicBxPlat10_7.MouseMove, PicBxPlat10_6.MouseMove, PicBxPlat10_5.MouseMove, PicBxPlat10_4.MouseMove, PicBxPlat10_3.MouseMove, PicBxPlat10_20.MouseMove, PicBxPlat10_2.MouseMove, PicBxPlat10_19.MouseMove, PicBxPlat10_18.MouseMove, PicBxPlat10_17.MouseMove, PicBxPlat10_16.MouseMove, PicBxPlat10_15.MouseMove, PicBxPlat10_14.MouseMove, PicBxPlat10_13.MouseMove, PicBxPlat10_12.MouseMove, PicBxPlat10_11.MouseMove, PicBxPlat10_10.MouseMove, PicBxPlat10_1.MouseMove, PicBxPlat1_9.MouseMove, PicBxPlat1_8.MouseMove, PicBxPlat1_7.MouseMove, PicBxPlat1_6.MouseMove, PicBxPlat1_5.MouseMove, PicBxPlat1_4.MouseMove, PicBxPlat1_3.MouseMove, PicBxPlat1_20.MouseMove, PicBxPlat1_2.MouseMove, PicBxPlat1_19.MouseMove, PicBxPlat1_18.MouseMove, PicBxPlat1_17.MouseMove, PicBxPlat1_16.MouseMove, PicBxPlat1_15.MouseMove, PicBxPlat1_14.MouseMove, PicBxPlat1_13.MouseMove, PicBxPlat1_12.MouseMove, PicBxPlat1_11.MouseMove, PicBxPlat1_10.MouseMove, PicBxPlat1_1.MouseMove, PctBx_Pioche.MouseDown
        'On déclare la variable EfReal afin d'éviter de réutiliser DragDropEffects plusieurs fois
        Dim EfReal As DragDropEffects
        Dim pic As PictureBox = sender

        Dim tabchar As String()
        Dim cpt As Integer = 0

        chaine = pic.Name.Substring(10)
        tabchar = chaine.Split("_") 'On va chercher le nom de l'image pour pouvoir transmettre à la partie c# quel ID à été utilisé.

        Dim tabnbr(1) As Integer

        For Each chara As String In tabchar
            Int32.TryParse(chara, tabnbr(cpt))
            cpt = cpt + 1
        Next

        ligne = tabnbr(0)
        colonne = tabnbr(1)
        If e.Button = MouseButtons.Left AndAlso pic.Image IsNot Nothing Then 'On vérifie que le bouton gauche de la souris a bien été enfoncé
            pic.AllowDrop = False
            EfReal = pic.DoDragDrop(pic.Image, DragDropEffects.Move)
            If EfReal = DragDropEffects.Move Then
                pic.Image = Nothing
            End If
            pic.AllowDrop = True 'On autorise le joueur à modifier son placement de la tuile sur le plateau au cour du tour
        End If
    End Sub

    Private Sub Btn_Valid_Click(sender As Object, e As EventArgs) Handles Btn_Valid.Click
        plateau = plateautransi  'Le plateau de jeu se voit modifié par le "plateautransi"
        MainDuTour.Renvoie_tuiles(pioche) 'si des tuiles se trouvaient dans la liste "TabRenvoie", alors elles seront vriament envoyées vers la pioche et seront mélangées
        MainDuTour.Recup_tuiles(pioche) 'Ici, on récupère les nouvelles tuiles que l'on va mettre dans la main
        JoueurDuTour.SetMain(MainDuTour.Getlist()) 'On refait la main du joueur dont c'est le tour
        ListJoueur(cptjoueur) = JoueurDuTour 'Ici, une mise à jour du joueur qui joue son tour est faite. On y met sa nouvelle main (et ses points)
        cptjoueur = cptjoueur + 1
        'Pour ce qui suit, c'est une vérification de l'avancée dans la liste de joueur : si nous en sommes au dernier joueur, cela va revenir au premier joueur,sinon on avance dans le plateau
        If JoueurDuTour.GetPriorite() = ListJoueur(NbreJ - 1).GetPriorite() Then
            JoueurDuTour = ListJoueur(0)
            cptjoueur = 0
        Else
            JoueurDuTour = ListJoueur(cptjoueur)
        End If
    End Sub
End Class