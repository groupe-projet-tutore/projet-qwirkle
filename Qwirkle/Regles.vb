﻿Public Class Regles
    'Pour les boutons quitter et de retour au menu on utilise les même méthode que sur le formulaire Fenetre_Menu en rajoutant uniquement la méthode close pour la Fenetre_Menu qui est toujours cachée
    Private Sub Btn_Menu_Click(sender As Object, e As EventArgs) Handles Btn_Menu.Click
        Fenetre_Menu.Show()
        Close()
    End Sub

    Private Sub Btn_Quit_Click(sender As Object, e As EventArgs) Handles Btn_Quit.Click
        Close()
        Fenetre_Menu.Close()
    End Sub

    'Afin d'afficher les régles au chargement du formulaire on utilise l'évenement Load sur le formulaire
    Private Sub Regles_Load(sender As Object, e As EventArgs) Handles Me.Load
        'On déclare Fsys et MonFic afin de gagner en lisibilité dans le code ces deux variables vont permettre de prendre le contenu de Regles.txt et de l'inetegrer dans MonFic

        Dim FSys = CreateObject("Scripting.FileSystemObject")
        Dim MonFic = FSys.OpenTextFile("../../Regles.txt", 1)
        Dim Contenu = MonFic.ReadAll
        'On déclare contenu comme étant la totalité du document lu dans MonFic et on affiche ce contenu dans une RichTextBox afin de rendre les règles plus simple à lire pour le joueur
        RichTextBox1.Text = Contenu
    End Sub
End Class