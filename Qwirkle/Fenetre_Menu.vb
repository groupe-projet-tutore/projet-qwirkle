﻿Imports ClassLibrary
Public Class Fenetre_Menu
    Public Shared j1 As Joueur 'On déclare ici j1 en public pour pouvoir l'utliser dans les autres formulaires windows
    Public Shared j2 As Joueur
    Public Shared j3 As Joueur
    Public Shared j4 As Joueur

    Private Sub Btn_Play_Click(sender As Object, e As EventArgs) Handles Btn_play.Click 'Pour afficher le formulaire parametre on utilise la méthode show ainsi que la méthode hide pour cacher le formulaire fenetre_menu et non pas close pour ne pas fermer le programme  
        Parametre.Show()
        Hide() 'En utilisant simplement hide() on cache le formulaire utilisé
    End Sub

    Private Sub Btn_Regles_Click(sender As Object, e As EventArgs) Handles Btn_Regles.Click 'On réutilise les même méthodes que pour le bouton jouer
        Regles.Show()
        Hide()
    End Sub

    Private Sub Btn_Quit_Click(sender As Object, e As EventArgs) Handles Btn_Quit.Click 'On ferme le seul formulaire qui est lancé
        Close()
    End Sub

End Class
