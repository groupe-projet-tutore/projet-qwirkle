﻿Imports ClassLibrary
Imports Qwirkle.Parametre
Imports Qwirkle.Fenetre_Menu

Public Class FinDePartie

    'Au chargement du formulaire on modifie les textes affichés dans les labels pour afficher le nom du gagnant
    Private Sub Plateau_Load(sender As Object, e As EventArgs) Handles Me.Load
        'On affiche au moins les noms de 2 joueurs ainsi que leur scores
        Lbl_NomJ1.Text = j1.Getnom()
        Lbl_ScoreJ1.Text = j1.GetPtJoueur()
        Lbl_NomJ2.Text = j2.Getnom()
        Lbl_ScoreJ2.Text = j2.GetPtJoueur()
        Pnl_FinPartie3j.Hide()
        Pnl_FinPartie4J.Hide()

        'Selon le nombre de joueur selectionnés lors du paramètrage de la partie on affiche 3 ou 4 Joueurs ainsi que leur score final
        If NbreJ = 3 Then
            Pnl_FinPartie3j.Show()
            Lbl_NomJ3.Text = j3.Getnom()
            Lbl_ScoreJ3.Text = j3.GetPtJoueur()
        ElseIf NbreJ = 4 Then
            Pnl_FinPartie3j.Show()
            Pnl_FinPartie4J.Show()
            Lbl_NomJ3.Text = j3.Getnom()
            Lbl_ScoreJ3.Text = j3.GetPtJoueur()
            Lbl_NomJ4.Text = j4.Getnom()
            Lbl_ScoreJ4.Text = j4.GetPtJoueur()
        End If
    End Sub

    'Pour les boutons quitter et de retour au menu on utilise les même méthode que sur le formulaire Fenetre_Menu en rajoutant uniquement la méthode close pour la Fenetre_Menu qui est toujours cachée
    Private Sub Btn_Quitter_Click(sender As Object, e As EventArgs) Handles Btn_Quitter.Click
        Close()
        Fenetre_Menu.Close()
    End Sub

    Private Sub Btn_Menu_Click(sender As Object, e As EventArgs) Handles Btn_Menu.Click
        Hide()
        Fenetre_Menu.Show()
    End Sub
End Class