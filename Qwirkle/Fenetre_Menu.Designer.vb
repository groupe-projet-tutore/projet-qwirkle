﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Fenetre_Menu
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Btn_Regles = New System.Windows.Forms.Button()
        Me.Btn_play = New System.Windows.Forms.Button()
        Me.Btn_Quit = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Btn_Regles
        '
        Me.Btn_Regles.Image = Global.Qwirkle.My.Resources.Resources.Régles_Menu
        Me.Btn_Regles.Location = New System.Drawing.Point(338, 241)
        Me.Btn_Regles.Name = "Btn_Regles"
        Me.Btn_Regles.Size = New System.Drawing.Size(123, 46)
        Me.Btn_Regles.TabIndex = 7
        Me.Btn_Regles.UseVisualStyleBackColor = True
        '
        'Btn_play
        '
        Me.Btn_play.Image = Global.Qwirkle.My.Resources.Resources.BoutonJouer_Menu
        Me.Btn_play.Location = New System.Drawing.Point(338, 189)
        Me.Btn_play.Name = "Btn_play"
        Me.Btn_play.Size = New System.Drawing.Size(123, 46)
        Me.Btn_play.TabIndex = 5
        Me.Btn_play.UseVisualStyleBackColor = True
        '
        'Btn_Quit
        '
        Me.Btn_Quit.Image = Global.Qwirkle.My.Resources.Resources.BoutonQuitter_Menu
        Me.Btn_Quit.Location = New System.Drawing.Point(338, 293)
        Me.Btn_Quit.Name = "Btn_Quit"
        Me.Btn_Quit.Size = New System.Drawing.Size(123, 46)
        Me.Btn_Quit.TabIndex = 3
        Me.Btn_Quit.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = Global.Qwirkle.My.Resources.Resources.LogoQwirkle
        Me.PictureBox1.Location = New System.Drawing.Point(300, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(200, 133)
        Me.PictureBox1.TabIndex = 5
        Me.PictureBox1.TabStop = False
        '
        'Fenetre_Menu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.Qwirkle.My.Resources.Resources.BackgroundQwirkle
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.Btn_Regles)
        Me.Controls.Add(Me.Btn_play)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Btn_Quit)
        Me.Name = "Fenetre_Menu"
        Me.Text = "Form1"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Btn_Quit As Button
    Friend WithEvents Btn_play As Button
    Friend WithEvents Btn_Regles As Button
    Friend WithEvents PictureBox1 As PictureBox
End Class
