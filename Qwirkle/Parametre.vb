﻿Imports ClassLibrary 'On importe les méthodes de la partie c# du projet
Imports Qwirkle.Fenetre_Menu
Imports System.Collections.Generic

Public Class Parametre
    Public Shared NbreJ As Integer 'On déclare la variable NbreJ en tant qu'entier afin de pouvoir donner le nombre de joueurs
    Public Shared mainset As New List(Of Tuile)

    Private Sub Paramètre_Load(sender As Object, e As EventArgs) Handles Me.Load 'On empêche les joueurs d'écrire leurs nom vant d'avoir selectionner le nombre de joueur
        Panel_NomJ.Enabled = False 'On désactive le Panel ce qui empeche les joueurs d'acceder aux text box
    End Sub

    Public Sub Btn_2J_Click(sender As Object, e As EventArgs) Handles Btn_2J.Click 'On utilise l'évenement clic car l'option 2 joueurs est sélectionné par défaut 
        'On active uniquement le panel nomJ pour laisser uniquement 2 joueurs entrer leur noms
        Panel_NomJ.Enabled = True
        'On laisse les text box 3 et 4 désactivée afin d'empecher les joueurs de rentrer trop de nom
        TxtBx_NomJ3.Enabled = False
        TxtBx_NomJ4.Enabled = False
        NbreJ = 2 'On utilise la variable NbreJ afin de de connaitre dans le formulaire Plateau le nombre de nom a afficher
    End Sub

    'On réutilise la même méthode que pour le bouton 2 joueurs et l'adaptant pour 3
    Public Sub Btn_3J_CheckedChanged(sender As Object, e As EventArgs) Handles Btn_3J.CheckedChanged
        Panel_NomJ.Enabled = True
        TxtBx_NomJ3.Enabled = True
        TxtBx_NomJ4.Enabled = False
        NbreJ = 3
    End Sub

    'On réutilise la même méthode que pour le bouton 2 joueurs et l'adaptant pour 4
    Public Sub Btn_4J_CheckedChanged(sender As Object, e As EventArgs) Handles Btn_4J.CheckedChanged
        Panel_NomJ.Enabled = True
        TxtBx_NomJ3.Enabled = True
        TxtBx_NomJ4.Enabled = True
        NbreJ = 4
    End Sub

    Public Sub Btn_play_Click(sender As Object, e As EventArgs) Handles Btn_play.Click
        If NbreJ = 2 Then 'On défini Nbrej2 pouvoir afficher uniquement les label utilisés dans le formulaire plateau 
            'Si un des deux textbox est vide alors un message d'erreur apparait dans le cas contraire on affiche le formulaire plateau 
            If TxtBx_NomJ1.Text = Nothing Or TxtBx_NomJ2.Text = Nothing Then
                MsgBox("Veuillez renseigner tous les noms !")
            Else
                Plateau.Show()
                Close()
            End If
        End If

        'On réutilise les boucles if de la même manière afin de l'adapter pour trois ou quatre joueurs
        If NbreJ = 3 Then
            If TxtBx_NomJ1.Text = Nothing Or TxtBx_NomJ2.Text = Nothing Or TxtBx_NomJ3.Text = Nothing Then

                MsgBox("Veuillez renseigner tous les noms !")
            Else
                Plateau.Show()
                Hide()
            End If
        End If

        If NbreJ = 4 Then
            If TxtBx_NomJ1.Text = Nothing Or TxtBx_NomJ2.Text = Nothing Or TxtBx_NomJ3.Text = Nothing Or TxtBx_NomJ4.Text = Nothing Then

                MsgBox("Veuillez renseigner tous les noms !")
            Else
                Plateau.Show()
                Hide()
            End If
        End If
    End Sub

    'On créer des variables pour chaque joueur qui seront réutilisées dans le formulaire Plateau
    Private Sub TxtBx_NomJ1_TextChanged(sender As Object, e As EventArgs) Handles TxtBx_NomJ1.TextChanged
        Dim liste As New List(Of Tuile)
        j1 = New Joueur(0, 0, TxtBx_NomJ1.Text, liste)
    End Sub

    Private Sub TxtBx_NomJ2_TextChanged(sender As Object, e As EventArgs) Handles TxtBx_NomJ2.TextChanged
        Dim liste As New List(Of Tuile)
        j2 = New Joueur(0, 0, TxtBx_NomJ2.Text, liste)
    End Sub

    Private Sub TxtBx_NomJ3_TextChanged(sender As Object, e As EventArgs) Handles TxtBx_NomJ3.TextChanged
        Dim liste As New List(Of Tuile)
        j3 = New Joueur(0, 0, TxtBx_NomJ3.Text, liste)
    End Sub

    Private Sub TxtBx_NomJ4_TextChanged(sender As Object, e As EventArgs) Handles TxtBx_NomJ4.TextChanged
        Dim liste As New List(Of Tuile)
        j4 = New Joueur(0, 0, TxtBx_NomJ4.Text, liste)
    End Sub

    'On utilise pour retourner au menu les méthode close et show afin de n'afficher qu'un seul formulaire a la fois
    Private Sub Btn_Menu_Click(sender As Object, e As EventArgs) Handles Btn_Menu.Click
        Fenetre_Menu.Show()
        Close()
    End Sub




End Class

