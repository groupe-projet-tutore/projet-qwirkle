﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FinDePartie
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PicBx_FinPartie = New System.Windows.Forms.PictureBox()
        Me.Pnl_FinPartie = New System.Windows.Forms.Panel()
        Me.Btn_Menu = New System.Windows.Forms.Button()
        Me.Btn_Quitter = New System.Windows.Forms.Button()
        Me.Pnl_FinPartie3j = New System.Windows.Forms.Panel()
        Me.Lbl_ScoreJ1 = New System.Windows.Forms.Label()
        Me.Lbl_NomJ1 = New System.Windows.Forms.Label()
        Me.Lbl_Score1 = New System.Windows.Forms.Label()
        Me.Lbl_ScoreJ2 = New System.Windows.Forms.Label()
        Me.Lbl_NomJ2 = New System.Windows.Forms.Label()
        Me.Lbl_Score2 = New System.Windows.Forms.Label()
        Me.Lbl_ScoreJ3 = New System.Windows.Forms.Label()
        Me.Lbl_NomJ3 = New System.Windows.Forms.Label()
        Me.Lbl_Score3 = New System.Windows.Forms.Label()
        Me.Pnl_FinPartie4J = New System.Windows.Forms.Panel()
        Me.Lbl_ScoreJ4 = New System.Windows.Forms.Label()
        Me.Lbl_NomJ4 = New System.Windows.Forms.Label()
        Me.Lbl_Score4 = New System.Windows.Forms.Label()
        CType(Me.PicBx_FinPartie, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Pnl_FinPartie.SuspendLayout()
        Me.Pnl_FinPartie3j.SuspendLayout()
        Me.Pnl_FinPartie4J.SuspendLayout()
        Me.SuspendLayout()
        '
        'PicBx_FinPartie
        '
        Me.PicBx_FinPartie.BackColor = System.Drawing.Color.Transparent
        Me.PicBx_FinPartie.Image = Global.Qwirkle.My.Resources.Resources.LogoQwirkle
        Me.PicBx_FinPartie.Location = New System.Drawing.Point(291, 71)
        Me.PicBx_FinPartie.Name = "PicBx_FinPartie"
        Me.PicBx_FinPartie.Size = New System.Drawing.Size(200, 133)
        Me.PicBx_FinPartie.TabIndex = 6
        Me.PicBx_FinPartie.TabStop = False
        '
        'Pnl_FinPartie
        '
        Me.Pnl_FinPartie.BackColor = System.Drawing.Color.Transparent
        Me.Pnl_FinPartie.Controls.Add(Me.Lbl_ScoreJ2)
        Me.Pnl_FinPartie.Controls.Add(Me.Lbl_NomJ2)
        Me.Pnl_FinPartie.Controls.Add(Me.Lbl_Score2)
        Me.Pnl_FinPartie.Controls.Add(Me.Lbl_ScoreJ1)
        Me.Pnl_FinPartie.Controls.Add(Me.Lbl_NomJ1)
        Me.Pnl_FinPartie.Controls.Add(Me.Lbl_Score1)
        Me.Pnl_FinPartie.Controls.Add(Me.Pnl_FinPartie3j)
        Me.Pnl_FinPartie.Location = New System.Drawing.Point(180, 210)
        Me.Pnl_FinPartie.Name = "Pnl_FinPartie"
        Me.Pnl_FinPartie.Size = New System.Drawing.Size(447, 147)
        Me.Pnl_FinPartie.TabIndex = 10
        '
        'Btn_Menu
        '
        Me.Btn_Menu.Image = Global.Qwirkle.My.Resources.Resources.BoutonMenu_Plateau
        Me.Btn_Menu.Location = New System.Drawing.Point(295, 413)
        Me.Btn_Menu.Name = "Btn_Menu"
        Me.Btn_Menu.Size = New System.Drawing.Size(80, 25)
        Me.Btn_Menu.TabIndex = 12
        Me.Btn_Menu.UseVisualStyleBackColor = True
        '
        'Btn_Quitter
        '
        Me.Btn_Quitter.Image = Global.Qwirkle.My.Resources.Resources.BoutonQuitter_Plateau
        Me.Btn_Quitter.Location = New System.Drawing.Point(411, 413)
        Me.Btn_Quitter.Name = "Btn_Quitter"
        Me.Btn_Quitter.Size = New System.Drawing.Size(80, 25)
        Me.Btn_Quitter.TabIndex = 11
        Me.Btn_Quitter.UseVisualStyleBackColor = True
        '
        'Pnl_FinPartie3j
        '
        Me.Pnl_FinPartie3j.Controls.Add(Me.Pnl_FinPartie4J)
        Me.Pnl_FinPartie3j.Controls.Add(Me.Lbl_ScoreJ3)
        Me.Pnl_FinPartie3j.Controls.Add(Me.Lbl_NomJ3)
        Me.Pnl_FinPartie3j.Controls.Add(Me.Lbl_Score3)
        Me.Pnl_FinPartie3j.Location = New System.Drawing.Point(3, 66)
        Me.Pnl_FinPartie3j.Name = "Pnl_FinPartie3j"
        Me.Pnl_FinPartie3j.Size = New System.Drawing.Size(426, 59)
        Me.Pnl_FinPartie3j.TabIndex = 1
        '
        'Lbl_ScoreJ1
        '
        Me.Lbl_ScoreJ1.AutoSize = True
        Me.Lbl_ScoreJ1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
        Me.Lbl_ScoreJ1.Location = New System.Drawing.Point(376, 15)
        Me.Lbl_ScoreJ1.Name = "Lbl_ScoreJ1"
        Me.Lbl_ScoreJ1.Size = New System.Drawing.Size(20, 24)
        Me.Lbl_ScoreJ1.TabIndex = 6
        Me.Lbl_ScoreJ1.Text = "0"
        '
        'Lbl_NomJ1
        '
        Me.Lbl_NomJ1.AutoSize = True
        Me.Lbl_NomJ1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
        Me.Lbl_NomJ1.Location = New System.Drawing.Point(83, 15)
        Me.Lbl_NomJ1.Name = "Lbl_NomJ1"
        Me.Lbl_NomJ1.Size = New System.Drawing.Size(70, 24)
        Me.Lbl_NomJ1.TabIndex = 5
        Me.Lbl_NomJ1.Text = "NomJ1"
        '
        'Lbl_Score1
        '
        Me.Lbl_Score1.AutoSize = True
        Me.Lbl_Score1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
        Me.Lbl_Score1.Location = New System.Drawing.Point(17, 15)
        Me.Lbl_Score1.Name = "Lbl_Score1"
        Me.Lbl_Score1.Size = New System.Drawing.Size(60, 24)
        Me.Lbl_Score1.TabIndex = 4
        Me.Lbl_Score1.Text = "Score"
        '
        'Lbl_ScoreJ2
        '
        Me.Lbl_ScoreJ2.AutoSize = True
        Me.Lbl_ScoreJ2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
        Me.Lbl_ScoreJ2.Location = New System.Drawing.Point(376, 43)
        Me.Lbl_ScoreJ2.Name = "Lbl_ScoreJ2"
        Me.Lbl_ScoreJ2.Size = New System.Drawing.Size(20, 24)
        Me.Lbl_ScoreJ2.TabIndex = 9
        Me.Lbl_ScoreJ2.Text = "0"
        '
        'Lbl_NomJ2
        '
        Me.Lbl_NomJ2.AutoSize = True
        Me.Lbl_NomJ2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
        Me.Lbl_NomJ2.Location = New System.Drawing.Point(83, 43)
        Me.Lbl_NomJ2.Name = "Lbl_NomJ2"
        Me.Lbl_NomJ2.Size = New System.Drawing.Size(70, 24)
        Me.Lbl_NomJ2.TabIndex = 8
        Me.Lbl_NomJ2.Text = "NomJ2"
        '
        'Lbl_Score2
        '
        Me.Lbl_Score2.AutoSize = True
        Me.Lbl_Score2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
        Me.Lbl_Score2.Location = New System.Drawing.Point(17, 43)
        Me.Lbl_Score2.Name = "Lbl_Score2"
        Me.Lbl_Score2.Size = New System.Drawing.Size(60, 24)
        Me.Lbl_Score2.TabIndex = 7
        Me.Lbl_Score2.Text = "Score"
        '
        'Lbl_ScoreJ3
        '
        Me.Lbl_ScoreJ3.AutoSize = True
        Me.Lbl_ScoreJ3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
        Me.Lbl_ScoreJ3.Location = New System.Drawing.Point(373, 4)
        Me.Lbl_ScoreJ3.Name = "Lbl_ScoreJ3"
        Me.Lbl_ScoreJ3.Size = New System.Drawing.Size(20, 24)
        Me.Lbl_ScoreJ3.TabIndex = 12
        Me.Lbl_ScoreJ3.Text = "0"
        '
        'Lbl_NomJ3
        '
        Me.Lbl_NomJ3.AutoSize = True
        Me.Lbl_NomJ3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
        Me.Lbl_NomJ3.Location = New System.Drawing.Point(80, 4)
        Me.Lbl_NomJ3.Name = "Lbl_NomJ3"
        Me.Lbl_NomJ3.Size = New System.Drawing.Size(70, 24)
        Me.Lbl_NomJ3.TabIndex = 11
        Me.Lbl_NomJ3.Text = "NomJ3"
        '
        'Lbl_Score3
        '
        Me.Lbl_Score3.AutoSize = True
        Me.Lbl_Score3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
        Me.Lbl_Score3.Location = New System.Drawing.Point(14, 4)
        Me.Lbl_Score3.Name = "Lbl_Score3"
        Me.Lbl_Score3.Size = New System.Drawing.Size(60, 24)
        Me.Lbl_Score3.TabIndex = 10
        Me.Lbl_Score3.Text = "Score"
        '
        'Pnl_FinPartie4J
        '
        Me.Pnl_FinPartie4J.BackColor = System.Drawing.Color.Transparent
        Me.Pnl_FinPartie4J.Controls.Add(Me.Lbl_ScoreJ4)
        Me.Pnl_FinPartie4J.Controls.Add(Me.Lbl_NomJ4)
        Me.Pnl_FinPartie4J.Controls.Add(Me.Lbl_Score4)
        Me.Pnl_FinPartie4J.Location = New System.Drawing.Point(3, 30)
        Me.Pnl_FinPartie4J.Name = "Pnl_FinPartie4J"
        Me.Pnl_FinPartie4J.Size = New System.Drawing.Size(420, 26)
        Me.Pnl_FinPartie4J.TabIndex = 13
        '
        'Lbl_ScoreJ4
        '
        Me.Lbl_ScoreJ4.AutoSize = True
        Me.Lbl_ScoreJ4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
        Me.Lbl_ScoreJ4.Location = New System.Drawing.Point(370, 2)
        Me.Lbl_ScoreJ4.Name = "Lbl_ScoreJ4"
        Me.Lbl_ScoreJ4.Size = New System.Drawing.Size(20, 24)
        Me.Lbl_ScoreJ4.TabIndex = 18
        Me.Lbl_ScoreJ4.Text = "0"
        '
        'Lbl_NomJ4
        '
        Me.Lbl_NomJ4.AutoSize = True
        Me.Lbl_NomJ4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
        Me.Lbl_NomJ4.Location = New System.Drawing.Point(77, 2)
        Me.Lbl_NomJ4.Name = "Lbl_NomJ4"
        Me.Lbl_NomJ4.Size = New System.Drawing.Size(70, 24)
        Me.Lbl_NomJ4.TabIndex = 17
        Me.Lbl_NomJ4.Text = "NomJ4"
        '
        'Lbl_Score4
        '
        Me.Lbl_Score4.AutoSize = True
        Me.Lbl_Score4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
        Me.Lbl_Score4.Location = New System.Drawing.Point(11, 2)
        Me.Lbl_Score4.Name = "Lbl_Score4"
        Me.Lbl_Score4.Size = New System.Drawing.Size(60, 24)
        Me.Lbl_Score4.TabIndex = 16
        Me.Lbl_Score4.Text = "Score"
        '
        'FinDePartie
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.Qwirkle.My.Resources.Resources.BackgroundQwirkle
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.Btn_Menu)
        Me.Controls.Add(Me.Btn_Quitter)
        Me.Controls.Add(Me.Pnl_FinPartie)
        Me.Controls.Add(Me.PicBx_FinPartie)
        Me.Name = "FinDePartie"
        Me.Text = "FinDePartie"
        CType(Me.PicBx_FinPartie, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Pnl_FinPartie.ResumeLayout(False)
        Me.Pnl_FinPartie.PerformLayout()
        Me.Pnl_FinPartie3j.ResumeLayout(False)
        Me.Pnl_FinPartie3j.PerformLayout()
        Me.Pnl_FinPartie4J.ResumeLayout(False)
        Me.Pnl_FinPartie4J.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents PicBx_FinPartie As PictureBox
    Friend WithEvents Pnl_FinPartie As Panel
    Friend WithEvents Btn_Menu As Button
    Friend WithEvents Btn_Quitter As Button
    Friend WithEvents Lbl_ScoreJ2 As Label
    Friend WithEvents Lbl_NomJ2 As Label
    Friend WithEvents Lbl_Score2 As Label
    Friend WithEvents Lbl_ScoreJ1 As Label
    Friend WithEvents Lbl_NomJ1 As Label
    Friend WithEvents Lbl_Score1 As Label
    Friend WithEvents Pnl_FinPartie3j As Panel
    Friend WithEvents Lbl_ScoreJ3 As Label
    Friend WithEvents Lbl_NomJ3 As Label
    Friend WithEvents Lbl_Score3 As Label
    Friend WithEvents Pnl_FinPartie4J As Panel
    Friend WithEvents Lbl_ScoreJ4 As Label
    Friend WithEvents Lbl_NomJ4 As Label
    Friend WithEvents Lbl_Score4 As Label
End Class
