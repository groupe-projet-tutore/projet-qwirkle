﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Regles
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.RichTextBox1 = New System.Windows.Forms.RichTextBox()
        Me.Btn_Quit = New System.Windows.Forms.Button()
        Me.Btn_Menu = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'RichTextBox1
        '
        Me.RichTextBox1.Location = New System.Drawing.Point(167, 29)
        Me.RichTextBox1.Name = "RichTextBox1"
        Me.RichTextBox1.Size = New System.Drawing.Size(549, 400)
        Me.RichTextBox1.TabIndex = 2
        Me.RichTextBox1.Text = ""
        '
        'Btn_Quit
        '
        Me.Btn_Quit.Image = Global.Qwirkle.My.Resources.Resources.BoutonQuitter_Régles
        Me.Btn_Quit.Location = New System.Drawing.Point(25, 70)
        Me.Btn_Quit.Name = "Btn_Quit"
        Me.Btn_Quit.Size = New System.Drawing.Size(100, 25)
        Me.Btn_Quit.TabIndex = 1
        Me.Btn_Quit.UseVisualStyleBackColor = True
        '
        'Btn_Menu
        '
        Me.Btn_Menu.Image = Global.Qwirkle.My.Resources.Resources.BoutonRetour_Régles
        Me.Btn_Menu.Location = New System.Drawing.Point(25, 29)
        Me.Btn_Menu.Name = "Btn_Menu"
        Me.Btn_Menu.Size = New System.Drawing.Size(100, 25)
        Me.Btn_Menu.TabIndex = 0
        Me.Btn_Menu.UseVisualStyleBackColor = True
        '
        'Regles
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.Qwirkle.My.Resources.Resources.BackgroundQwirkle
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.RichTextBox1)
        Me.Controls.Add(Me.Btn_Quit)
        Me.Controls.Add(Me.Btn_Menu)
        Me.Name = "Regles"
        Me.Text = "Regles"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Btn_Menu As Button
    Friend WithEvents Btn_Quit As Button
    Friend WithEvents RichTextBox1 As RichTextBox
End Class
