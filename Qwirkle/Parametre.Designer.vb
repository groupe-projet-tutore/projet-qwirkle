﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Parametre
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Gp_selection = New System.Windows.Forms.GroupBox()
        Me.Panel_NomJ = New System.Windows.Forms.Panel()
        Me.TxtBx_NomJ3 = New System.Windows.Forms.TextBox()
        Me.TxtBx_NomJ4 = New System.Windows.Forms.TextBox()
        Me.Btn_play = New System.Windows.Forms.Button()
        Me.TxtBx_NomJ2 = New System.Windows.Forms.TextBox()
        Me.TxtBx_NomJ1 = New System.Windows.Forms.TextBox()
        Me.Lbl_NomJ = New System.Windows.Forms.Label()
        Me.Panel_Select = New System.Windows.Forms.Panel()
        Me.Btn_4J = New System.Windows.Forms.RadioButton()
        Me.Btn_3J = New System.Windows.Forms.RadioButton()
        Me.Btn_2J = New System.Windows.Forms.RadioButton()
        Me.Btn_Menu = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Gp_selection.SuspendLayout()
        Me.Panel_NomJ.SuspendLayout()
        Me.Panel_Select.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Gp_selection
        '
        Me.Gp_selection.BackColor = System.Drawing.Color.Transparent
        Me.Gp_selection.Controls.Add(Me.Panel_NomJ)
        Me.Gp_selection.Controls.Add(Me.Panel_Select)
        Me.Gp_selection.Location = New System.Drawing.Point(160, 125)
        Me.Gp_selection.Name = "Gp_selection"
        Me.Gp_selection.Size = New System.Drawing.Size(500, 346)
        Me.Gp_selection.TabIndex = 5
        Me.Gp_selection.TabStop = False
        '
        'Panel_NomJ
        '
        Me.Panel_NomJ.Controls.Add(Me.TxtBx_NomJ3)
        Me.Panel_NomJ.Controls.Add(Me.TxtBx_NomJ4)
        Me.Panel_NomJ.Controls.Add(Me.Btn_play)
        Me.Panel_NomJ.Controls.Add(Me.TxtBx_NomJ2)
        Me.Panel_NomJ.Controls.Add(Me.TxtBx_NomJ1)
        Me.Panel_NomJ.Controls.Add(Me.Lbl_NomJ)
        Me.Panel_NomJ.Location = New System.Drawing.Point(46, 104)
        Me.Panel_NomJ.Name = "Panel_NomJ"
        Me.Panel_NomJ.Size = New System.Drawing.Size(404, 224)
        Me.Panel_NomJ.TabIndex = 32
        '
        'TxtBx_NomJ3
        '
        Me.TxtBx_NomJ3.Location = New System.Drawing.Point(138, 80)
        Me.TxtBx_NomJ3.Name = "TxtBx_NomJ3"
        Me.TxtBx_NomJ3.Size = New System.Drawing.Size(100, 20)
        Me.TxtBx_NomJ3.TabIndex = 45
        '
        'TxtBx_NomJ4
        '
        Me.TxtBx_NomJ4.Location = New System.Drawing.Point(138, 106)
        Me.TxtBx_NomJ4.Name = "TxtBx_NomJ4"
        Me.TxtBx_NomJ4.Size = New System.Drawing.Size(100, 20)
        Me.TxtBx_NomJ4.TabIndex = 44
        '
        'Btn_play
        '
        Me.Btn_play.Image = Global.Qwirkle.My.Resources.Resources.BoutonJouer_Menu
        Me.Btn_play.Location = New System.Drawing.Point(128, 162)
        Me.Btn_play.Name = "Btn_play"
        Me.Btn_play.Size = New System.Drawing.Size(123, 46)
        Me.Btn_play.TabIndex = 43
        Me.Btn_play.UseVisualStyleBackColor = True
        '
        'TxtBx_NomJ2
        '
        Me.TxtBx_NomJ2.Location = New System.Drawing.Point(138, 54)
        Me.TxtBx_NomJ2.Name = "TxtBx_NomJ2"
        Me.TxtBx_NomJ2.Size = New System.Drawing.Size(100, 20)
        Me.TxtBx_NomJ2.TabIndex = 39
        '
        'TxtBx_NomJ1
        '
        Me.TxtBx_NomJ1.Location = New System.Drawing.Point(138, 28)
        Me.TxtBx_NomJ1.Name = "TxtBx_NomJ1"
        Me.TxtBx_NomJ1.Size = New System.Drawing.Size(100, 20)
        Me.TxtBx_NomJ1.TabIndex = 38
        '
        'Lbl_NomJ
        '
        Me.Lbl_NomJ.AutoSize = True
        Me.Lbl_NomJ.Location = New System.Drawing.Point(146, 4)
        Me.Lbl_NomJ.Name = "Lbl_NomJ"
        Me.Lbl_NomJ.Size = New System.Drawing.Size(92, 13)
        Me.Lbl_NomJ.TabIndex = 40
        Me.Lbl_NomJ.Text = "Nom des joueurs :"
        '
        'Panel_Select
        '
        Me.Panel_Select.Controls.Add(Me.Btn_4J)
        Me.Panel_Select.Controls.Add(Me.Btn_3J)
        Me.Panel_Select.Controls.Add(Me.Btn_2J)
        Me.Panel_Select.Location = New System.Drawing.Point(46, 44)
        Me.Panel_Select.Name = "Panel_Select"
        Me.Panel_Select.Size = New System.Drawing.Size(404, 41)
        Me.Panel_Select.TabIndex = 31
        '
        'Btn_4J
        '
        Me.Btn_4J.Appearance = System.Windows.Forms.Appearance.Button
        Me.Btn_4J.AutoSize = True
        Me.Btn_4J.Image = Global.Qwirkle.My.Resources.Resources.Bouton4J_Paramètre
        Me.Btn_4J.Location = New System.Drawing.Point(299, 9)
        Me.Btn_4J.Name = "Btn_4J"
        Me.Btn_4J.Size = New System.Drawing.Size(69, 29)
        Me.Btn_4J.TabIndex = 21
        Me.Btn_4J.TabStop = True
        Me.Btn_4J.UseVisualStyleBackColor = True
        '
        'Btn_3J
        '
        Me.Btn_3J.Appearance = System.Windows.Forms.Appearance.Button
        Me.Btn_3J.AutoSize = True
        Me.Btn_3J.Image = Global.Qwirkle.My.Resources.Resources.Bouton3J_Paramètre
        Me.Btn_3J.Location = New System.Drawing.Point(159, 9)
        Me.Btn_3J.Name = "Btn_3J"
        Me.Btn_3J.Size = New System.Drawing.Size(69, 29)
        Me.Btn_3J.TabIndex = 20
        Me.Btn_3J.TabStop = True
        Me.Btn_3J.UseVisualStyleBackColor = True
        '
        'Btn_2J
        '
        Me.Btn_2J.Appearance = System.Windows.Forms.Appearance.Button
        Me.Btn_2J.AutoSize = True
        Me.Btn_2J.Image = Global.Qwirkle.My.Resources.Resources.Bouton2J_Paramètre
        Me.Btn_2J.Location = New System.Drawing.Point(34, 9)
        Me.Btn_2J.Name = "Btn_2J"
        Me.Btn_2J.Size = New System.Drawing.Size(69, 29)
        Me.Btn_2J.TabIndex = 19
        Me.Btn_2J.TabStop = True
        Me.Btn_2J.UseVisualStyleBackColor = True
        '
        'Btn_Menu
        '
        Me.Btn_Menu.Image = Global.Qwirkle.My.Resources.Resources.BoutonRetour_Régles
        Me.Btn_Menu.Location = New System.Drawing.Point(39, 29)
        Me.Btn_Menu.Name = "Btn_Menu"
        Me.Btn_Menu.Size = New System.Drawing.Size(100, 25)
        Me.Btn_Menu.TabIndex = 7
        Me.Btn_Menu.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = Global.Qwirkle.My.Resources.Resources.LogoQwirkle
        Me.PictureBox1.Location = New System.Drawing.Point(287, -3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(200, 133)
        Me.PictureBox1.TabIndex = 8
        Me.PictureBox1.TabStop = False
        '
        'Parametre
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.Qwirkle.My.Resources.Resources.BackgroundQwirkle
        Me.ClientSize = New System.Drawing.Size(839, 500)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Btn_Menu)
        Me.Controls.Add(Me.Gp_selection)
        Me.Name = "Parametre"
        Me.Text = "Paramètre"
        Me.Gp_selection.ResumeLayout(False)
        Me.Panel_NomJ.ResumeLayout(False)
        Me.Panel_NomJ.PerformLayout()
        Me.Panel_Select.ResumeLayout(False)
        Me.Panel_Select.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Gp_selection As GroupBox
    Friend WithEvents Panel_Select As Panel
    Friend WithEvents Btn_4J As RadioButton
    Friend WithEvents Btn_3J As RadioButton
    Friend WithEvents Btn_2J As RadioButton
    Friend WithEvents Btn_Menu As Button
    Friend WithEvents Panel_NomJ As Panel
    Friend WithEvents Btn_play As Button
    Friend WithEvents TxtBx_NomJ2 As TextBox
    Friend WithEvents TxtBx_NomJ1 As TextBox
    Friend WithEvents Lbl_NomJ As Label
    Friend WithEvents TxtBx_NomJ3 As TextBox
    Friend WithEvents TxtBx_NomJ4 As TextBox
    Friend WithEvents PictureBox1 As PictureBox
End Class
