﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Plateau
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Gp_MainJ = New System.Windows.Forms.GroupBox()
        Me.PctBx_TuileMJ6 = New System.Windows.Forms.PictureBox()
        Me.PctBx_TuileMJ5 = New System.Windows.Forms.PictureBox()
        Me.PctBx_TuileMJ4 = New System.Windows.Forms.PictureBox()
        Me.PctBx_TuileMJ2 = New System.Windows.Forms.PictureBox()
        Me.PctBx_TuileMJ3 = New System.Windows.Forms.PictureBox()
        Me.PctBx_TuileMJ1 = New System.Windows.Forms.PictureBox()
        Me.Btn_Retour = New System.Windows.Forms.Button()
        Me.Btn_Valid = New System.Windows.Forms.Button()
        Me.GpBx_Plateau = New System.Windows.Forms.GroupBox()
        Me.PicBxPlat20_20 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat20_19 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat20_18 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat20_17 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat20_16 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat20_15 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat20_14 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat20_13 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat20_12 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat20_11 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat20_10 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat20_9 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat20_8 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat20_7 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat20_6 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat20_5 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat20_4 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat20_3 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat20_2 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat20_1 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat19_20 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat19_19 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat19_18 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat19_17 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat19_16 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat19_15 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat19_14 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat19_13 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat19_12 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat19_11 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat19_10 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat19_9 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat19_8 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat19_7 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat19_6 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat19_5 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat19_4 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat19_3 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat19_2 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat19_1 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat18_20 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat18_19 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat18_18 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat18_17 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat18_16 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat18_15 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat18_14 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat18_13 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat18_12 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat18_11 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat18_10 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat18_9 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat18_8 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat18_7 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat18_6 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat18_5 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat18_4 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat18_3 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat18_2 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat18_1 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat17_20 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat17_19 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat17_18 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat17_17 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat17_16 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat17_15 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat17_14 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat17_13 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat17_12 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat17_11 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat17_10 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat17_9 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat17_8 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat17_7 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat17_6 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat17_5 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat17_4 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat17_3 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat17_2 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat17_1 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat16_20 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat16_19 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat16_18 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat16_17 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat16_16 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat16_15 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat16_14 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat16_13 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat16_12 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat16_11 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat16_10 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat16_9 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat16_8 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat16_7 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat16_6 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat16_5 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat16_4 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat16_3 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat16_2 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat16_1 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat15_20 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat15_19 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat15_18 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat15_17 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat15_16 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat15_15 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat15_14 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat15_13 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat15_12 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat15_11 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat15_10 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat15_9 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat15_8 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat15_7 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat15_6 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat15_5 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat15_4 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat15_3 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat15_2 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat15_1 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat14_20 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat14_19 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat14_18 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat14_17 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat14_16 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat14_15 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat14_14 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat14_13 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat14_12 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat14_11 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat14_10 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat14_9 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat14_8 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat14_7 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat14_6 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat14_5 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat14_4 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat14_3 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat14_2 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat14_1 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat13_20 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat13_19 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat13_18 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat13_17 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat13_16 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat13_15 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat13_14 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat13_13 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat13_12 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat13_11 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat13_10 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat13_9 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat13_8 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat13_7 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat13_6 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat13_5 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat13_4 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat13_3 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat13_2 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat13_1 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat12_20 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat12_19 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat12_18 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat12_17 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat12_16 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat12_15 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat12_14 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat12_13 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat12_12 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat12_11 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat12_10 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat12_9 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat12_8 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat12_7 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat12_6 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat12_5 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat12_4 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat12_3 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat12_2 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat12_1 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat11_20 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat11_19 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat11_18 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat11_17 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat11_16 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat11_15 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat11_14 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat11_13 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat11_12 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat11_11 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat11_10 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat11_9 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat11_8 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat11_7 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat11_6 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat11_5 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat11_4 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat11_3 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat11_2 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat11_1 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat10_20 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat10_19 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat10_18 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat10_17 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat10_16 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat10_15 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat10_14 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat10_13 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat10_12 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat10_11 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat10_10 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat10_9 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat10_8 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat10_7 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat10_6 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat10_5 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat10_4 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat10_3 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat10_2 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat10_1 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat9_20 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat9_19 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat9_18 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat9_17 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat9_16 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat9_15 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat9_14 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat9_13 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat9_12 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat9_11 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat9_10 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat9_9 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat9_8 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat9_7 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat9_6 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat9_5 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat9_4 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat9_3 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat9_2 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat9_1 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat8_20 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat8_19 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat8_18 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat8_17 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat8_16 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat8_15 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat8_14 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat8_13 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat8_12 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat8_11 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat8_10 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat8_9 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat8_8 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat8_7 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat8_6 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat8_5 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat8_4 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat8_3 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat8_2 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat8_1 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat7_20 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat7_19 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat7_18 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat7_17 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat7_16 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat7_15 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat7_14 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat7_13 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat7_12 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat7_11 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat7_10 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat7_9 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat7_8 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat7_7 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat7_6 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat7_5 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat7_4 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat7_3 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat7_2 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat7_1 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat6_20 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat6_19 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat6_18 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat6_17 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat6_16 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat6_15 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat6_14 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat6_13 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat6_12 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat6_11 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat6_10 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat6_9 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat6_8 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat6_7 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat6_6 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat6_5 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat6_4 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat6_3 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat6_2 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat6_1 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat5_20 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat5_19 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat5_18 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat5_17 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat5_16 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat5_15 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat5_14 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat5_13 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat5_12 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat5_11 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat5_10 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat5_9 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat5_8 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat5_7 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat5_6 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat5_5 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat5_4 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat5_3 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat5_2 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat5_1 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat4_20 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat4_19 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat4_18 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat4_17 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat4_16 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat4_15 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat4_14 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat4_13 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat4_12 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat4_11 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat4_10 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat4_9 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat4_8 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat4_7 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat4_6 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat4_5 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat4_4 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat4_3 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat4_2 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat4_1 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat3_20 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat3_19 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat3_18 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat3_17 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat3_16 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat3_15 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat3_14 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat3_13 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat3_12 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat3_11 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat3_10 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat3_9 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat3_8 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat3_7 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat3_6 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat3_5 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat3_4 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat3_3 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat3_2 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat3_1 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat2_20 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat2_19 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat2_18 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat2_17 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat2_16 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat2_15 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat2_14 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat2_13 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat2_12 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat2_11 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat2_10 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat2_9 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat2_8 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat2_7 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat2_6 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat2_5 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat2_4 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat2_3 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat2_2 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat2_1 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat1_20 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat1_19 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat1_18 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat1_17 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat1_16 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat1_15 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat1_14 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat1_13 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat1_12 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat1_11 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat1_10 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat1_9 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat1_8 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat1_7 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat1_6 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat1_5 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat1_4 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat1_3 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat1_2 = New System.Windows.Forms.PictureBox()
        Me.PicBxPlat1_1 = New System.Windows.Forms.PictureBox()
        Me.PctBx_Pioche = New System.Windows.Forms.PictureBox()
        Me.BindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Lbl_PionRest = New System.Windows.Forms.Label()
        Me.Lbl_JTour = New System.Windows.Forms.Label()
        Me.Lbl_tourTxt = New System.Windows.Forms.Label()
        Me.Lbl_PionrestTxt = New System.Windows.Forms.Label()
        Me.Lbl_ScJ4 = New System.Windows.Forms.Label()
        Me.Lbl_txtSJ4 = New System.Windows.Forms.Label()
        Me.Lbl_NomJ4 = New System.Windows.Forms.Label()
        Me.Lbl_ScJ3 = New System.Windows.Forms.Label()
        Me.Lbl_txtSJ3 = New System.Windows.Forms.Label()
        Me.Lbl_NomJ3 = New System.Windows.Forms.Label()
        Me.Lbl_ScJ2 = New System.Windows.Forms.Label()
        Me.Lbl_txtSJ2 = New System.Windows.Forms.Label()
        Me.Lbl_NomJ2 = New System.Windows.Forms.Label()
        Me.Lbl_ScJ1 = New System.Windows.Forms.Label()
        Me.Lbl_txtSJ1 = New System.Windows.Forms.Label()
        Me.Lbl_NomJ1 = New System.Windows.Forms.Label()
        Me.Btn_Menu = New System.Windows.Forms.Button()
        Me.Btn_Quitter = New System.Windows.Forms.Button()
        Me.Pnl_J3 = New System.Windows.Forms.Panel()
        Me.Pnl_J4 = New System.Windows.Forms.Panel()
        Me.Gp_MainJ.SuspendLayout()
        CType(Me.PctBx_TuileMJ6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PctBx_TuileMJ5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PctBx_TuileMJ4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PctBx_TuileMJ2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PctBx_TuileMJ3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PctBx_TuileMJ1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GpBx_Plateau.SuspendLayout()
        CType(Me.PicBxPlat20_20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat20_19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat20_18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat20_17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat20_16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat20_15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat20_14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat20_13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat20_12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat20_11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat20_10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat20_9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat20_8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat20_7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat20_6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat20_5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat20_4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat20_3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat20_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat20_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat19_20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat19_19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat19_18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat19_17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat19_16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat19_15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat19_14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat19_13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat19_12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat19_11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat19_10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat19_9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat19_8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat19_7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat19_6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat19_5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat19_4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat19_3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat19_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat19_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat18_20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat18_19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat18_18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat18_17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat18_16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat18_15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat18_14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat18_13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat18_12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat18_11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat18_10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat18_9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat18_8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat18_7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat18_6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat18_5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat18_4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat18_3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat18_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat18_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat17_20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat17_19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat17_18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat17_17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat17_16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat17_15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat17_14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat17_13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat17_12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat17_11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat17_10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat17_9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat17_8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat17_7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat17_6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat17_5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat17_4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat17_3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat17_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat17_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat16_20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat16_19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat16_18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat16_17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat16_16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat16_15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat16_14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat16_13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat16_12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat16_11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat16_10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat16_9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat16_8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat16_7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat16_6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat16_5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat16_4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat16_3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat16_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat16_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat15_20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat15_19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat15_18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat15_17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat15_16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat15_15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat15_14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat15_13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat15_12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat15_11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat15_10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat15_9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat15_8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat15_7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat15_6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat15_5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat15_4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat15_3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat15_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat15_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat14_20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat14_19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat14_18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat14_17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat14_16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat14_15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat14_14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat14_13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat14_12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat14_11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat14_10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat14_9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat14_8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat14_7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat14_6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat14_5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat14_4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat14_3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat14_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat14_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat13_20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat13_19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat13_18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat13_17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat13_16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat13_15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat13_14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat13_13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat13_12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat13_11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat13_10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat13_9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat13_8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat13_7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat13_6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat13_5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat13_4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat13_3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat13_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat13_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat12_20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat12_19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat12_18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat12_17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat12_16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat12_15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat12_14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat12_13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat12_12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat12_11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat12_10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat12_9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat12_8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat12_7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat12_6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat12_5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat12_4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat12_3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat12_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat12_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat11_20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat11_19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat11_18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat11_17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat11_16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat11_15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat11_14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat11_13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat11_12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat11_11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat11_10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat11_9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat11_8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat11_7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat11_6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat11_5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat11_4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat11_3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat11_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat11_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat10_20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat10_19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat10_18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat10_17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat10_16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat10_15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat10_14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat10_13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat10_12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat10_11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat10_10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat10_9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat10_8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat10_7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat10_6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat10_5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat10_4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat10_3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat10_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat10_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat9_20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat9_19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat9_18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat9_17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat9_16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat9_15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat9_14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat9_13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat9_12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat9_11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat9_10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat9_9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat9_8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat9_7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat9_6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat9_5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat9_4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat9_3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat9_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat9_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat8_20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat8_19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat8_18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat8_17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat8_16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat8_15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat8_14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat8_13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat8_12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat8_11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat8_10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat8_9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat8_8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat8_7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat8_6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat8_5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat8_4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat8_3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat8_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat8_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat7_20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat7_19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat7_18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat7_17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat7_16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat7_15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat7_14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat7_13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat7_12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat7_11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat7_10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat7_9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat7_8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat7_7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat7_6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat7_5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat7_4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat7_3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat7_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat7_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat6_20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat6_19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat6_18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat6_17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat6_16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat6_15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat6_14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat6_13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat6_12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat6_11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat6_10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat6_9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat6_8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat6_7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat6_6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat6_5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat6_4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat6_3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat6_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat6_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat5_20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat5_19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat5_18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat5_17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat5_16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat5_15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat5_14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat5_13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat5_12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat5_11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat5_10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat5_9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat5_8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat5_7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat5_6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat5_5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat5_4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat5_3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat5_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat5_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat4_20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat4_19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat4_18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat4_17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat4_16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat4_15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat4_14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat4_13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat4_12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat4_11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat4_10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat4_9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat4_8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat4_7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat4_6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat4_5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat4_4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat4_3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat4_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat4_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat3_20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat3_19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat3_18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat3_17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat3_16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat3_15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat3_14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat3_13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat3_12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat3_11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat3_10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat3_9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat3_8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat3_7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat3_6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat3_5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat3_4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat3_3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat3_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat3_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat2_20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat2_19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat2_18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat2_17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat2_16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat2_15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat2_14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat2_13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat2_12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat2_11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat2_10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat2_9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat2_8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat2_7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat2_6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat2_5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat2_4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat2_3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat2_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat2_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat1_20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat1_19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat1_18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat1_17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat1_16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat1_15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat1_14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat1_13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat1_12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat1_11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat1_10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat1_9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat1_8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat1_7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat1_6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat1_5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat1_4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat1_3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat1_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBxPlat1_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PctBx_Pioche, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Pnl_J3.SuspendLayout()
        Me.Pnl_J4.SuspendLayout()
        Me.SuspendLayout()
        '
        'Gp_MainJ
        '
        Me.Gp_MainJ.BackColor = System.Drawing.Color.Transparent
        Me.Gp_MainJ.Controls.Add(Me.PctBx_TuileMJ6)
        Me.Gp_MainJ.Controls.Add(Me.PctBx_TuileMJ5)
        Me.Gp_MainJ.Controls.Add(Me.PctBx_TuileMJ4)
        Me.Gp_MainJ.Controls.Add(Me.PctBx_TuileMJ2)
        Me.Gp_MainJ.Controls.Add(Me.PctBx_TuileMJ3)
        Me.Gp_MainJ.Controls.Add(Me.PctBx_TuileMJ1)
        Me.Gp_MainJ.Location = New System.Drawing.Point(1157, 646)
        Me.Gp_MainJ.Margin = New System.Windows.Forms.Padding(4)
        Me.Gp_MainJ.Name = "Gp_MainJ"
        Me.Gp_MainJ.Padding = New System.Windows.Forms.Padding(4)
        Me.Gp_MainJ.Size = New System.Drawing.Size(653, 111)
        Me.Gp_MainJ.TabIndex = 20
        Me.Gp_MainJ.TabStop = False
        '
        'PctBx_TuileMJ6
        '
        Me.PctBx_TuileMJ6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.PctBx_TuileMJ6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PctBx_TuileMJ6.Image = Global.Qwirkle.My.Resources.Resources.CroixVerte
        Me.PctBx_TuileMJ6.Location = New System.Drawing.Point(544, 12)
        Me.PctBx_TuileMJ6.Margin = New System.Windows.Forms.Padding(4)
        Me.PctBx_TuileMJ6.Name = "PctBx_TuileMJ6"
        Me.PctBx_TuileMJ6.Size = New System.Drawing.Size(99, 92)
        Me.PctBx_TuileMJ6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PctBx_TuileMJ6.TabIndex = 34
        Me.PctBx_TuileMJ6.TabStop = False
        '
        'PctBx_TuileMJ5
        '
        Me.PctBx_TuileMJ5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PctBx_TuileMJ5.Image = Global.Qwirkle.My.Resources.Resources.RondBleu
        Me.PctBx_TuileMJ5.Location = New System.Drawing.Point(437, 12)
        Me.PctBx_TuileMJ5.Margin = New System.Windows.Forms.Padding(4)
        Me.PctBx_TuileMJ5.Name = "PctBx_TuileMJ5"
        Me.PctBx_TuileMJ5.Size = New System.Drawing.Size(99, 92)
        Me.PctBx_TuileMJ5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PctBx_TuileMJ5.TabIndex = 32
        Me.PctBx_TuileMJ5.TabStop = False
        '
        'PctBx_TuileMJ4
        '
        Me.PctBx_TuileMJ4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PctBx_TuileMJ4.Image = Global.Qwirkle.My.Resources.Resources.EtoileRouge
        Me.PctBx_TuileMJ4.Location = New System.Drawing.Point(331, 12)
        Me.PctBx_TuileMJ4.Margin = New System.Windows.Forms.Padding(4)
        Me.PctBx_TuileMJ4.Name = "PctBx_TuileMJ4"
        Me.PctBx_TuileMJ4.Size = New System.Drawing.Size(99, 92)
        Me.PctBx_TuileMJ4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PctBx_TuileMJ4.TabIndex = 33
        Me.PctBx_TuileMJ4.TabStop = False
        '
        'PctBx_TuileMJ2
        '
        Me.PctBx_TuileMJ2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PctBx_TuileMJ2.Image = Global.Qwirkle.My.Resources.Resources.TrefleBleu
        Me.PctBx_TuileMJ2.Location = New System.Drawing.Point(117, 12)
        Me.PctBx_TuileMJ2.Margin = New System.Windows.Forms.Padding(4)
        Me.PctBx_TuileMJ2.Name = "PctBx_TuileMJ2"
        Me.PctBx_TuileMJ2.Size = New System.Drawing.Size(99, 92)
        Me.PctBx_TuileMJ2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PctBx_TuileMJ2.TabIndex = 31
        Me.PctBx_TuileMJ2.TabStop = False
        '
        'PctBx_TuileMJ3
        '
        Me.PctBx_TuileMJ3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PctBx_TuileMJ3.Image = Global.Qwirkle.My.Resources.Resources.TrefleJaune
        Me.PctBx_TuileMJ3.Location = New System.Drawing.Point(224, 12)
        Me.PctBx_TuileMJ3.Margin = New System.Windows.Forms.Padding(4)
        Me.PctBx_TuileMJ3.Name = "PctBx_TuileMJ3"
        Me.PctBx_TuileMJ3.Size = New System.Drawing.Size(99, 92)
        Me.PctBx_TuileMJ3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PctBx_TuileMJ3.TabIndex = 30
        Me.PctBx_TuileMJ3.TabStop = False
        '
        'PctBx_TuileMJ1
        '
        Me.PctBx_TuileMJ1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PctBx_TuileMJ1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PctBx_TuileMJ1.Image = Global.Qwirkle.My.Resources.Resources.LosangeOrange
        Me.PctBx_TuileMJ1.Location = New System.Drawing.Point(11, 12)
        Me.PctBx_TuileMJ1.Margin = New System.Windows.Forms.Padding(4)
        Me.PctBx_TuileMJ1.Name = "PctBx_TuileMJ1"
        Me.PctBx_TuileMJ1.Size = New System.Drawing.Size(99, 92)
        Me.PctBx_TuileMJ1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PctBx_TuileMJ1.TabIndex = 29
        Me.PctBx_TuileMJ1.TabStop = False
        '
        'Btn_Retour
        '
        Me.Btn_Retour.Image = Global.Qwirkle.My.Resources.Resources.BoutonRetour_Plateau
        Me.Btn_Retour.Location = New System.Drawing.Point(1569, 802)
        Me.Btn_Retour.Margin = New System.Windows.Forms.Padding(4)
        Me.Btn_Retour.Name = "Btn_Retour"
        Me.Btn_Retour.Size = New System.Drawing.Size(107, 98)
        Me.Btn_Retour.TabIndex = 22
        Me.Btn_Retour.UseVisualStyleBackColor = True
        '
        'Btn_Valid
        '
        Me.Btn_Valid.Image = Global.Qwirkle.My.Resources.Resources.BoutonValidez_Plateau
        Me.Btn_Valid.Location = New System.Drawing.Point(1697, 802)
        Me.Btn_Valid.Margin = New System.Windows.Forms.Padding(4)
        Me.Btn_Valid.Name = "Btn_Valid"
        Me.Btn_Valid.Size = New System.Drawing.Size(107, 98)
        Me.Btn_Valid.TabIndex = 23
        Me.Btn_Valid.UseVisualStyleBackColor = True
        '
        'GpBx_Plateau
        '
        Me.GpBx_Plateau.BackColor = System.Drawing.SystemColors.Control
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat20_20)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat20_19)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat20_18)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat20_17)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat20_16)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat20_15)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat20_14)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat20_13)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat20_12)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat20_11)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat20_10)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat20_9)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat20_8)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat20_7)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat20_6)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat20_5)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat20_4)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat20_3)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat20_2)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat20_1)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat19_20)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat19_19)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat19_18)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat19_17)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat19_16)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat19_15)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat19_14)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat19_13)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat19_12)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat19_11)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat19_10)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat19_9)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat19_8)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat19_7)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat19_6)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat19_5)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat19_4)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat19_3)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat19_2)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat19_1)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat18_20)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat18_19)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat18_18)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat18_17)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat18_16)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat18_15)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat18_14)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat18_13)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat18_12)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat18_11)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat18_10)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat18_9)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat18_8)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat18_7)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat18_6)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat18_5)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat18_4)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat18_3)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat18_2)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat18_1)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat17_20)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat17_19)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat17_18)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat17_17)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat17_16)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat17_15)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat17_14)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat17_13)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat17_12)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat17_11)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat17_10)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat17_9)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat17_8)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat17_7)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat17_6)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat17_5)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat17_4)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat17_3)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat17_2)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat17_1)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat16_20)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat16_19)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat16_18)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat16_17)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat16_16)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat16_15)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat16_14)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat16_13)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat16_12)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat16_11)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat16_10)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat16_9)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat16_8)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat16_7)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat16_6)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat16_5)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat16_4)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat16_3)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat16_2)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat16_1)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat15_20)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat15_19)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat15_18)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat15_17)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat15_16)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat15_15)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat15_14)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat15_13)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat15_12)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat15_11)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat15_10)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat15_9)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat15_8)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat15_7)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat15_6)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat15_5)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat15_4)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat15_3)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat15_2)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat15_1)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat14_20)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat14_19)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat14_18)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat14_17)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat14_16)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat14_15)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat14_14)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat14_13)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat14_12)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat14_11)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat14_10)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat14_9)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat14_8)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat14_7)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat14_6)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat14_5)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat14_4)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat14_3)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat14_2)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat14_1)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat13_20)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat13_19)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat13_18)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat13_17)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat13_16)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat13_15)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat13_14)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat13_13)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat13_12)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat13_11)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat13_10)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat13_9)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat13_8)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat13_7)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat13_6)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat13_5)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat13_4)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat13_3)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat13_2)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat13_1)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat12_20)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat12_19)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat12_18)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat12_17)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat12_16)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat12_15)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat12_14)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat12_13)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat12_12)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat12_11)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat12_10)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat12_9)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat12_8)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat12_7)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat12_6)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat12_5)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat12_4)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat12_3)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat12_2)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat12_1)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat11_20)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat11_19)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat11_18)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat11_17)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat11_16)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat11_15)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat11_14)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat11_13)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat11_12)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat11_11)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat11_10)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat11_9)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat11_8)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat11_7)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat11_6)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat11_5)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat11_4)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat11_3)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat11_2)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat11_1)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat10_20)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat10_19)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat10_18)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat10_17)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat10_16)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat10_15)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat10_14)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat10_13)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat10_12)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat10_11)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat10_10)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat10_9)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat10_8)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat10_7)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat10_6)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat10_5)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat10_4)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat10_3)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat10_2)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat10_1)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat9_20)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat9_19)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat9_18)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat9_17)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat9_16)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat9_15)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat9_14)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat9_13)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat9_12)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat9_11)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat9_10)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat9_9)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat9_8)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat9_7)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat9_6)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat9_5)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat9_4)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat9_3)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat9_2)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat9_1)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat8_20)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat8_19)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat8_18)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat8_17)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat8_16)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat8_15)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat8_14)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat8_13)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat8_12)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat8_11)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat8_10)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat8_9)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat8_8)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat8_7)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat8_6)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat8_5)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat8_4)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat8_3)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat8_2)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat8_1)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat7_20)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat7_19)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat7_18)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat7_17)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat7_16)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat7_15)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat7_14)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat7_13)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat7_12)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat7_11)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat7_10)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat7_9)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat7_8)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat7_7)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat7_6)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat7_5)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat7_4)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat7_3)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat7_2)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat7_1)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat6_20)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat6_19)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat6_18)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat6_17)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat6_16)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat6_15)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat6_14)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat6_13)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat6_12)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat6_11)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat6_10)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat6_9)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat6_8)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat6_7)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat6_6)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat6_5)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat6_4)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat6_3)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat6_2)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat6_1)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat5_20)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat5_19)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat5_18)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat5_17)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat5_16)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat5_15)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat5_14)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat5_13)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat5_12)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat5_11)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat5_10)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat5_9)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat5_8)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat5_7)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat5_6)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat5_5)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat5_4)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat5_3)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat5_2)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat5_1)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat4_20)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat4_19)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat4_18)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat4_17)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat4_16)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat4_15)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat4_14)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat4_13)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat4_12)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat4_11)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat4_10)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat4_9)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat4_8)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat4_7)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat4_6)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat4_5)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat4_4)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat4_3)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat4_2)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat4_1)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat3_20)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat3_19)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat3_18)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat3_17)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat3_16)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat3_15)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat3_14)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat3_13)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat3_12)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat3_11)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat3_10)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat3_9)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat3_8)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat3_7)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat3_6)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat3_5)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat3_4)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat3_3)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat3_2)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat3_1)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat2_20)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat2_19)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat2_18)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat2_17)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat2_16)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat2_15)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat2_14)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat2_13)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat2_12)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat2_11)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat2_10)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat2_9)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat2_8)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat2_7)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat2_6)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat2_5)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat2_4)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat2_3)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat2_2)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat2_1)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat1_20)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat1_19)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat1_18)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat1_17)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat1_16)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat1_15)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat1_14)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat1_13)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat1_12)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat1_11)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat1_10)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat1_9)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat1_8)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat1_7)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat1_6)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat1_5)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat1_4)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat1_3)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat1_2)
        Me.GpBx_Plateau.Controls.Add(Me.PicBxPlat1_1)
        Me.GpBx_Plateau.Location = New System.Drawing.Point(35, 22)
        Me.GpBx_Plateau.Margin = New System.Windows.Forms.Padding(4)
        Me.GpBx_Plateau.Name = "GpBx_Plateau"
        Me.GpBx_Plateau.Padding = New System.Windows.Forms.Padding(4)
        Me.GpBx_Plateau.Size = New System.Drawing.Size(1064, 975)
        Me.GpBx_Plateau.TabIndex = 27
        Me.GpBx_Plateau.TabStop = False
        '
        'PicBxPlat20_20
        '
        Me.PicBxPlat20_20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat20_20.Location = New System.Drawing.Point(1004, 912)
        Me.PicBxPlat20_20.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat20_20.Name = "PicBxPlat20_20"
        Me.PicBxPlat20_20.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat20_20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat20_20.TabIndex = 801
        Me.PicBxPlat20_20.TabStop = False
        '
        'PicBxPlat20_19
        '
        Me.PicBxPlat20_19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat20_19.Location = New System.Drawing.Point(952, 912)
        Me.PicBxPlat20_19.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat20_19.Name = "PicBxPlat20_19"
        Me.PicBxPlat20_19.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat20_19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat20_19.TabIndex = 800
        Me.PicBxPlat20_19.TabStop = False
        '
        'PicBxPlat20_18
        '
        Me.PicBxPlat20_18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat20_18.Location = New System.Drawing.Point(900, 912)
        Me.PicBxPlat20_18.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat20_18.Name = "PicBxPlat20_18"
        Me.PicBxPlat20_18.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat20_18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat20_18.TabIndex = 799
        Me.PicBxPlat20_18.TabStop = False
        '
        'PicBxPlat20_17
        '
        Me.PicBxPlat20_17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat20_17.Location = New System.Drawing.Point(848, 912)
        Me.PicBxPlat20_17.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat20_17.Name = "PicBxPlat20_17"
        Me.PicBxPlat20_17.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat20_17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat20_17.TabIndex = 798
        Me.PicBxPlat20_17.TabStop = False
        '
        'PicBxPlat20_16
        '
        Me.PicBxPlat20_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat20_16.Location = New System.Drawing.Point(796, 912)
        Me.PicBxPlat20_16.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat20_16.Name = "PicBxPlat20_16"
        Me.PicBxPlat20_16.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat20_16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat20_16.TabIndex = 797
        Me.PicBxPlat20_16.TabStop = False
        '
        'PicBxPlat20_15
        '
        Me.PicBxPlat20_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat20_15.Location = New System.Drawing.Point(744, 912)
        Me.PicBxPlat20_15.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat20_15.Name = "PicBxPlat20_15"
        Me.PicBxPlat20_15.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat20_15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat20_15.TabIndex = 796
        Me.PicBxPlat20_15.TabStop = False
        '
        'PicBxPlat20_14
        '
        Me.PicBxPlat20_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat20_14.Location = New System.Drawing.Point(692, 912)
        Me.PicBxPlat20_14.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat20_14.Name = "PicBxPlat20_14"
        Me.PicBxPlat20_14.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat20_14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat20_14.TabIndex = 795
        Me.PicBxPlat20_14.TabStop = False
        '
        'PicBxPlat20_13
        '
        Me.PicBxPlat20_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat20_13.Location = New System.Drawing.Point(640, 912)
        Me.PicBxPlat20_13.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat20_13.Name = "PicBxPlat20_13"
        Me.PicBxPlat20_13.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat20_13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat20_13.TabIndex = 794
        Me.PicBxPlat20_13.TabStop = False
        '
        'PicBxPlat20_12
        '
        Me.PicBxPlat20_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat20_12.Location = New System.Drawing.Point(588, 912)
        Me.PicBxPlat20_12.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat20_12.Name = "PicBxPlat20_12"
        Me.PicBxPlat20_12.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat20_12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat20_12.TabIndex = 793
        Me.PicBxPlat20_12.TabStop = False
        '
        'PicBxPlat20_11
        '
        Me.PicBxPlat20_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat20_11.Location = New System.Drawing.Point(536, 912)
        Me.PicBxPlat20_11.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat20_11.Name = "PicBxPlat20_11"
        Me.PicBxPlat20_11.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat20_11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat20_11.TabIndex = 792
        Me.PicBxPlat20_11.TabStop = False
        '
        'PicBxPlat20_10
        '
        Me.PicBxPlat20_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat20_10.Location = New System.Drawing.Point(484, 912)
        Me.PicBxPlat20_10.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat20_10.Name = "PicBxPlat20_10"
        Me.PicBxPlat20_10.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat20_10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat20_10.TabIndex = 791
        Me.PicBxPlat20_10.TabStop = False
        '
        'PicBxPlat20_9
        '
        Me.PicBxPlat20_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat20_9.Location = New System.Drawing.Point(432, 912)
        Me.PicBxPlat20_9.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat20_9.Name = "PicBxPlat20_9"
        Me.PicBxPlat20_9.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat20_9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat20_9.TabIndex = 790
        Me.PicBxPlat20_9.TabStop = False
        '
        'PicBxPlat20_8
        '
        Me.PicBxPlat20_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat20_8.Location = New System.Drawing.Point(380, 912)
        Me.PicBxPlat20_8.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat20_8.Name = "PicBxPlat20_8"
        Me.PicBxPlat20_8.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat20_8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat20_8.TabIndex = 789
        Me.PicBxPlat20_8.TabStop = False
        '
        'PicBxPlat20_7
        '
        Me.PicBxPlat20_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat20_7.Location = New System.Drawing.Point(328, 912)
        Me.PicBxPlat20_7.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat20_7.Name = "PicBxPlat20_7"
        Me.PicBxPlat20_7.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat20_7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat20_7.TabIndex = 788
        Me.PicBxPlat20_7.TabStop = False
        '
        'PicBxPlat20_6
        '
        Me.PicBxPlat20_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat20_6.Location = New System.Drawing.Point(276, 912)
        Me.PicBxPlat20_6.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat20_6.Name = "PicBxPlat20_6"
        Me.PicBxPlat20_6.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat20_6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat20_6.TabIndex = 787
        Me.PicBxPlat20_6.TabStop = False
        '
        'PicBxPlat20_5
        '
        Me.PicBxPlat20_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat20_5.Location = New System.Drawing.Point(224, 912)
        Me.PicBxPlat20_5.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat20_5.Name = "PicBxPlat20_5"
        Me.PicBxPlat20_5.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat20_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat20_5.TabIndex = 786
        Me.PicBxPlat20_5.TabStop = False
        '
        'PicBxPlat20_4
        '
        Me.PicBxPlat20_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat20_4.Location = New System.Drawing.Point(172, 912)
        Me.PicBxPlat20_4.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat20_4.Name = "PicBxPlat20_4"
        Me.PicBxPlat20_4.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat20_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat20_4.TabIndex = 785
        Me.PicBxPlat20_4.TabStop = False
        '
        'PicBxPlat20_3
        '
        Me.PicBxPlat20_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat20_3.Location = New System.Drawing.Point(120, 912)
        Me.PicBxPlat20_3.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat20_3.Name = "PicBxPlat20_3"
        Me.PicBxPlat20_3.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat20_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat20_3.TabIndex = 784
        Me.PicBxPlat20_3.TabStop = False
        '
        'PicBxPlat20_2
        '
        Me.PicBxPlat20_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat20_2.Location = New System.Drawing.Point(68, 912)
        Me.PicBxPlat20_2.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat20_2.Name = "PicBxPlat20_2"
        Me.PicBxPlat20_2.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat20_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat20_2.TabIndex = 783
        Me.PicBxPlat20_2.TabStop = False
        '
        'PicBxPlat20_1
        '
        Me.PicBxPlat20_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat20_1.Location = New System.Drawing.Point(16, 912)
        Me.PicBxPlat20_1.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat20_1.Name = "PicBxPlat20_1"
        Me.PicBxPlat20_1.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat20_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat20_1.TabIndex = 782
        Me.PicBxPlat20_1.TabStop = False
        '
        'PicBxPlat19_20
        '
        Me.PicBxPlat19_20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat19_20.Location = New System.Drawing.Point(1004, 864)
        Me.PicBxPlat19_20.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat19_20.Name = "PicBxPlat19_20"
        Me.PicBxPlat19_20.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat19_20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat19_20.TabIndex = 781
        Me.PicBxPlat19_20.TabStop = False
        '
        'PicBxPlat19_19
        '
        Me.PicBxPlat19_19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat19_19.Location = New System.Drawing.Point(952, 864)
        Me.PicBxPlat19_19.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat19_19.Name = "PicBxPlat19_19"
        Me.PicBxPlat19_19.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat19_19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat19_19.TabIndex = 780
        Me.PicBxPlat19_19.TabStop = False
        '
        'PicBxPlat19_18
        '
        Me.PicBxPlat19_18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat19_18.Location = New System.Drawing.Point(900, 864)
        Me.PicBxPlat19_18.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat19_18.Name = "PicBxPlat19_18"
        Me.PicBxPlat19_18.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat19_18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat19_18.TabIndex = 779
        Me.PicBxPlat19_18.TabStop = False
        '
        'PicBxPlat19_17
        '
        Me.PicBxPlat19_17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat19_17.Location = New System.Drawing.Point(848, 864)
        Me.PicBxPlat19_17.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat19_17.Name = "PicBxPlat19_17"
        Me.PicBxPlat19_17.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat19_17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat19_17.TabIndex = 778
        Me.PicBxPlat19_17.TabStop = False
        '
        'PicBxPlat19_16
        '
        Me.PicBxPlat19_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat19_16.Location = New System.Drawing.Point(796, 864)
        Me.PicBxPlat19_16.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat19_16.Name = "PicBxPlat19_16"
        Me.PicBxPlat19_16.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat19_16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat19_16.TabIndex = 777
        Me.PicBxPlat19_16.TabStop = False
        '
        'PicBxPlat19_15
        '
        Me.PicBxPlat19_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat19_15.Location = New System.Drawing.Point(744, 864)
        Me.PicBxPlat19_15.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat19_15.Name = "PicBxPlat19_15"
        Me.PicBxPlat19_15.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat19_15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat19_15.TabIndex = 776
        Me.PicBxPlat19_15.TabStop = False
        '
        'PicBxPlat19_14
        '
        Me.PicBxPlat19_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat19_14.Location = New System.Drawing.Point(692, 864)
        Me.PicBxPlat19_14.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat19_14.Name = "PicBxPlat19_14"
        Me.PicBxPlat19_14.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat19_14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat19_14.TabIndex = 775
        Me.PicBxPlat19_14.TabStop = False
        '
        'PicBxPlat19_13
        '
        Me.PicBxPlat19_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat19_13.Location = New System.Drawing.Point(640, 864)
        Me.PicBxPlat19_13.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat19_13.Name = "PicBxPlat19_13"
        Me.PicBxPlat19_13.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat19_13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat19_13.TabIndex = 774
        Me.PicBxPlat19_13.TabStop = False
        '
        'PicBxPlat19_12
        '
        Me.PicBxPlat19_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat19_12.Location = New System.Drawing.Point(588, 864)
        Me.PicBxPlat19_12.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat19_12.Name = "PicBxPlat19_12"
        Me.PicBxPlat19_12.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat19_12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat19_12.TabIndex = 773
        Me.PicBxPlat19_12.TabStop = False
        '
        'PicBxPlat19_11
        '
        Me.PicBxPlat19_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat19_11.Location = New System.Drawing.Point(536, 864)
        Me.PicBxPlat19_11.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat19_11.Name = "PicBxPlat19_11"
        Me.PicBxPlat19_11.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat19_11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat19_11.TabIndex = 772
        Me.PicBxPlat19_11.TabStop = False
        '
        'PicBxPlat19_10
        '
        Me.PicBxPlat19_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat19_10.Location = New System.Drawing.Point(484, 864)
        Me.PicBxPlat19_10.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat19_10.Name = "PicBxPlat19_10"
        Me.PicBxPlat19_10.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat19_10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat19_10.TabIndex = 771
        Me.PicBxPlat19_10.TabStop = False
        '
        'PicBxPlat19_9
        '
        Me.PicBxPlat19_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat19_9.Location = New System.Drawing.Point(432, 864)
        Me.PicBxPlat19_9.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat19_9.Name = "PicBxPlat19_9"
        Me.PicBxPlat19_9.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat19_9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat19_9.TabIndex = 770
        Me.PicBxPlat19_9.TabStop = False
        '
        'PicBxPlat19_8
        '
        Me.PicBxPlat19_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat19_8.Location = New System.Drawing.Point(380, 864)
        Me.PicBxPlat19_8.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat19_8.Name = "PicBxPlat19_8"
        Me.PicBxPlat19_8.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat19_8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat19_8.TabIndex = 769
        Me.PicBxPlat19_8.TabStop = False
        '
        'PicBxPlat19_7
        '
        Me.PicBxPlat19_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat19_7.Location = New System.Drawing.Point(328, 864)
        Me.PicBxPlat19_7.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat19_7.Name = "PicBxPlat19_7"
        Me.PicBxPlat19_7.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat19_7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat19_7.TabIndex = 768
        Me.PicBxPlat19_7.TabStop = False
        '
        'PicBxPlat19_6
        '
        Me.PicBxPlat19_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat19_6.Location = New System.Drawing.Point(276, 864)
        Me.PicBxPlat19_6.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat19_6.Name = "PicBxPlat19_6"
        Me.PicBxPlat19_6.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat19_6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat19_6.TabIndex = 767
        Me.PicBxPlat19_6.TabStop = False
        '
        'PicBxPlat19_5
        '
        Me.PicBxPlat19_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat19_5.Location = New System.Drawing.Point(224, 864)
        Me.PicBxPlat19_5.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat19_5.Name = "PicBxPlat19_5"
        Me.PicBxPlat19_5.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat19_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat19_5.TabIndex = 766
        Me.PicBxPlat19_5.TabStop = False
        '
        'PicBxPlat19_4
        '
        Me.PicBxPlat19_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat19_4.Location = New System.Drawing.Point(172, 864)
        Me.PicBxPlat19_4.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat19_4.Name = "PicBxPlat19_4"
        Me.PicBxPlat19_4.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat19_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat19_4.TabIndex = 765
        Me.PicBxPlat19_4.TabStop = False
        '
        'PicBxPlat19_3
        '
        Me.PicBxPlat19_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat19_3.Location = New System.Drawing.Point(120, 864)
        Me.PicBxPlat19_3.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat19_3.Name = "PicBxPlat19_3"
        Me.PicBxPlat19_3.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat19_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat19_3.TabIndex = 764
        Me.PicBxPlat19_3.TabStop = False
        '
        'PicBxPlat19_2
        '
        Me.PicBxPlat19_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat19_2.Location = New System.Drawing.Point(68, 864)
        Me.PicBxPlat19_2.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat19_2.Name = "PicBxPlat19_2"
        Me.PicBxPlat19_2.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat19_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat19_2.TabIndex = 763
        Me.PicBxPlat19_2.TabStop = False
        '
        'PicBxPlat19_1
        '
        Me.PicBxPlat19_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat19_1.Location = New System.Drawing.Point(16, 864)
        Me.PicBxPlat19_1.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat19_1.Name = "PicBxPlat19_1"
        Me.PicBxPlat19_1.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat19_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat19_1.TabIndex = 762
        Me.PicBxPlat19_1.TabStop = False
        '
        'PicBxPlat18_20
        '
        Me.PicBxPlat18_20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat18_20.Location = New System.Drawing.Point(1004, 816)
        Me.PicBxPlat18_20.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat18_20.Name = "PicBxPlat18_20"
        Me.PicBxPlat18_20.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat18_20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat18_20.TabIndex = 761
        Me.PicBxPlat18_20.TabStop = False
        '
        'PicBxPlat18_19
        '
        Me.PicBxPlat18_19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat18_19.Location = New System.Drawing.Point(952, 816)
        Me.PicBxPlat18_19.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat18_19.Name = "PicBxPlat18_19"
        Me.PicBxPlat18_19.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat18_19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat18_19.TabIndex = 760
        Me.PicBxPlat18_19.TabStop = False
        '
        'PicBxPlat18_18
        '
        Me.PicBxPlat18_18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat18_18.Location = New System.Drawing.Point(900, 816)
        Me.PicBxPlat18_18.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat18_18.Name = "PicBxPlat18_18"
        Me.PicBxPlat18_18.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat18_18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat18_18.TabIndex = 759
        Me.PicBxPlat18_18.TabStop = False
        '
        'PicBxPlat18_17
        '
        Me.PicBxPlat18_17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat18_17.Location = New System.Drawing.Point(848, 816)
        Me.PicBxPlat18_17.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat18_17.Name = "PicBxPlat18_17"
        Me.PicBxPlat18_17.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat18_17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat18_17.TabIndex = 758
        Me.PicBxPlat18_17.TabStop = False
        '
        'PicBxPlat18_16
        '
        Me.PicBxPlat18_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat18_16.Location = New System.Drawing.Point(796, 816)
        Me.PicBxPlat18_16.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat18_16.Name = "PicBxPlat18_16"
        Me.PicBxPlat18_16.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat18_16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat18_16.TabIndex = 757
        Me.PicBxPlat18_16.TabStop = False
        '
        'PicBxPlat18_15
        '
        Me.PicBxPlat18_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat18_15.Location = New System.Drawing.Point(744, 816)
        Me.PicBxPlat18_15.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat18_15.Name = "PicBxPlat18_15"
        Me.PicBxPlat18_15.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat18_15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat18_15.TabIndex = 756
        Me.PicBxPlat18_15.TabStop = False
        '
        'PicBxPlat18_14
        '
        Me.PicBxPlat18_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat18_14.Location = New System.Drawing.Point(692, 816)
        Me.PicBxPlat18_14.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat18_14.Name = "PicBxPlat18_14"
        Me.PicBxPlat18_14.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat18_14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat18_14.TabIndex = 755
        Me.PicBxPlat18_14.TabStop = False
        '
        'PicBxPlat18_13
        '
        Me.PicBxPlat18_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat18_13.Location = New System.Drawing.Point(640, 816)
        Me.PicBxPlat18_13.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat18_13.Name = "PicBxPlat18_13"
        Me.PicBxPlat18_13.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat18_13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat18_13.TabIndex = 754
        Me.PicBxPlat18_13.TabStop = False
        '
        'PicBxPlat18_12
        '
        Me.PicBxPlat18_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat18_12.Location = New System.Drawing.Point(588, 816)
        Me.PicBxPlat18_12.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat18_12.Name = "PicBxPlat18_12"
        Me.PicBxPlat18_12.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat18_12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat18_12.TabIndex = 753
        Me.PicBxPlat18_12.TabStop = False
        '
        'PicBxPlat18_11
        '
        Me.PicBxPlat18_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat18_11.Location = New System.Drawing.Point(536, 816)
        Me.PicBxPlat18_11.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat18_11.Name = "PicBxPlat18_11"
        Me.PicBxPlat18_11.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat18_11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat18_11.TabIndex = 752
        Me.PicBxPlat18_11.TabStop = False
        '
        'PicBxPlat18_10
        '
        Me.PicBxPlat18_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat18_10.Location = New System.Drawing.Point(484, 816)
        Me.PicBxPlat18_10.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat18_10.Name = "PicBxPlat18_10"
        Me.PicBxPlat18_10.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat18_10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat18_10.TabIndex = 751
        Me.PicBxPlat18_10.TabStop = False
        '
        'PicBxPlat18_9
        '
        Me.PicBxPlat18_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat18_9.Location = New System.Drawing.Point(432, 816)
        Me.PicBxPlat18_9.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat18_9.Name = "PicBxPlat18_9"
        Me.PicBxPlat18_9.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat18_9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat18_9.TabIndex = 750
        Me.PicBxPlat18_9.TabStop = False
        '
        'PicBxPlat18_8
        '
        Me.PicBxPlat18_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat18_8.Location = New System.Drawing.Point(380, 816)
        Me.PicBxPlat18_8.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat18_8.Name = "PicBxPlat18_8"
        Me.PicBxPlat18_8.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat18_8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat18_8.TabIndex = 749
        Me.PicBxPlat18_8.TabStop = False
        '
        'PicBxPlat18_7
        '
        Me.PicBxPlat18_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat18_7.Location = New System.Drawing.Point(328, 816)
        Me.PicBxPlat18_7.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat18_7.Name = "PicBxPlat18_7"
        Me.PicBxPlat18_7.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat18_7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat18_7.TabIndex = 748
        Me.PicBxPlat18_7.TabStop = False
        '
        'PicBxPlat18_6
        '
        Me.PicBxPlat18_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat18_6.Location = New System.Drawing.Point(276, 816)
        Me.PicBxPlat18_6.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat18_6.Name = "PicBxPlat18_6"
        Me.PicBxPlat18_6.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat18_6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat18_6.TabIndex = 747
        Me.PicBxPlat18_6.TabStop = False
        '
        'PicBxPlat18_5
        '
        Me.PicBxPlat18_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat18_5.Location = New System.Drawing.Point(224, 816)
        Me.PicBxPlat18_5.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat18_5.Name = "PicBxPlat18_5"
        Me.PicBxPlat18_5.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat18_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat18_5.TabIndex = 746
        Me.PicBxPlat18_5.TabStop = False
        '
        'PicBxPlat18_4
        '
        Me.PicBxPlat18_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat18_4.Location = New System.Drawing.Point(172, 816)
        Me.PicBxPlat18_4.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat18_4.Name = "PicBxPlat18_4"
        Me.PicBxPlat18_4.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat18_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat18_4.TabIndex = 745
        Me.PicBxPlat18_4.TabStop = False
        '
        'PicBxPlat18_3
        '
        Me.PicBxPlat18_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat18_3.Location = New System.Drawing.Point(120, 816)
        Me.PicBxPlat18_3.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat18_3.Name = "PicBxPlat18_3"
        Me.PicBxPlat18_3.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat18_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat18_3.TabIndex = 744
        Me.PicBxPlat18_3.TabStop = False
        '
        'PicBxPlat18_2
        '
        Me.PicBxPlat18_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat18_2.Location = New System.Drawing.Point(68, 816)
        Me.PicBxPlat18_2.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat18_2.Name = "PicBxPlat18_2"
        Me.PicBxPlat18_2.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat18_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat18_2.TabIndex = 743
        Me.PicBxPlat18_2.TabStop = False
        '
        'PicBxPlat18_1
        '
        Me.PicBxPlat18_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat18_1.Location = New System.Drawing.Point(16, 816)
        Me.PicBxPlat18_1.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat18_1.Name = "PicBxPlat18_1"
        Me.PicBxPlat18_1.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat18_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat18_1.TabIndex = 742
        Me.PicBxPlat18_1.TabStop = False
        '
        'PicBxPlat17_20
        '
        Me.PicBxPlat17_20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat17_20.Location = New System.Drawing.Point(1004, 768)
        Me.PicBxPlat17_20.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat17_20.Name = "PicBxPlat17_20"
        Me.PicBxPlat17_20.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat17_20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat17_20.TabIndex = 741
        Me.PicBxPlat17_20.TabStop = False
        '
        'PicBxPlat17_19
        '
        Me.PicBxPlat17_19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat17_19.Location = New System.Drawing.Point(952, 768)
        Me.PicBxPlat17_19.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat17_19.Name = "PicBxPlat17_19"
        Me.PicBxPlat17_19.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat17_19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat17_19.TabIndex = 740
        Me.PicBxPlat17_19.TabStop = False
        '
        'PicBxPlat17_18
        '
        Me.PicBxPlat17_18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat17_18.Location = New System.Drawing.Point(900, 768)
        Me.PicBxPlat17_18.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat17_18.Name = "PicBxPlat17_18"
        Me.PicBxPlat17_18.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat17_18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat17_18.TabIndex = 739
        Me.PicBxPlat17_18.TabStop = False
        '
        'PicBxPlat17_17
        '
        Me.PicBxPlat17_17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat17_17.Location = New System.Drawing.Point(848, 768)
        Me.PicBxPlat17_17.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat17_17.Name = "PicBxPlat17_17"
        Me.PicBxPlat17_17.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat17_17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat17_17.TabIndex = 738
        Me.PicBxPlat17_17.TabStop = False
        '
        'PicBxPlat17_16
        '
        Me.PicBxPlat17_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat17_16.Location = New System.Drawing.Point(796, 768)
        Me.PicBxPlat17_16.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat17_16.Name = "PicBxPlat17_16"
        Me.PicBxPlat17_16.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat17_16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat17_16.TabIndex = 737
        Me.PicBxPlat17_16.TabStop = False
        '
        'PicBxPlat17_15
        '
        Me.PicBxPlat17_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat17_15.Location = New System.Drawing.Point(744, 768)
        Me.PicBxPlat17_15.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat17_15.Name = "PicBxPlat17_15"
        Me.PicBxPlat17_15.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat17_15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat17_15.TabIndex = 736
        Me.PicBxPlat17_15.TabStop = False
        '
        'PicBxPlat17_14
        '
        Me.PicBxPlat17_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat17_14.Location = New System.Drawing.Point(692, 768)
        Me.PicBxPlat17_14.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat17_14.Name = "PicBxPlat17_14"
        Me.PicBxPlat17_14.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat17_14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat17_14.TabIndex = 735
        Me.PicBxPlat17_14.TabStop = False
        '
        'PicBxPlat17_13
        '
        Me.PicBxPlat17_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat17_13.Location = New System.Drawing.Point(640, 768)
        Me.PicBxPlat17_13.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat17_13.Name = "PicBxPlat17_13"
        Me.PicBxPlat17_13.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat17_13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat17_13.TabIndex = 734
        Me.PicBxPlat17_13.TabStop = False
        '
        'PicBxPlat17_12
        '
        Me.PicBxPlat17_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat17_12.Location = New System.Drawing.Point(588, 768)
        Me.PicBxPlat17_12.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat17_12.Name = "PicBxPlat17_12"
        Me.PicBxPlat17_12.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat17_12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat17_12.TabIndex = 733
        Me.PicBxPlat17_12.TabStop = False
        '
        'PicBxPlat17_11
        '
        Me.PicBxPlat17_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat17_11.Location = New System.Drawing.Point(536, 768)
        Me.PicBxPlat17_11.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat17_11.Name = "PicBxPlat17_11"
        Me.PicBxPlat17_11.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat17_11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat17_11.TabIndex = 732
        Me.PicBxPlat17_11.TabStop = False
        '
        'PicBxPlat17_10
        '
        Me.PicBxPlat17_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat17_10.Location = New System.Drawing.Point(484, 768)
        Me.PicBxPlat17_10.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat17_10.Name = "PicBxPlat17_10"
        Me.PicBxPlat17_10.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat17_10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat17_10.TabIndex = 731
        Me.PicBxPlat17_10.TabStop = False
        '
        'PicBxPlat17_9
        '
        Me.PicBxPlat17_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat17_9.Location = New System.Drawing.Point(432, 768)
        Me.PicBxPlat17_9.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat17_9.Name = "PicBxPlat17_9"
        Me.PicBxPlat17_9.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat17_9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat17_9.TabIndex = 730
        Me.PicBxPlat17_9.TabStop = False
        '
        'PicBxPlat17_8
        '
        Me.PicBxPlat17_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat17_8.Location = New System.Drawing.Point(380, 768)
        Me.PicBxPlat17_8.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat17_8.Name = "PicBxPlat17_8"
        Me.PicBxPlat17_8.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat17_8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat17_8.TabIndex = 729
        Me.PicBxPlat17_8.TabStop = False
        '
        'PicBxPlat17_7
        '
        Me.PicBxPlat17_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat17_7.Location = New System.Drawing.Point(328, 768)
        Me.PicBxPlat17_7.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat17_7.Name = "PicBxPlat17_7"
        Me.PicBxPlat17_7.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat17_7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat17_7.TabIndex = 728
        Me.PicBxPlat17_7.TabStop = False
        '
        'PicBxPlat17_6
        '
        Me.PicBxPlat17_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat17_6.Location = New System.Drawing.Point(276, 768)
        Me.PicBxPlat17_6.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat17_6.Name = "PicBxPlat17_6"
        Me.PicBxPlat17_6.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat17_6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat17_6.TabIndex = 727
        Me.PicBxPlat17_6.TabStop = False
        '
        'PicBxPlat17_5
        '
        Me.PicBxPlat17_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat17_5.Location = New System.Drawing.Point(224, 768)
        Me.PicBxPlat17_5.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat17_5.Name = "PicBxPlat17_5"
        Me.PicBxPlat17_5.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat17_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat17_5.TabIndex = 726
        Me.PicBxPlat17_5.TabStop = False
        '
        'PicBxPlat17_4
        '
        Me.PicBxPlat17_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat17_4.Location = New System.Drawing.Point(172, 768)
        Me.PicBxPlat17_4.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat17_4.Name = "PicBxPlat17_4"
        Me.PicBxPlat17_4.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat17_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat17_4.TabIndex = 725
        Me.PicBxPlat17_4.TabStop = False
        '
        'PicBxPlat17_3
        '
        Me.PicBxPlat17_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat17_3.Location = New System.Drawing.Point(120, 768)
        Me.PicBxPlat17_3.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat17_3.Name = "PicBxPlat17_3"
        Me.PicBxPlat17_3.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat17_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat17_3.TabIndex = 724
        Me.PicBxPlat17_3.TabStop = False
        '
        'PicBxPlat17_2
        '
        Me.PicBxPlat17_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat17_2.Location = New System.Drawing.Point(68, 768)
        Me.PicBxPlat17_2.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat17_2.Name = "PicBxPlat17_2"
        Me.PicBxPlat17_2.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat17_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat17_2.TabIndex = 723
        Me.PicBxPlat17_2.TabStop = False
        '
        'PicBxPlat17_1
        '
        Me.PicBxPlat17_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat17_1.Location = New System.Drawing.Point(16, 768)
        Me.PicBxPlat17_1.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat17_1.Name = "PicBxPlat17_1"
        Me.PicBxPlat17_1.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat17_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat17_1.TabIndex = 722
        Me.PicBxPlat17_1.TabStop = False
        '
        'PicBxPlat16_20
        '
        Me.PicBxPlat16_20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat16_20.Location = New System.Drawing.Point(1004, 720)
        Me.PicBxPlat16_20.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat16_20.Name = "PicBxPlat16_20"
        Me.PicBxPlat16_20.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat16_20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat16_20.TabIndex = 721
        Me.PicBxPlat16_20.TabStop = False
        '
        'PicBxPlat16_19
        '
        Me.PicBxPlat16_19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat16_19.Location = New System.Drawing.Point(952, 720)
        Me.PicBxPlat16_19.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat16_19.Name = "PicBxPlat16_19"
        Me.PicBxPlat16_19.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat16_19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat16_19.TabIndex = 720
        Me.PicBxPlat16_19.TabStop = False
        '
        'PicBxPlat16_18
        '
        Me.PicBxPlat16_18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat16_18.Location = New System.Drawing.Point(900, 720)
        Me.PicBxPlat16_18.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat16_18.Name = "PicBxPlat16_18"
        Me.PicBxPlat16_18.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat16_18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat16_18.TabIndex = 719
        Me.PicBxPlat16_18.TabStop = False
        '
        'PicBxPlat16_17
        '
        Me.PicBxPlat16_17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat16_17.Location = New System.Drawing.Point(848, 720)
        Me.PicBxPlat16_17.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat16_17.Name = "PicBxPlat16_17"
        Me.PicBxPlat16_17.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat16_17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat16_17.TabIndex = 718
        Me.PicBxPlat16_17.TabStop = False
        '
        'PicBxPlat16_16
        '
        Me.PicBxPlat16_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat16_16.Location = New System.Drawing.Point(796, 720)
        Me.PicBxPlat16_16.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat16_16.Name = "PicBxPlat16_16"
        Me.PicBxPlat16_16.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat16_16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat16_16.TabIndex = 717
        Me.PicBxPlat16_16.TabStop = False
        '
        'PicBxPlat16_15
        '
        Me.PicBxPlat16_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat16_15.Location = New System.Drawing.Point(744, 720)
        Me.PicBxPlat16_15.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat16_15.Name = "PicBxPlat16_15"
        Me.PicBxPlat16_15.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat16_15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat16_15.TabIndex = 716
        Me.PicBxPlat16_15.TabStop = False
        '
        'PicBxPlat16_14
        '
        Me.PicBxPlat16_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat16_14.Location = New System.Drawing.Point(692, 720)
        Me.PicBxPlat16_14.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat16_14.Name = "PicBxPlat16_14"
        Me.PicBxPlat16_14.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat16_14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat16_14.TabIndex = 715
        Me.PicBxPlat16_14.TabStop = False
        '
        'PicBxPlat16_13
        '
        Me.PicBxPlat16_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat16_13.Location = New System.Drawing.Point(640, 720)
        Me.PicBxPlat16_13.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat16_13.Name = "PicBxPlat16_13"
        Me.PicBxPlat16_13.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat16_13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat16_13.TabIndex = 714
        Me.PicBxPlat16_13.TabStop = False
        '
        'PicBxPlat16_12
        '
        Me.PicBxPlat16_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat16_12.Location = New System.Drawing.Point(588, 720)
        Me.PicBxPlat16_12.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat16_12.Name = "PicBxPlat16_12"
        Me.PicBxPlat16_12.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat16_12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat16_12.TabIndex = 713
        Me.PicBxPlat16_12.TabStop = False
        '
        'PicBxPlat16_11
        '
        Me.PicBxPlat16_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat16_11.Location = New System.Drawing.Point(536, 720)
        Me.PicBxPlat16_11.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat16_11.Name = "PicBxPlat16_11"
        Me.PicBxPlat16_11.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat16_11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat16_11.TabIndex = 712
        Me.PicBxPlat16_11.TabStop = False
        '
        'PicBxPlat16_10
        '
        Me.PicBxPlat16_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat16_10.Location = New System.Drawing.Point(484, 720)
        Me.PicBxPlat16_10.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat16_10.Name = "PicBxPlat16_10"
        Me.PicBxPlat16_10.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat16_10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat16_10.TabIndex = 711
        Me.PicBxPlat16_10.TabStop = False
        '
        'PicBxPlat16_9
        '
        Me.PicBxPlat16_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat16_9.Location = New System.Drawing.Point(432, 720)
        Me.PicBxPlat16_9.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat16_9.Name = "PicBxPlat16_9"
        Me.PicBxPlat16_9.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat16_9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat16_9.TabIndex = 710
        Me.PicBxPlat16_9.TabStop = False
        '
        'PicBxPlat16_8
        '
        Me.PicBxPlat16_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat16_8.Location = New System.Drawing.Point(380, 720)
        Me.PicBxPlat16_8.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat16_8.Name = "PicBxPlat16_8"
        Me.PicBxPlat16_8.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat16_8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat16_8.TabIndex = 709
        Me.PicBxPlat16_8.TabStop = False
        '
        'PicBxPlat16_7
        '
        Me.PicBxPlat16_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat16_7.Location = New System.Drawing.Point(328, 720)
        Me.PicBxPlat16_7.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat16_7.Name = "PicBxPlat16_7"
        Me.PicBxPlat16_7.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat16_7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat16_7.TabIndex = 708
        Me.PicBxPlat16_7.TabStop = False
        '
        'PicBxPlat16_6
        '
        Me.PicBxPlat16_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat16_6.Location = New System.Drawing.Point(276, 720)
        Me.PicBxPlat16_6.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat16_6.Name = "PicBxPlat16_6"
        Me.PicBxPlat16_6.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat16_6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat16_6.TabIndex = 707
        Me.PicBxPlat16_6.TabStop = False
        '
        'PicBxPlat16_5
        '
        Me.PicBxPlat16_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat16_5.Location = New System.Drawing.Point(224, 720)
        Me.PicBxPlat16_5.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat16_5.Name = "PicBxPlat16_5"
        Me.PicBxPlat16_5.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat16_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat16_5.TabIndex = 706
        Me.PicBxPlat16_5.TabStop = False
        '
        'PicBxPlat16_4
        '
        Me.PicBxPlat16_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat16_4.Location = New System.Drawing.Point(172, 720)
        Me.PicBxPlat16_4.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat16_4.Name = "PicBxPlat16_4"
        Me.PicBxPlat16_4.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat16_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat16_4.TabIndex = 705
        Me.PicBxPlat16_4.TabStop = False
        '
        'PicBxPlat16_3
        '
        Me.PicBxPlat16_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat16_3.Location = New System.Drawing.Point(120, 720)
        Me.PicBxPlat16_3.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat16_3.Name = "PicBxPlat16_3"
        Me.PicBxPlat16_3.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat16_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat16_3.TabIndex = 704
        Me.PicBxPlat16_3.TabStop = False
        '
        'PicBxPlat16_2
        '
        Me.PicBxPlat16_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat16_2.Location = New System.Drawing.Point(68, 720)
        Me.PicBxPlat16_2.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat16_2.Name = "PicBxPlat16_2"
        Me.PicBxPlat16_2.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat16_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat16_2.TabIndex = 703
        Me.PicBxPlat16_2.TabStop = False
        '
        'PicBxPlat16_1
        '
        Me.PicBxPlat16_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat16_1.Location = New System.Drawing.Point(16, 720)
        Me.PicBxPlat16_1.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat16_1.Name = "PicBxPlat16_1"
        Me.PicBxPlat16_1.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat16_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat16_1.TabIndex = 702
        Me.PicBxPlat16_1.TabStop = False
        '
        'PicBxPlat15_20
        '
        Me.PicBxPlat15_20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat15_20.Location = New System.Drawing.Point(1004, 672)
        Me.PicBxPlat15_20.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat15_20.Name = "PicBxPlat15_20"
        Me.PicBxPlat15_20.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat15_20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat15_20.TabIndex = 701
        Me.PicBxPlat15_20.TabStop = False
        '
        'PicBxPlat15_19
        '
        Me.PicBxPlat15_19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat15_19.Location = New System.Drawing.Point(952, 672)
        Me.PicBxPlat15_19.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat15_19.Name = "PicBxPlat15_19"
        Me.PicBxPlat15_19.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat15_19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat15_19.TabIndex = 700
        Me.PicBxPlat15_19.TabStop = False
        '
        'PicBxPlat15_18
        '
        Me.PicBxPlat15_18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat15_18.Location = New System.Drawing.Point(900, 672)
        Me.PicBxPlat15_18.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat15_18.Name = "PicBxPlat15_18"
        Me.PicBxPlat15_18.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat15_18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat15_18.TabIndex = 699
        Me.PicBxPlat15_18.TabStop = False
        '
        'PicBxPlat15_17
        '
        Me.PicBxPlat15_17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat15_17.Location = New System.Drawing.Point(848, 672)
        Me.PicBxPlat15_17.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat15_17.Name = "PicBxPlat15_17"
        Me.PicBxPlat15_17.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat15_17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat15_17.TabIndex = 698
        Me.PicBxPlat15_17.TabStop = False
        '
        'PicBxPlat15_16
        '
        Me.PicBxPlat15_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat15_16.Location = New System.Drawing.Point(796, 672)
        Me.PicBxPlat15_16.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat15_16.Name = "PicBxPlat15_16"
        Me.PicBxPlat15_16.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat15_16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat15_16.TabIndex = 697
        Me.PicBxPlat15_16.TabStop = False
        '
        'PicBxPlat15_15
        '
        Me.PicBxPlat15_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat15_15.Location = New System.Drawing.Point(744, 672)
        Me.PicBxPlat15_15.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat15_15.Name = "PicBxPlat15_15"
        Me.PicBxPlat15_15.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat15_15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat15_15.TabIndex = 696
        Me.PicBxPlat15_15.TabStop = False
        '
        'PicBxPlat15_14
        '
        Me.PicBxPlat15_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat15_14.Location = New System.Drawing.Point(692, 672)
        Me.PicBxPlat15_14.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat15_14.Name = "PicBxPlat15_14"
        Me.PicBxPlat15_14.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat15_14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat15_14.TabIndex = 695
        Me.PicBxPlat15_14.TabStop = False
        '
        'PicBxPlat15_13
        '
        Me.PicBxPlat15_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat15_13.Location = New System.Drawing.Point(640, 672)
        Me.PicBxPlat15_13.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat15_13.Name = "PicBxPlat15_13"
        Me.PicBxPlat15_13.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat15_13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat15_13.TabIndex = 694
        Me.PicBxPlat15_13.TabStop = False
        '
        'PicBxPlat15_12
        '
        Me.PicBxPlat15_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat15_12.Location = New System.Drawing.Point(588, 672)
        Me.PicBxPlat15_12.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat15_12.Name = "PicBxPlat15_12"
        Me.PicBxPlat15_12.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat15_12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat15_12.TabIndex = 693
        Me.PicBxPlat15_12.TabStop = False
        '
        'PicBxPlat15_11
        '
        Me.PicBxPlat15_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat15_11.Location = New System.Drawing.Point(536, 672)
        Me.PicBxPlat15_11.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat15_11.Name = "PicBxPlat15_11"
        Me.PicBxPlat15_11.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat15_11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat15_11.TabIndex = 692
        Me.PicBxPlat15_11.TabStop = False
        '
        'PicBxPlat15_10
        '
        Me.PicBxPlat15_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat15_10.Location = New System.Drawing.Point(484, 672)
        Me.PicBxPlat15_10.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat15_10.Name = "PicBxPlat15_10"
        Me.PicBxPlat15_10.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat15_10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat15_10.TabIndex = 691
        Me.PicBxPlat15_10.TabStop = False
        '
        'PicBxPlat15_9
        '
        Me.PicBxPlat15_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat15_9.Location = New System.Drawing.Point(432, 672)
        Me.PicBxPlat15_9.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat15_9.Name = "PicBxPlat15_9"
        Me.PicBxPlat15_9.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat15_9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat15_9.TabIndex = 690
        Me.PicBxPlat15_9.TabStop = False
        '
        'PicBxPlat15_8
        '
        Me.PicBxPlat15_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat15_8.Location = New System.Drawing.Point(380, 672)
        Me.PicBxPlat15_8.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat15_8.Name = "PicBxPlat15_8"
        Me.PicBxPlat15_8.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat15_8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat15_8.TabIndex = 689
        Me.PicBxPlat15_8.TabStop = False
        '
        'PicBxPlat15_7
        '
        Me.PicBxPlat15_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat15_7.Location = New System.Drawing.Point(328, 672)
        Me.PicBxPlat15_7.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat15_7.Name = "PicBxPlat15_7"
        Me.PicBxPlat15_7.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat15_7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat15_7.TabIndex = 688
        Me.PicBxPlat15_7.TabStop = False
        '
        'PicBxPlat15_6
        '
        Me.PicBxPlat15_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat15_6.Location = New System.Drawing.Point(276, 672)
        Me.PicBxPlat15_6.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat15_6.Name = "PicBxPlat15_6"
        Me.PicBxPlat15_6.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat15_6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat15_6.TabIndex = 687
        Me.PicBxPlat15_6.TabStop = False
        '
        'PicBxPlat15_5
        '
        Me.PicBxPlat15_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat15_5.Location = New System.Drawing.Point(224, 672)
        Me.PicBxPlat15_5.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat15_5.Name = "PicBxPlat15_5"
        Me.PicBxPlat15_5.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat15_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat15_5.TabIndex = 686
        Me.PicBxPlat15_5.TabStop = False
        '
        'PicBxPlat15_4
        '
        Me.PicBxPlat15_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat15_4.Location = New System.Drawing.Point(172, 672)
        Me.PicBxPlat15_4.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat15_4.Name = "PicBxPlat15_4"
        Me.PicBxPlat15_4.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat15_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat15_4.TabIndex = 685
        Me.PicBxPlat15_4.TabStop = False
        '
        'PicBxPlat15_3
        '
        Me.PicBxPlat15_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat15_3.Location = New System.Drawing.Point(120, 672)
        Me.PicBxPlat15_3.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat15_3.Name = "PicBxPlat15_3"
        Me.PicBxPlat15_3.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat15_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat15_3.TabIndex = 684
        Me.PicBxPlat15_3.TabStop = False
        '
        'PicBxPlat15_2
        '
        Me.PicBxPlat15_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat15_2.Location = New System.Drawing.Point(68, 672)
        Me.PicBxPlat15_2.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat15_2.Name = "PicBxPlat15_2"
        Me.PicBxPlat15_2.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat15_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat15_2.TabIndex = 683
        Me.PicBxPlat15_2.TabStop = False
        '
        'PicBxPlat15_1
        '
        Me.PicBxPlat15_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat15_1.Location = New System.Drawing.Point(16, 672)
        Me.PicBxPlat15_1.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat15_1.Name = "PicBxPlat15_1"
        Me.PicBxPlat15_1.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat15_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat15_1.TabIndex = 682
        Me.PicBxPlat15_1.TabStop = False
        '
        'PicBxPlat14_20
        '
        Me.PicBxPlat14_20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat14_20.Location = New System.Drawing.Point(1004, 624)
        Me.PicBxPlat14_20.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat14_20.Name = "PicBxPlat14_20"
        Me.PicBxPlat14_20.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat14_20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat14_20.TabIndex = 681
        Me.PicBxPlat14_20.TabStop = False
        '
        'PicBxPlat14_19
        '
        Me.PicBxPlat14_19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat14_19.Location = New System.Drawing.Point(952, 624)
        Me.PicBxPlat14_19.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat14_19.Name = "PicBxPlat14_19"
        Me.PicBxPlat14_19.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat14_19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat14_19.TabIndex = 680
        Me.PicBxPlat14_19.TabStop = False
        '
        'PicBxPlat14_18
        '
        Me.PicBxPlat14_18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat14_18.Location = New System.Drawing.Point(900, 624)
        Me.PicBxPlat14_18.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat14_18.Name = "PicBxPlat14_18"
        Me.PicBxPlat14_18.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat14_18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat14_18.TabIndex = 679
        Me.PicBxPlat14_18.TabStop = False
        '
        'PicBxPlat14_17
        '
        Me.PicBxPlat14_17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat14_17.Location = New System.Drawing.Point(848, 624)
        Me.PicBxPlat14_17.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat14_17.Name = "PicBxPlat14_17"
        Me.PicBxPlat14_17.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat14_17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat14_17.TabIndex = 678
        Me.PicBxPlat14_17.TabStop = False
        '
        'PicBxPlat14_16
        '
        Me.PicBxPlat14_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat14_16.Location = New System.Drawing.Point(796, 624)
        Me.PicBxPlat14_16.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat14_16.Name = "PicBxPlat14_16"
        Me.PicBxPlat14_16.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat14_16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat14_16.TabIndex = 677
        Me.PicBxPlat14_16.TabStop = False
        '
        'PicBxPlat14_15
        '
        Me.PicBxPlat14_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat14_15.Location = New System.Drawing.Point(744, 624)
        Me.PicBxPlat14_15.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat14_15.Name = "PicBxPlat14_15"
        Me.PicBxPlat14_15.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat14_15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat14_15.TabIndex = 676
        Me.PicBxPlat14_15.TabStop = False
        '
        'PicBxPlat14_14
        '
        Me.PicBxPlat14_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat14_14.Location = New System.Drawing.Point(692, 624)
        Me.PicBxPlat14_14.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat14_14.Name = "PicBxPlat14_14"
        Me.PicBxPlat14_14.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat14_14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat14_14.TabIndex = 675
        Me.PicBxPlat14_14.TabStop = False
        '
        'PicBxPlat14_13
        '
        Me.PicBxPlat14_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat14_13.Location = New System.Drawing.Point(640, 624)
        Me.PicBxPlat14_13.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat14_13.Name = "PicBxPlat14_13"
        Me.PicBxPlat14_13.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat14_13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat14_13.TabIndex = 674
        Me.PicBxPlat14_13.TabStop = False
        '
        'PicBxPlat14_12
        '
        Me.PicBxPlat14_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat14_12.Location = New System.Drawing.Point(588, 624)
        Me.PicBxPlat14_12.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat14_12.Name = "PicBxPlat14_12"
        Me.PicBxPlat14_12.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat14_12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat14_12.TabIndex = 673
        Me.PicBxPlat14_12.TabStop = False
        '
        'PicBxPlat14_11
        '
        Me.PicBxPlat14_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat14_11.Location = New System.Drawing.Point(536, 624)
        Me.PicBxPlat14_11.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat14_11.Name = "PicBxPlat14_11"
        Me.PicBxPlat14_11.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat14_11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat14_11.TabIndex = 672
        Me.PicBxPlat14_11.TabStop = False
        '
        'PicBxPlat14_10
        '
        Me.PicBxPlat14_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat14_10.Location = New System.Drawing.Point(484, 624)
        Me.PicBxPlat14_10.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat14_10.Name = "PicBxPlat14_10"
        Me.PicBxPlat14_10.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat14_10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat14_10.TabIndex = 671
        Me.PicBxPlat14_10.TabStop = False
        '
        'PicBxPlat14_9
        '
        Me.PicBxPlat14_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat14_9.Location = New System.Drawing.Point(432, 624)
        Me.PicBxPlat14_9.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat14_9.Name = "PicBxPlat14_9"
        Me.PicBxPlat14_9.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat14_9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat14_9.TabIndex = 670
        Me.PicBxPlat14_9.TabStop = False
        '
        'PicBxPlat14_8
        '
        Me.PicBxPlat14_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat14_8.Location = New System.Drawing.Point(380, 624)
        Me.PicBxPlat14_8.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat14_8.Name = "PicBxPlat14_8"
        Me.PicBxPlat14_8.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat14_8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat14_8.TabIndex = 669
        Me.PicBxPlat14_8.TabStop = False
        '
        'PicBxPlat14_7
        '
        Me.PicBxPlat14_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat14_7.Location = New System.Drawing.Point(328, 624)
        Me.PicBxPlat14_7.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat14_7.Name = "PicBxPlat14_7"
        Me.PicBxPlat14_7.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat14_7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat14_7.TabIndex = 668
        Me.PicBxPlat14_7.TabStop = False
        '
        'PicBxPlat14_6
        '
        Me.PicBxPlat14_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat14_6.Location = New System.Drawing.Point(276, 624)
        Me.PicBxPlat14_6.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat14_6.Name = "PicBxPlat14_6"
        Me.PicBxPlat14_6.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat14_6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat14_6.TabIndex = 667
        Me.PicBxPlat14_6.TabStop = False
        '
        'PicBxPlat14_5
        '
        Me.PicBxPlat14_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat14_5.Location = New System.Drawing.Point(224, 624)
        Me.PicBxPlat14_5.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat14_5.Name = "PicBxPlat14_5"
        Me.PicBxPlat14_5.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat14_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat14_5.TabIndex = 666
        Me.PicBxPlat14_5.TabStop = False
        '
        'PicBxPlat14_4
        '
        Me.PicBxPlat14_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat14_4.Location = New System.Drawing.Point(172, 624)
        Me.PicBxPlat14_4.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat14_4.Name = "PicBxPlat14_4"
        Me.PicBxPlat14_4.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat14_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat14_4.TabIndex = 665
        Me.PicBxPlat14_4.TabStop = False
        '
        'PicBxPlat14_3
        '
        Me.PicBxPlat14_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat14_3.Location = New System.Drawing.Point(120, 624)
        Me.PicBxPlat14_3.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat14_3.Name = "PicBxPlat14_3"
        Me.PicBxPlat14_3.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat14_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat14_3.TabIndex = 664
        Me.PicBxPlat14_3.TabStop = False
        '
        'PicBxPlat14_2
        '
        Me.PicBxPlat14_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat14_2.Location = New System.Drawing.Point(68, 624)
        Me.PicBxPlat14_2.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat14_2.Name = "PicBxPlat14_2"
        Me.PicBxPlat14_2.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat14_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat14_2.TabIndex = 663
        Me.PicBxPlat14_2.TabStop = False
        '
        'PicBxPlat14_1
        '
        Me.PicBxPlat14_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat14_1.Location = New System.Drawing.Point(16, 624)
        Me.PicBxPlat14_1.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat14_1.Name = "PicBxPlat14_1"
        Me.PicBxPlat14_1.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat14_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat14_1.TabIndex = 662
        Me.PicBxPlat14_1.TabStop = False
        '
        'PicBxPlat13_20
        '
        Me.PicBxPlat13_20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat13_20.Location = New System.Drawing.Point(1004, 576)
        Me.PicBxPlat13_20.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat13_20.Name = "PicBxPlat13_20"
        Me.PicBxPlat13_20.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat13_20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat13_20.TabIndex = 661
        Me.PicBxPlat13_20.TabStop = False
        '
        'PicBxPlat13_19
        '
        Me.PicBxPlat13_19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat13_19.Location = New System.Drawing.Point(952, 576)
        Me.PicBxPlat13_19.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat13_19.Name = "PicBxPlat13_19"
        Me.PicBxPlat13_19.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat13_19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat13_19.TabIndex = 660
        Me.PicBxPlat13_19.TabStop = False
        '
        'PicBxPlat13_18
        '
        Me.PicBxPlat13_18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat13_18.Location = New System.Drawing.Point(900, 576)
        Me.PicBxPlat13_18.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat13_18.Name = "PicBxPlat13_18"
        Me.PicBxPlat13_18.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat13_18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat13_18.TabIndex = 659
        Me.PicBxPlat13_18.TabStop = False
        '
        'PicBxPlat13_17
        '
        Me.PicBxPlat13_17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat13_17.Location = New System.Drawing.Point(848, 576)
        Me.PicBxPlat13_17.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat13_17.Name = "PicBxPlat13_17"
        Me.PicBxPlat13_17.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat13_17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat13_17.TabIndex = 658
        Me.PicBxPlat13_17.TabStop = False
        '
        'PicBxPlat13_16
        '
        Me.PicBxPlat13_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat13_16.Location = New System.Drawing.Point(796, 576)
        Me.PicBxPlat13_16.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat13_16.Name = "PicBxPlat13_16"
        Me.PicBxPlat13_16.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat13_16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat13_16.TabIndex = 657
        Me.PicBxPlat13_16.TabStop = False
        '
        'PicBxPlat13_15
        '
        Me.PicBxPlat13_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat13_15.Location = New System.Drawing.Point(744, 576)
        Me.PicBxPlat13_15.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat13_15.Name = "PicBxPlat13_15"
        Me.PicBxPlat13_15.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat13_15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat13_15.TabIndex = 656
        Me.PicBxPlat13_15.TabStop = False
        '
        'PicBxPlat13_14
        '
        Me.PicBxPlat13_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat13_14.Location = New System.Drawing.Point(692, 576)
        Me.PicBxPlat13_14.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat13_14.Name = "PicBxPlat13_14"
        Me.PicBxPlat13_14.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat13_14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat13_14.TabIndex = 655
        Me.PicBxPlat13_14.TabStop = False
        '
        'PicBxPlat13_13
        '
        Me.PicBxPlat13_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat13_13.Location = New System.Drawing.Point(640, 576)
        Me.PicBxPlat13_13.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat13_13.Name = "PicBxPlat13_13"
        Me.PicBxPlat13_13.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat13_13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat13_13.TabIndex = 654
        Me.PicBxPlat13_13.TabStop = False
        '
        'PicBxPlat13_12
        '
        Me.PicBxPlat13_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat13_12.Location = New System.Drawing.Point(588, 576)
        Me.PicBxPlat13_12.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat13_12.Name = "PicBxPlat13_12"
        Me.PicBxPlat13_12.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat13_12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat13_12.TabIndex = 653
        Me.PicBxPlat13_12.TabStop = False
        '
        'PicBxPlat13_11
        '
        Me.PicBxPlat13_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat13_11.Location = New System.Drawing.Point(536, 576)
        Me.PicBxPlat13_11.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat13_11.Name = "PicBxPlat13_11"
        Me.PicBxPlat13_11.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat13_11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat13_11.TabIndex = 652
        Me.PicBxPlat13_11.TabStop = False
        '
        'PicBxPlat13_10
        '
        Me.PicBxPlat13_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat13_10.Location = New System.Drawing.Point(484, 576)
        Me.PicBxPlat13_10.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat13_10.Name = "PicBxPlat13_10"
        Me.PicBxPlat13_10.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat13_10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat13_10.TabIndex = 651
        Me.PicBxPlat13_10.TabStop = False
        '
        'PicBxPlat13_9
        '
        Me.PicBxPlat13_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat13_9.Location = New System.Drawing.Point(432, 576)
        Me.PicBxPlat13_9.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat13_9.Name = "PicBxPlat13_9"
        Me.PicBxPlat13_9.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat13_9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat13_9.TabIndex = 650
        Me.PicBxPlat13_9.TabStop = False
        '
        'PicBxPlat13_8
        '
        Me.PicBxPlat13_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat13_8.Location = New System.Drawing.Point(380, 576)
        Me.PicBxPlat13_8.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat13_8.Name = "PicBxPlat13_8"
        Me.PicBxPlat13_8.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat13_8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat13_8.TabIndex = 649
        Me.PicBxPlat13_8.TabStop = False
        '
        'PicBxPlat13_7
        '
        Me.PicBxPlat13_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat13_7.Location = New System.Drawing.Point(328, 576)
        Me.PicBxPlat13_7.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat13_7.Name = "PicBxPlat13_7"
        Me.PicBxPlat13_7.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat13_7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat13_7.TabIndex = 648
        Me.PicBxPlat13_7.TabStop = False
        '
        'PicBxPlat13_6
        '
        Me.PicBxPlat13_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat13_6.Location = New System.Drawing.Point(276, 576)
        Me.PicBxPlat13_6.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat13_6.Name = "PicBxPlat13_6"
        Me.PicBxPlat13_6.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat13_6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat13_6.TabIndex = 647
        Me.PicBxPlat13_6.TabStop = False
        '
        'PicBxPlat13_5
        '
        Me.PicBxPlat13_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat13_5.Location = New System.Drawing.Point(224, 576)
        Me.PicBxPlat13_5.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat13_5.Name = "PicBxPlat13_5"
        Me.PicBxPlat13_5.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat13_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat13_5.TabIndex = 646
        Me.PicBxPlat13_5.TabStop = False
        '
        'PicBxPlat13_4
        '
        Me.PicBxPlat13_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat13_4.Location = New System.Drawing.Point(172, 576)
        Me.PicBxPlat13_4.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat13_4.Name = "PicBxPlat13_4"
        Me.PicBxPlat13_4.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat13_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat13_4.TabIndex = 645
        Me.PicBxPlat13_4.TabStop = False
        '
        'PicBxPlat13_3
        '
        Me.PicBxPlat13_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat13_3.Location = New System.Drawing.Point(120, 576)
        Me.PicBxPlat13_3.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat13_3.Name = "PicBxPlat13_3"
        Me.PicBxPlat13_3.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat13_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat13_3.TabIndex = 644
        Me.PicBxPlat13_3.TabStop = False
        '
        'PicBxPlat13_2
        '
        Me.PicBxPlat13_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat13_2.Location = New System.Drawing.Point(68, 576)
        Me.PicBxPlat13_2.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat13_2.Name = "PicBxPlat13_2"
        Me.PicBxPlat13_2.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat13_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat13_2.TabIndex = 643
        Me.PicBxPlat13_2.TabStop = False
        '
        'PicBxPlat13_1
        '
        Me.PicBxPlat13_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat13_1.Location = New System.Drawing.Point(16, 576)
        Me.PicBxPlat13_1.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat13_1.Name = "PicBxPlat13_1"
        Me.PicBxPlat13_1.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat13_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat13_1.TabIndex = 642
        Me.PicBxPlat13_1.TabStop = False
        '
        'PicBxPlat12_20
        '
        Me.PicBxPlat12_20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat12_20.Location = New System.Drawing.Point(1004, 528)
        Me.PicBxPlat12_20.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat12_20.Name = "PicBxPlat12_20"
        Me.PicBxPlat12_20.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat12_20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat12_20.TabIndex = 641
        Me.PicBxPlat12_20.TabStop = False
        '
        'PicBxPlat12_19
        '
        Me.PicBxPlat12_19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat12_19.Location = New System.Drawing.Point(952, 528)
        Me.PicBxPlat12_19.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat12_19.Name = "PicBxPlat12_19"
        Me.PicBxPlat12_19.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat12_19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat12_19.TabIndex = 640
        Me.PicBxPlat12_19.TabStop = False
        '
        'PicBxPlat12_18
        '
        Me.PicBxPlat12_18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat12_18.Location = New System.Drawing.Point(900, 528)
        Me.PicBxPlat12_18.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat12_18.Name = "PicBxPlat12_18"
        Me.PicBxPlat12_18.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat12_18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat12_18.TabIndex = 639
        Me.PicBxPlat12_18.TabStop = False
        '
        'PicBxPlat12_17
        '
        Me.PicBxPlat12_17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat12_17.Location = New System.Drawing.Point(848, 528)
        Me.PicBxPlat12_17.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat12_17.Name = "PicBxPlat12_17"
        Me.PicBxPlat12_17.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat12_17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat12_17.TabIndex = 638
        Me.PicBxPlat12_17.TabStop = False
        '
        'PicBxPlat12_16
        '
        Me.PicBxPlat12_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat12_16.Location = New System.Drawing.Point(796, 528)
        Me.PicBxPlat12_16.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat12_16.Name = "PicBxPlat12_16"
        Me.PicBxPlat12_16.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat12_16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat12_16.TabIndex = 637
        Me.PicBxPlat12_16.TabStop = False
        '
        'PicBxPlat12_15
        '
        Me.PicBxPlat12_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat12_15.Location = New System.Drawing.Point(744, 528)
        Me.PicBxPlat12_15.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat12_15.Name = "PicBxPlat12_15"
        Me.PicBxPlat12_15.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat12_15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat12_15.TabIndex = 636
        Me.PicBxPlat12_15.TabStop = False
        '
        'PicBxPlat12_14
        '
        Me.PicBxPlat12_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat12_14.Location = New System.Drawing.Point(692, 528)
        Me.PicBxPlat12_14.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat12_14.Name = "PicBxPlat12_14"
        Me.PicBxPlat12_14.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat12_14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat12_14.TabIndex = 635
        Me.PicBxPlat12_14.TabStop = False
        '
        'PicBxPlat12_13
        '
        Me.PicBxPlat12_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat12_13.Location = New System.Drawing.Point(640, 528)
        Me.PicBxPlat12_13.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat12_13.Name = "PicBxPlat12_13"
        Me.PicBxPlat12_13.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat12_13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat12_13.TabIndex = 634
        Me.PicBxPlat12_13.TabStop = False
        '
        'PicBxPlat12_12
        '
        Me.PicBxPlat12_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat12_12.Location = New System.Drawing.Point(588, 528)
        Me.PicBxPlat12_12.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat12_12.Name = "PicBxPlat12_12"
        Me.PicBxPlat12_12.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat12_12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat12_12.TabIndex = 633
        Me.PicBxPlat12_12.TabStop = False
        '
        'PicBxPlat12_11
        '
        Me.PicBxPlat12_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat12_11.Location = New System.Drawing.Point(536, 528)
        Me.PicBxPlat12_11.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat12_11.Name = "PicBxPlat12_11"
        Me.PicBxPlat12_11.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat12_11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat12_11.TabIndex = 632
        Me.PicBxPlat12_11.TabStop = False
        '
        'PicBxPlat12_10
        '
        Me.PicBxPlat12_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat12_10.Location = New System.Drawing.Point(484, 528)
        Me.PicBxPlat12_10.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat12_10.Name = "PicBxPlat12_10"
        Me.PicBxPlat12_10.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat12_10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat12_10.TabIndex = 631
        Me.PicBxPlat12_10.TabStop = False
        '
        'PicBxPlat12_9
        '
        Me.PicBxPlat12_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat12_9.Location = New System.Drawing.Point(432, 528)
        Me.PicBxPlat12_9.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat12_9.Name = "PicBxPlat12_9"
        Me.PicBxPlat12_9.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat12_9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat12_9.TabIndex = 630
        Me.PicBxPlat12_9.TabStop = False
        '
        'PicBxPlat12_8
        '
        Me.PicBxPlat12_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat12_8.Location = New System.Drawing.Point(380, 528)
        Me.PicBxPlat12_8.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat12_8.Name = "PicBxPlat12_8"
        Me.PicBxPlat12_8.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat12_8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat12_8.TabIndex = 629
        Me.PicBxPlat12_8.TabStop = False
        '
        'PicBxPlat12_7
        '
        Me.PicBxPlat12_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat12_7.Location = New System.Drawing.Point(328, 528)
        Me.PicBxPlat12_7.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat12_7.Name = "PicBxPlat12_7"
        Me.PicBxPlat12_7.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat12_7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat12_7.TabIndex = 628
        Me.PicBxPlat12_7.TabStop = False
        '
        'PicBxPlat12_6
        '
        Me.PicBxPlat12_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat12_6.Location = New System.Drawing.Point(276, 528)
        Me.PicBxPlat12_6.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat12_6.Name = "PicBxPlat12_6"
        Me.PicBxPlat12_6.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat12_6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat12_6.TabIndex = 627
        Me.PicBxPlat12_6.TabStop = False
        '
        'PicBxPlat12_5
        '
        Me.PicBxPlat12_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat12_5.Location = New System.Drawing.Point(224, 528)
        Me.PicBxPlat12_5.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat12_5.Name = "PicBxPlat12_5"
        Me.PicBxPlat12_5.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat12_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat12_5.TabIndex = 626
        Me.PicBxPlat12_5.TabStop = False
        '
        'PicBxPlat12_4
        '
        Me.PicBxPlat12_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat12_4.Location = New System.Drawing.Point(172, 528)
        Me.PicBxPlat12_4.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat12_4.Name = "PicBxPlat12_4"
        Me.PicBxPlat12_4.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat12_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat12_4.TabIndex = 625
        Me.PicBxPlat12_4.TabStop = False
        '
        'PicBxPlat12_3
        '
        Me.PicBxPlat12_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat12_3.Location = New System.Drawing.Point(120, 528)
        Me.PicBxPlat12_3.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat12_3.Name = "PicBxPlat12_3"
        Me.PicBxPlat12_3.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat12_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat12_3.TabIndex = 624
        Me.PicBxPlat12_3.TabStop = False
        '
        'PicBxPlat12_2
        '
        Me.PicBxPlat12_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat12_2.Location = New System.Drawing.Point(68, 528)
        Me.PicBxPlat12_2.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat12_2.Name = "PicBxPlat12_2"
        Me.PicBxPlat12_2.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat12_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat12_2.TabIndex = 623
        Me.PicBxPlat12_2.TabStop = False
        '
        'PicBxPlat12_1
        '
        Me.PicBxPlat12_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat12_1.Location = New System.Drawing.Point(16, 528)
        Me.PicBxPlat12_1.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat12_1.Name = "PicBxPlat12_1"
        Me.PicBxPlat12_1.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat12_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat12_1.TabIndex = 622
        Me.PicBxPlat12_1.TabStop = False
        '
        'PicBxPlat11_20
        '
        Me.PicBxPlat11_20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat11_20.Location = New System.Drawing.Point(1004, 480)
        Me.PicBxPlat11_20.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat11_20.Name = "PicBxPlat11_20"
        Me.PicBxPlat11_20.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat11_20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat11_20.TabIndex = 621
        Me.PicBxPlat11_20.TabStop = False
        '
        'PicBxPlat11_19
        '
        Me.PicBxPlat11_19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat11_19.Location = New System.Drawing.Point(952, 480)
        Me.PicBxPlat11_19.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat11_19.Name = "PicBxPlat11_19"
        Me.PicBxPlat11_19.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat11_19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat11_19.TabIndex = 620
        Me.PicBxPlat11_19.TabStop = False
        '
        'PicBxPlat11_18
        '
        Me.PicBxPlat11_18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat11_18.Location = New System.Drawing.Point(900, 480)
        Me.PicBxPlat11_18.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat11_18.Name = "PicBxPlat11_18"
        Me.PicBxPlat11_18.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat11_18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat11_18.TabIndex = 619
        Me.PicBxPlat11_18.TabStop = False
        '
        'PicBxPlat11_17
        '
        Me.PicBxPlat11_17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat11_17.Location = New System.Drawing.Point(848, 480)
        Me.PicBxPlat11_17.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat11_17.Name = "PicBxPlat11_17"
        Me.PicBxPlat11_17.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat11_17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat11_17.TabIndex = 618
        Me.PicBxPlat11_17.TabStop = False
        '
        'PicBxPlat11_16
        '
        Me.PicBxPlat11_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat11_16.Location = New System.Drawing.Point(796, 480)
        Me.PicBxPlat11_16.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat11_16.Name = "PicBxPlat11_16"
        Me.PicBxPlat11_16.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat11_16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat11_16.TabIndex = 617
        Me.PicBxPlat11_16.TabStop = False
        '
        'PicBxPlat11_15
        '
        Me.PicBxPlat11_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat11_15.Location = New System.Drawing.Point(744, 480)
        Me.PicBxPlat11_15.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat11_15.Name = "PicBxPlat11_15"
        Me.PicBxPlat11_15.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat11_15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat11_15.TabIndex = 616
        Me.PicBxPlat11_15.TabStop = False
        '
        'PicBxPlat11_14
        '
        Me.PicBxPlat11_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat11_14.Location = New System.Drawing.Point(692, 480)
        Me.PicBxPlat11_14.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat11_14.Name = "PicBxPlat11_14"
        Me.PicBxPlat11_14.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat11_14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat11_14.TabIndex = 615
        Me.PicBxPlat11_14.TabStop = False
        '
        'PicBxPlat11_13
        '
        Me.PicBxPlat11_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat11_13.Location = New System.Drawing.Point(640, 480)
        Me.PicBxPlat11_13.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat11_13.Name = "PicBxPlat11_13"
        Me.PicBxPlat11_13.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat11_13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat11_13.TabIndex = 614
        Me.PicBxPlat11_13.TabStop = False
        '
        'PicBxPlat11_12
        '
        Me.PicBxPlat11_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat11_12.Location = New System.Drawing.Point(588, 480)
        Me.PicBxPlat11_12.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat11_12.Name = "PicBxPlat11_12"
        Me.PicBxPlat11_12.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat11_12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat11_12.TabIndex = 613
        Me.PicBxPlat11_12.TabStop = False
        '
        'PicBxPlat11_11
        '
        Me.PicBxPlat11_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat11_11.Location = New System.Drawing.Point(536, 480)
        Me.PicBxPlat11_11.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat11_11.Name = "PicBxPlat11_11"
        Me.PicBxPlat11_11.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat11_11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat11_11.TabIndex = 612
        Me.PicBxPlat11_11.TabStop = False
        '
        'PicBxPlat11_10
        '
        Me.PicBxPlat11_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat11_10.Location = New System.Drawing.Point(484, 480)
        Me.PicBxPlat11_10.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat11_10.Name = "PicBxPlat11_10"
        Me.PicBxPlat11_10.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat11_10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat11_10.TabIndex = 611
        Me.PicBxPlat11_10.TabStop = False
        '
        'PicBxPlat11_9
        '
        Me.PicBxPlat11_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat11_9.Location = New System.Drawing.Point(432, 480)
        Me.PicBxPlat11_9.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat11_9.Name = "PicBxPlat11_9"
        Me.PicBxPlat11_9.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat11_9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat11_9.TabIndex = 610
        Me.PicBxPlat11_9.TabStop = False
        '
        'PicBxPlat11_8
        '
        Me.PicBxPlat11_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat11_8.Location = New System.Drawing.Point(380, 480)
        Me.PicBxPlat11_8.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat11_8.Name = "PicBxPlat11_8"
        Me.PicBxPlat11_8.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat11_8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat11_8.TabIndex = 609
        Me.PicBxPlat11_8.TabStop = False
        '
        'PicBxPlat11_7
        '
        Me.PicBxPlat11_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat11_7.Location = New System.Drawing.Point(328, 480)
        Me.PicBxPlat11_7.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat11_7.Name = "PicBxPlat11_7"
        Me.PicBxPlat11_7.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat11_7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat11_7.TabIndex = 608
        Me.PicBxPlat11_7.TabStop = False
        '
        'PicBxPlat11_6
        '
        Me.PicBxPlat11_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat11_6.Location = New System.Drawing.Point(276, 480)
        Me.PicBxPlat11_6.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat11_6.Name = "PicBxPlat11_6"
        Me.PicBxPlat11_6.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat11_6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat11_6.TabIndex = 607
        Me.PicBxPlat11_6.TabStop = False
        '
        'PicBxPlat11_5
        '
        Me.PicBxPlat11_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat11_5.Location = New System.Drawing.Point(224, 480)
        Me.PicBxPlat11_5.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat11_5.Name = "PicBxPlat11_5"
        Me.PicBxPlat11_5.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat11_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat11_5.TabIndex = 606
        Me.PicBxPlat11_5.TabStop = False
        '
        'PicBxPlat11_4
        '
        Me.PicBxPlat11_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat11_4.Location = New System.Drawing.Point(172, 480)
        Me.PicBxPlat11_4.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat11_4.Name = "PicBxPlat11_4"
        Me.PicBxPlat11_4.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat11_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat11_4.TabIndex = 605
        Me.PicBxPlat11_4.TabStop = False
        '
        'PicBxPlat11_3
        '
        Me.PicBxPlat11_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat11_3.Location = New System.Drawing.Point(120, 480)
        Me.PicBxPlat11_3.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat11_3.Name = "PicBxPlat11_3"
        Me.PicBxPlat11_3.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat11_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat11_3.TabIndex = 604
        Me.PicBxPlat11_3.TabStop = False
        '
        'PicBxPlat11_2
        '
        Me.PicBxPlat11_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat11_2.Location = New System.Drawing.Point(68, 480)
        Me.PicBxPlat11_2.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat11_2.Name = "PicBxPlat11_2"
        Me.PicBxPlat11_2.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat11_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat11_2.TabIndex = 603
        Me.PicBxPlat11_2.TabStop = False
        '
        'PicBxPlat11_1
        '
        Me.PicBxPlat11_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat11_1.Location = New System.Drawing.Point(16, 480)
        Me.PicBxPlat11_1.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat11_1.Name = "PicBxPlat11_1"
        Me.PicBxPlat11_1.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat11_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat11_1.TabIndex = 602
        Me.PicBxPlat11_1.TabStop = False
        '
        'PicBxPlat10_20
        '
        Me.PicBxPlat10_20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat10_20.Location = New System.Drawing.Point(1004, 432)
        Me.PicBxPlat10_20.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat10_20.Name = "PicBxPlat10_20"
        Me.PicBxPlat10_20.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat10_20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat10_20.TabIndex = 601
        Me.PicBxPlat10_20.TabStop = False
        '
        'PicBxPlat10_19
        '
        Me.PicBxPlat10_19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat10_19.Location = New System.Drawing.Point(952, 432)
        Me.PicBxPlat10_19.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat10_19.Name = "PicBxPlat10_19"
        Me.PicBxPlat10_19.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat10_19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat10_19.TabIndex = 600
        Me.PicBxPlat10_19.TabStop = False
        '
        'PicBxPlat10_18
        '
        Me.PicBxPlat10_18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat10_18.Location = New System.Drawing.Point(900, 432)
        Me.PicBxPlat10_18.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat10_18.Name = "PicBxPlat10_18"
        Me.PicBxPlat10_18.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat10_18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat10_18.TabIndex = 599
        Me.PicBxPlat10_18.TabStop = False
        '
        'PicBxPlat10_17
        '
        Me.PicBxPlat10_17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat10_17.Location = New System.Drawing.Point(848, 432)
        Me.PicBxPlat10_17.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat10_17.Name = "PicBxPlat10_17"
        Me.PicBxPlat10_17.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat10_17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat10_17.TabIndex = 598
        Me.PicBxPlat10_17.TabStop = False
        '
        'PicBxPlat10_16
        '
        Me.PicBxPlat10_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat10_16.Location = New System.Drawing.Point(796, 432)
        Me.PicBxPlat10_16.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat10_16.Name = "PicBxPlat10_16"
        Me.PicBxPlat10_16.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat10_16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat10_16.TabIndex = 597
        Me.PicBxPlat10_16.TabStop = False
        '
        'PicBxPlat10_15
        '
        Me.PicBxPlat10_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat10_15.Location = New System.Drawing.Point(744, 432)
        Me.PicBxPlat10_15.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat10_15.Name = "PicBxPlat10_15"
        Me.PicBxPlat10_15.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat10_15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat10_15.TabIndex = 596
        Me.PicBxPlat10_15.TabStop = False
        '
        'PicBxPlat10_14
        '
        Me.PicBxPlat10_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat10_14.Location = New System.Drawing.Point(692, 432)
        Me.PicBxPlat10_14.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat10_14.Name = "PicBxPlat10_14"
        Me.PicBxPlat10_14.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat10_14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat10_14.TabIndex = 595
        Me.PicBxPlat10_14.TabStop = False
        '
        'PicBxPlat10_13
        '
        Me.PicBxPlat10_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat10_13.Location = New System.Drawing.Point(640, 432)
        Me.PicBxPlat10_13.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat10_13.Name = "PicBxPlat10_13"
        Me.PicBxPlat10_13.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat10_13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat10_13.TabIndex = 594
        Me.PicBxPlat10_13.TabStop = False
        '
        'PicBxPlat10_12
        '
        Me.PicBxPlat10_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat10_12.Location = New System.Drawing.Point(588, 432)
        Me.PicBxPlat10_12.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat10_12.Name = "PicBxPlat10_12"
        Me.PicBxPlat10_12.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat10_12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat10_12.TabIndex = 593
        Me.PicBxPlat10_12.TabStop = False
        '
        'PicBxPlat10_11
        '
        Me.PicBxPlat10_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat10_11.Location = New System.Drawing.Point(536, 432)
        Me.PicBxPlat10_11.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat10_11.Name = "PicBxPlat10_11"
        Me.PicBxPlat10_11.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat10_11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat10_11.TabIndex = 592
        Me.PicBxPlat10_11.TabStop = False
        '
        'PicBxPlat10_10
        '
        Me.PicBxPlat10_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat10_10.Location = New System.Drawing.Point(484, 432)
        Me.PicBxPlat10_10.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat10_10.Name = "PicBxPlat10_10"
        Me.PicBxPlat10_10.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat10_10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat10_10.TabIndex = 591
        Me.PicBxPlat10_10.TabStop = False
        '
        'PicBxPlat10_9
        '
        Me.PicBxPlat10_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat10_9.Location = New System.Drawing.Point(432, 432)
        Me.PicBxPlat10_9.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat10_9.Name = "PicBxPlat10_9"
        Me.PicBxPlat10_9.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat10_9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat10_9.TabIndex = 590
        Me.PicBxPlat10_9.TabStop = False
        '
        'PicBxPlat10_8
        '
        Me.PicBxPlat10_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat10_8.Location = New System.Drawing.Point(380, 432)
        Me.PicBxPlat10_8.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat10_8.Name = "PicBxPlat10_8"
        Me.PicBxPlat10_8.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat10_8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat10_8.TabIndex = 589
        Me.PicBxPlat10_8.TabStop = False
        '
        'PicBxPlat10_7
        '
        Me.PicBxPlat10_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat10_7.Location = New System.Drawing.Point(328, 432)
        Me.PicBxPlat10_7.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat10_7.Name = "PicBxPlat10_7"
        Me.PicBxPlat10_7.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat10_7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat10_7.TabIndex = 588
        Me.PicBxPlat10_7.TabStop = False
        '
        'PicBxPlat10_6
        '
        Me.PicBxPlat10_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat10_6.Location = New System.Drawing.Point(276, 432)
        Me.PicBxPlat10_6.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat10_6.Name = "PicBxPlat10_6"
        Me.PicBxPlat10_6.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat10_6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat10_6.TabIndex = 587
        Me.PicBxPlat10_6.TabStop = False
        '
        'PicBxPlat10_5
        '
        Me.PicBxPlat10_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat10_5.Location = New System.Drawing.Point(224, 432)
        Me.PicBxPlat10_5.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat10_5.Name = "PicBxPlat10_5"
        Me.PicBxPlat10_5.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat10_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat10_5.TabIndex = 586
        Me.PicBxPlat10_5.TabStop = False
        '
        'PicBxPlat10_4
        '
        Me.PicBxPlat10_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat10_4.Location = New System.Drawing.Point(172, 432)
        Me.PicBxPlat10_4.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat10_4.Name = "PicBxPlat10_4"
        Me.PicBxPlat10_4.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat10_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat10_4.TabIndex = 585
        Me.PicBxPlat10_4.TabStop = False
        '
        'PicBxPlat10_3
        '
        Me.PicBxPlat10_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat10_3.Location = New System.Drawing.Point(120, 432)
        Me.PicBxPlat10_3.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat10_3.Name = "PicBxPlat10_3"
        Me.PicBxPlat10_3.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat10_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat10_3.TabIndex = 584
        Me.PicBxPlat10_3.TabStop = False
        '
        'PicBxPlat10_2
        '
        Me.PicBxPlat10_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat10_2.Location = New System.Drawing.Point(68, 432)
        Me.PicBxPlat10_2.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat10_2.Name = "PicBxPlat10_2"
        Me.PicBxPlat10_2.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat10_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat10_2.TabIndex = 583
        Me.PicBxPlat10_2.TabStop = False
        '
        'PicBxPlat10_1
        '
        Me.PicBxPlat10_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat10_1.Location = New System.Drawing.Point(16, 432)
        Me.PicBxPlat10_1.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat10_1.Name = "PicBxPlat10_1"
        Me.PicBxPlat10_1.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat10_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat10_1.TabIndex = 582
        Me.PicBxPlat10_1.TabStop = False
        '
        'PicBxPlat9_20
        '
        Me.PicBxPlat9_20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat9_20.Location = New System.Drawing.Point(1004, 384)
        Me.PicBxPlat9_20.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat9_20.Name = "PicBxPlat9_20"
        Me.PicBxPlat9_20.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat9_20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat9_20.TabIndex = 581
        Me.PicBxPlat9_20.TabStop = False
        '
        'PicBxPlat9_19
        '
        Me.PicBxPlat9_19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat9_19.Location = New System.Drawing.Point(952, 384)
        Me.PicBxPlat9_19.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat9_19.Name = "PicBxPlat9_19"
        Me.PicBxPlat9_19.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat9_19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat9_19.TabIndex = 580
        Me.PicBxPlat9_19.TabStop = False
        '
        'PicBxPlat9_18
        '
        Me.PicBxPlat9_18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat9_18.Location = New System.Drawing.Point(900, 384)
        Me.PicBxPlat9_18.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat9_18.Name = "PicBxPlat9_18"
        Me.PicBxPlat9_18.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat9_18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat9_18.TabIndex = 579
        Me.PicBxPlat9_18.TabStop = False
        '
        'PicBxPlat9_17
        '
        Me.PicBxPlat9_17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat9_17.Location = New System.Drawing.Point(848, 384)
        Me.PicBxPlat9_17.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat9_17.Name = "PicBxPlat9_17"
        Me.PicBxPlat9_17.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat9_17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat9_17.TabIndex = 578
        Me.PicBxPlat9_17.TabStop = False
        '
        'PicBxPlat9_16
        '
        Me.PicBxPlat9_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat9_16.Location = New System.Drawing.Point(796, 384)
        Me.PicBxPlat9_16.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat9_16.Name = "PicBxPlat9_16"
        Me.PicBxPlat9_16.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat9_16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat9_16.TabIndex = 577
        Me.PicBxPlat9_16.TabStop = False
        '
        'PicBxPlat9_15
        '
        Me.PicBxPlat9_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat9_15.Location = New System.Drawing.Point(744, 384)
        Me.PicBxPlat9_15.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat9_15.Name = "PicBxPlat9_15"
        Me.PicBxPlat9_15.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat9_15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat9_15.TabIndex = 576
        Me.PicBxPlat9_15.TabStop = False
        '
        'PicBxPlat9_14
        '
        Me.PicBxPlat9_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat9_14.Location = New System.Drawing.Point(692, 384)
        Me.PicBxPlat9_14.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat9_14.Name = "PicBxPlat9_14"
        Me.PicBxPlat9_14.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat9_14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat9_14.TabIndex = 575
        Me.PicBxPlat9_14.TabStop = False
        '
        'PicBxPlat9_13
        '
        Me.PicBxPlat9_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat9_13.Location = New System.Drawing.Point(640, 384)
        Me.PicBxPlat9_13.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat9_13.Name = "PicBxPlat9_13"
        Me.PicBxPlat9_13.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat9_13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat9_13.TabIndex = 574
        Me.PicBxPlat9_13.TabStop = False
        '
        'PicBxPlat9_12
        '
        Me.PicBxPlat9_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat9_12.Location = New System.Drawing.Point(588, 384)
        Me.PicBxPlat9_12.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat9_12.Name = "PicBxPlat9_12"
        Me.PicBxPlat9_12.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat9_12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat9_12.TabIndex = 573
        Me.PicBxPlat9_12.TabStop = False
        '
        'PicBxPlat9_11
        '
        Me.PicBxPlat9_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat9_11.Location = New System.Drawing.Point(536, 384)
        Me.PicBxPlat9_11.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat9_11.Name = "PicBxPlat9_11"
        Me.PicBxPlat9_11.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat9_11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat9_11.TabIndex = 572
        Me.PicBxPlat9_11.TabStop = False
        '
        'PicBxPlat9_10
        '
        Me.PicBxPlat9_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat9_10.Location = New System.Drawing.Point(484, 384)
        Me.PicBxPlat9_10.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat9_10.Name = "PicBxPlat9_10"
        Me.PicBxPlat9_10.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat9_10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat9_10.TabIndex = 571
        Me.PicBxPlat9_10.TabStop = False
        '
        'PicBxPlat9_9
        '
        Me.PicBxPlat9_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat9_9.Location = New System.Drawing.Point(432, 384)
        Me.PicBxPlat9_9.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat9_9.Name = "PicBxPlat9_9"
        Me.PicBxPlat9_9.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat9_9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat9_9.TabIndex = 570
        Me.PicBxPlat9_9.TabStop = False
        '
        'PicBxPlat9_8
        '
        Me.PicBxPlat9_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat9_8.Location = New System.Drawing.Point(380, 384)
        Me.PicBxPlat9_8.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat9_8.Name = "PicBxPlat9_8"
        Me.PicBxPlat9_8.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat9_8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat9_8.TabIndex = 569
        Me.PicBxPlat9_8.TabStop = False
        '
        'PicBxPlat9_7
        '
        Me.PicBxPlat9_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat9_7.Location = New System.Drawing.Point(328, 384)
        Me.PicBxPlat9_7.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat9_7.Name = "PicBxPlat9_7"
        Me.PicBxPlat9_7.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat9_7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat9_7.TabIndex = 568
        Me.PicBxPlat9_7.TabStop = False
        '
        'PicBxPlat9_6
        '
        Me.PicBxPlat9_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat9_6.Location = New System.Drawing.Point(276, 384)
        Me.PicBxPlat9_6.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat9_6.Name = "PicBxPlat9_6"
        Me.PicBxPlat9_6.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat9_6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat9_6.TabIndex = 567
        Me.PicBxPlat9_6.TabStop = False
        '
        'PicBxPlat9_5
        '
        Me.PicBxPlat9_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat9_5.Location = New System.Drawing.Point(224, 384)
        Me.PicBxPlat9_5.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat9_5.Name = "PicBxPlat9_5"
        Me.PicBxPlat9_5.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat9_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat9_5.TabIndex = 566
        Me.PicBxPlat9_5.TabStop = False
        '
        'PicBxPlat9_4
        '
        Me.PicBxPlat9_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat9_4.Location = New System.Drawing.Point(172, 384)
        Me.PicBxPlat9_4.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat9_4.Name = "PicBxPlat9_4"
        Me.PicBxPlat9_4.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat9_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat9_4.TabIndex = 565
        Me.PicBxPlat9_4.TabStop = False
        '
        'PicBxPlat9_3
        '
        Me.PicBxPlat9_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat9_3.Location = New System.Drawing.Point(120, 384)
        Me.PicBxPlat9_3.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat9_3.Name = "PicBxPlat9_3"
        Me.PicBxPlat9_3.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat9_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat9_3.TabIndex = 564
        Me.PicBxPlat9_3.TabStop = False
        '
        'PicBxPlat9_2
        '
        Me.PicBxPlat9_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat9_2.Location = New System.Drawing.Point(68, 384)
        Me.PicBxPlat9_2.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat9_2.Name = "PicBxPlat9_2"
        Me.PicBxPlat9_2.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat9_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat9_2.TabIndex = 563
        Me.PicBxPlat9_2.TabStop = False
        '
        'PicBxPlat9_1
        '
        Me.PicBxPlat9_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat9_1.Location = New System.Drawing.Point(16, 384)
        Me.PicBxPlat9_1.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat9_1.Name = "PicBxPlat9_1"
        Me.PicBxPlat9_1.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat9_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat9_1.TabIndex = 562
        Me.PicBxPlat9_1.TabStop = False
        '
        'PicBxPlat8_20
        '
        Me.PicBxPlat8_20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat8_20.Location = New System.Drawing.Point(1004, 336)
        Me.PicBxPlat8_20.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat8_20.Name = "PicBxPlat8_20"
        Me.PicBxPlat8_20.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat8_20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat8_20.TabIndex = 561
        Me.PicBxPlat8_20.TabStop = False
        '
        'PicBxPlat8_19
        '
        Me.PicBxPlat8_19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat8_19.Location = New System.Drawing.Point(952, 336)
        Me.PicBxPlat8_19.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat8_19.Name = "PicBxPlat8_19"
        Me.PicBxPlat8_19.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat8_19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat8_19.TabIndex = 560
        Me.PicBxPlat8_19.TabStop = False
        '
        'PicBxPlat8_18
        '
        Me.PicBxPlat8_18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat8_18.Location = New System.Drawing.Point(900, 336)
        Me.PicBxPlat8_18.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat8_18.Name = "PicBxPlat8_18"
        Me.PicBxPlat8_18.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat8_18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat8_18.TabIndex = 559
        Me.PicBxPlat8_18.TabStop = False
        '
        'PicBxPlat8_17
        '
        Me.PicBxPlat8_17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat8_17.Location = New System.Drawing.Point(848, 336)
        Me.PicBxPlat8_17.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat8_17.Name = "PicBxPlat8_17"
        Me.PicBxPlat8_17.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat8_17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat8_17.TabIndex = 558
        Me.PicBxPlat8_17.TabStop = False
        '
        'PicBxPlat8_16
        '
        Me.PicBxPlat8_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat8_16.Location = New System.Drawing.Point(796, 336)
        Me.PicBxPlat8_16.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat8_16.Name = "PicBxPlat8_16"
        Me.PicBxPlat8_16.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat8_16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat8_16.TabIndex = 557
        Me.PicBxPlat8_16.TabStop = False
        '
        'PicBxPlat8_15
        '
        Me.PicBxPlat8_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat8_15.Location = New System.Drawing.Point(744, 336)
        Me.PicBxPlat8_15.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat8_15.Name = "PicBxPlat8_15"
        Me.PicBxPlat8_15.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat8_15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat8_15.TabIndex = 556
        Me.PicBxPlat8_15.TabStop = False
        '
        'PicBxPlat8_14
        '
        Me.PicBxPlat8_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat8_14.Location = New System.Drawing.Point(692, 336)
        Me.PicBxPlat8_14.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat8_14.Name = "PicBxPlat8_14"
        Me.PicBxPlat8_14.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat8_14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat8_14.TabIndex = 555
        Me.PicBxPlat8_14.TabStop = False
        '
        'PicBxPlat8_13
        '
        Me.PicBxPlat8_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat8_13.Location = New System.Drawing.Point(640, 336)
        Me.PicBxPlat8_13.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat8_13.Name = "PicBxPlat8_13"
        Me.PicBxPlat8_13.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat8_13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat8_13.TabIndex = 554
        Me.PicBxPlat8_13.TabStop = False
        '
        'PicBxPlat8_12
        '
        Me.PicBxPlat8_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat8_12.Location = New System.Drawing.Point(588, 336)
        Me.PicBxPlat8_12.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat8_12.Name = "PicBxPlat8_12"
        Me.PicBxPlat8_12.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat8_12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat8_12.TabIndex = 553
        Me.PicBxPlat8_12.TabStop = False
        '
        'PicBxPlat8_11
        '
        Me.PicBxPlat8_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat8_11.Location = New System.Drawing.Point(536, 336)
        Me.PicBxPlat8_11.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat8_11.Name = "PicBxPlat8_11"
        Me.PicBxPlat8_11.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat8_11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat8_11.TabIndex = 552
        Me.PicBxPlat8_11.TabStop = False
        '
        'PicBxPlat8_10
        '
        Me.PicBxPlat8_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat8_10.Location = New System.Drawing.Point(484, 336)
        Me.PicBxPlat8_10.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat8_10.Name = "PicBxPlat8_10"
        Me.PicBxPlat8_10.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat8_10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat8_10.TabIndex = 551
        Me.PicBxPlat8_10.TabStop = False
        '
        'PicBxPlat8_9
        '
        Me.PicBxPlat8_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat8_9.Location = New System.Drawing.Point(432, 336)
        Me.PicBxPlat8_9.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat8_9.Name = "PicBxPlat8_9"
        Me.PicBxPlat8_9.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat8_9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat8_9.TabIndex = 550
        Me.PicBxPlat8_9.TabStop = False
        '
        'PicBxPlat8_8
        '
        Me.PicBxPlat8_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat8_8.Location = New System.Drawing.Point(380, 336)
        Me.PicBxPlat8_8.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat8_8.Name = "PicBxPlat8_8"
        Me.PicBxPlat8_8.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat8_8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat8_8.TabIndex = 549
        Me.PicBxPlat8_8.TabStop = False
        '
        'PicBxPlat8_7
        '
        Me.PicBxPlat8_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat8_7.Location = New System.Drawing.Point(328, 336)
        Me.PicBxPlat8_7.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat8_7.Name = "PicBxPlat8_7"
        Me.PicBxPlat8_7.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat8_7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat8_7.TabIndex = 548
        Me.PicBxPlat8_7.TabStop = False
        '
        'PicBxPlat8_6
        '
        Me.PicBxPlat8_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat8_6.Location = New System.Drawing.Point(276, 336)
        Me.PicBxPlat8_6.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat8_6.Name = "PicBxPlat8_6"
        Me.PicBxPlat8_6.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat8_6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat8_6.TabIndex = 547
        Me.PicBxPlat8_6.TabStop = False
        '
        'PicBxPlat8_5
        '
        Me.PicBxPlat8_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat8_5.Location = New System.Drawing.Point(224, 336)
        Me.PicBxPlat8_5.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat8_5.Name = "PicBxPlat8_5"
        Me.PicBxPlat8_5.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat8_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat8_5.TabIndex = 546
        Me.PicBxPlat8_5.TabStop = False
        '
        'PicBxPlat8_4
        '
        Me.PicBxPlat8_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat8_4.Location = New System.Drawing.Point(172, 336)
        Me.PicBxPlat8_4.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat8_4.Name = "PicBxPlat8_4"
        Me.PicBxPlat8_4.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat8_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat8_4.TabIndex = 545
        Me.PicBxPlat8_4.TabStop = False
        '
        'PicBxPlat8_3
        '
        Me.PicBxPlat8_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat8_3.Location = New System.Drawing.Point(120, 336)
        Me.PicBxPlat8_3.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat8_3.Name = "PicBxPlat8_3"
        Me.PicBxPlat8_3.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat8_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat8_3.TabIndex = 544
        Me.PicBxPlat8_3.TabStop = False
        '
        'PicBxPlat8_2
        '
        Me.PicBxPlat8_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat8_2.Location = New System.Drawing.Point(68, 336)
        Me.PicBxPlat8_2.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat8_2.Name = "PicBxPlat8_2"
        Me.PicBxPlat8_2.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat8_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat8_2.TabIndex = 543
        Me.PicBxPlat8_2.TabStop = False
        '
        'PicBxPlat8_1
        '
        Me.PicBxPlat8_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat8_1.Location = New System.Drawing.Point(16, 336)
        Me.PicBxPlat8_1.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat8_1.Name = "PicBxPlat8_1"
        Me.PicBxPlat8_1.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat8_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat8_1.TabIndex = 542
        Me.PicBxPlat8_1.TabStop = False
        '
        'PicBxPlat7_20
        '
        Me.PicBxPlat7_20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat7_20.Location = New System.Drawing.Point(1004, 288)
        Me.PicBxPlat7_20.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat7_20.Name = "PicBxPlat7_20"
        Me.PicBxPlat7_20.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat7_20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat7_20.TabIndex = 541
        Me.PicBxPlat7_20.TabStop = False
        '
        'PicBxPlat7_19
        '
        Me.PicBxPlat7_19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat7_19.Location = New System.Drawing.Point(952, 288)
        Me.PicBxPlat7_19.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat7_19.Name = "PicBxPlat7_19"
        Me.PicBxPlat7_19.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat7_19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat7_19.TabIndex = 540
        Me.PicBxPlat7_19.TabStop = False
        '
        'PicBxPlat7_18
        '
        Me.PicBxPlat7_18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat7_18.Location = New System.Drawing.Point(900, 288)
        Me.PicBxPlat7_18.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat7_18.Name = "PicBxPlat7_18"
        Me.PicBxPlat7_18.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat7_18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat7_18.TabIndex = 539
        Me.PicBxPlat7_18.TabStop = False
        '
        'PicBxPlat7_17
        '
        Me.PicBxPlat7_17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat7_17.Location = New System.Drawing.Point(848, 288)
        Me.PicBxPlat7_17.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat7_17.Name = "PicBxPlat7_17"
        Me.PicBxPlat7_17.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat7_17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat7_17.TabIndex = 538
        Me.PicBxPlat7_17.TabStop = False
        '
        'PicBxPlat7_16
        '
        Me.PicBxPlat7_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat7_16.Location = New System.Drawing.Point(796, 288)
        Me.PicBxPlat7_16.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat7_16.Name = "PicBxPlat7_16"
        Me.PicBxPlat7_16.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat7_16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat7_16.TabIndex = 537
        Me.PicBxPlat7_16.TabStop = False
        '
        'PicBxPlat7_15
        '
        Me.PicBxPlat7_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat7_15.Location = New System.Drawing.Point(744, 288)
        Me.PicBxPlat7_15.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat7_15.Name = "PicBxPlat7_15"
        Me.PicBxPlat7_15.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat7_15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat7_15.TabIndex = 536
        Me.PicBxPlat7_15.TabStop = False
        '
        'PicBxPlat7_14
        '
        Me.PicBxPlat7_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat7_14.Location = New System.Drawing.Point(692, 288)
        Me.PicBxPlat7_14.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat7_14.Name = "PicBxPlat7_14"
        Me.PicBxPlat7_14.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat7_14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat7_14.TabIndex = 535
        Me.PicBxPlat7_14.TabStop = False
        '
        'PicBxPlat7_13
        '
        Me.PicBxPlat7_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat7_13.Location = New System.Drawing.Point(640, 288)
        Me.PicBxPlat7_13.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat7_13.Name = "PicBxPlat7_13"
        Me.PicBxPlat7_13.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat7_13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat7_13.TabIndex = 534
        Me.PicBxPlat7_13.TabStop = False
        '
        'PicBxPlat7_12
        '
        Me.PicBxPlat7_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat7_12.Location = New System.Drawing.Point(588, 288)
        Me.PicBxPlat7_12.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat7_12.Name = "PicBxPlat7_12"
        Me.PicBxPlat7_12.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat7_12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat7_12.TabIndex = 533
        Me.PicBxPlat7_12.TabStop = False
        '
        'PicBxPlat7_11
        '
        Me.PicBxPlat7_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat7_11.Location = New System.Drawing.Point(536, 288)
        Me.PicBxPlat7_11.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat7_11.Name = "PicBxPlat7_11"
        Me.PicBxPlat7_11.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat7_11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat7_11.TabIndex = 532
        Me.PicBxPlat7_11.TabStop = False
        '
        'PicBxPlat7_10
        '
        Me.PicBxPlat7_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat7_10.Location = New System.Drawing.Point(484, 288)
        Me.PicBxPlat7_10.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat7_10.Name = "PicBxPlat7_10"
        Me.PicBxPlat7_10.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat7_10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat7_10.TabIndex = 531
        Me.PicBxPlat7_10.TabStop = False
        '
        'PicBxPlat7_9
        '
        Me.PicBxPlat7_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat7_9.Location = New System.Drawing.Point(432, 288)
        Me.PicBxPlat7_9.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat7_9.Name = "PicBxPlat7_9"
        Me.PicBxPlat7_9.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat7_9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat7_9.TabIndex = 530
        Me.PicBxPlat7_9.TabStop = False
        '
        'PicBxPlat7_8
        '
        Me.PicBxPlat7_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat7_8.Location = New System.Drawing.Point(380, 288)
        Me.PicBxPlat7_8.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat7_8.Name = "PicBxPlat7_8"
        Me.PicBxPlat7_8.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat7_8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat7_8.TabIndex = 529
        Me.PicBxPlat7_8.TabStop = False
        '
        'PicBxPlat7_7
        '
        Me.PicBxPlat7_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat7_7.Location = New System.Drawing.Point(328, 288)
        Me.PicBxPlat7_7.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat7_7.Name = "PicBxPlat7_7"
        Me.PicBxPlat7_7.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat7_7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat7_7.TabIndex = 528
        Me.PicBxPlat7_7.TabStop = False
        '
        'PicBxPlat7_6
        '
        Me.PicBxPlat7_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat7_6.Location = New System.Drawing.Point(276, 288)
        Me.PicBxPlat7_6.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat7_6.Name = "PicBxPlat7_6"
        Me.PicBxPlat7_6.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat7_6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat7_6.TabIndex = 527
        Me.PicBxPlat7_6.TabStop = False
        '
        'PicBxPlat7_5
        '
        Me.PicBxPlat7_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat7_5.Location = New System.Drawing.Point(224, 288)
        Me.PicBxPlat7_5.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat7_5.Name = "PicBxPlat7_5"
        Me.PicBxPlat7_5.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat7_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat7_5.TabIndex = 526
        Me.PicBxPlat7_5.TabStop = False
        '
        'PicBxPlat7_4
        '
        Me.PicBxPlat7_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat7_4.Location = New System.Drawing.Point(172, 288)
        Me.PicBxPlat7_4.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat7_4.Name = "PicBxPlat7_4"
        Me.PicBxPlat7_4.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat7_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat7_4.TabIndex = 525
        Me.PicBxPlat7_4.TabStop = False
        '
        'PicBxPlat7_3
        '
        Me.PicBxPlat7_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat7_3.Location = New System.Drawing.Point(120, 288)
        Me.PicBxPlat7_3.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat7_3.Name = "PicBxPlat7_3"
        Me.PicBxPlat7_3.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat7_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat7_3.TabIndex = 524
        Me.PicBxPlat7_3.TabStop = False
        '
        'PicBxPlat7_2
        '
        Me.PicBxPlat7_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat7_2.Location = New System.Drawing.Point(68, 288)
        Me.PicBxPlat7_2.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat7_2.Name = "PicBxPlat7_2"
        Me.PicBxPlat7_2.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat7_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat7_2.TabIndex = 523
        Me.PicBxPlat7_2.TabStop = False
        '
        'PicBxPlat7_1
        '
        Me.PicBxPlat7_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat7_1.Location = New System.Drawing.Point(16, 288)
        Me.PicBxPlat7_1.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat7_1.Name = "PicBxPlat7_1"
        Me.PicBxPlat7_1.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat7_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat7_1.TabIndex = 522
        Me.PicBxPlat7_1.TabStop = False
        '
        'PicBxPlat6_20
        '
        Me.PicBxPlat6_20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat6_20.Location = New System.Drawing.Point(1004, 244)
        Me.PicBxPlat6_20.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat6_20.Name = "PicBxPlat6_20"
        Me.PicBxPlat6_20.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat6_20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat6_20.TabIndex = 521
        Me.PicBxPlat6_20.TabStop = False
        '
        'PicBxPlat6_19
        '
        Me.PicBxPlat6_19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat6_19.Location = New System.Drawing.Point(952, 244)
        Me.PicBxPlat6_19.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat6_19.Name = "PicBxPlat6_19"
        Me.PicBxPlat6_19.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat6_19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat6_19.TabIndex = 520
        Me.PicBxPlat6_19.TabStop = False
        '
        'PicBxPlat6_18
        '
        Me.PicBxPlat6_18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat6_18.Location = New System.Drawing.Point(900, 244)
        Me.PicBxPlat6_18.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat6_18.Name = "PicBxPlat6_18"
        Me.PicBxPlat6_18.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat6_18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat6_18.TabIndex = 519
        Me.PicBxPlat6_18.TabStop = False
        '
        'PicBxPlat6_17
        '
        Me.PicBxPlat6_17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat6_17.Location = New System.Drawing.Point(848, 244)
        Me.PicBxPlat6_17.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat6_17.Name = "PicBxPlat6_17"
        Me.PicBxPlat6_17.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat6_17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat6_17.TabIndex = 518
        Me.PicBxPlat6_17.TabStop = False
        '
        'PicBxPlat6_16
        '
        Me.PicBxPlat6_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat6_16.Location = New System.Drawing.Point(796, 244)
        Me.PicBxPlat6_16.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat6_16.Name = "PicBxPlat6_16"
        Me.PicBxPlat6_16.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat6_16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat6_16.TabIndex = 517
        Me.PicBxPlat6_16.TabStop = False
        '
        'PicBxPlat6_15
        '
        Me.PicBxPlat6_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat6_15.Location = New System.Drawing.Point(744, 244)
        Me.PicBxPlat6_15.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat6_15.Name = "PicBxPlat6_15"
        Me.PicBxPlat6_15.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat6_15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat6_15.TabIndex = 516
        Me.PicBxPlat6_15.TabStop = False
        '
        'PicBxPlat6_14
        '
        Me.PicBxPlat6_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat6_14.Location = New System.Drawing.Point(692, 244)
        Me.PicBxPlat6_14.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat6_14.Name = "PicBxPlat6_14"
        Me.PicBxPlat6_14.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat6_14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat6_14.TabIndex = 515
        Me.PicBxPlat6_14.TabStop = False
        '
        'PicBxPlat6_13
        '
        Me.PicBxPlat6_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat6_13.Location = New System.Drawing.Point(640, 244)
        Me.PicBxPlat6_13.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat6_13.Name = "PicBxPlat6_13"
        Me.PicBxPlat6_13.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat6_13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat6_13.TabIndex = 514
        Me.PicBxPlat6_13.TabStop = False
        '
        'PicBxPlat6_12
        '
        Me.PicBxPlat6_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat6_12.Location = New System.Drawing.Point(588, 244)
        Me.PicBxPlat6_12.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat6_12.Name = "PicBxPlat6_12"
        Me.PicBxPlat6_12.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat6_12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat6_12.TabIndex = 513
        Me.PicBxPlat6_12.TabStop = False
        '
        'PicBxPlat6_11
        '
        Me.PicBxPlat6_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat6_11.Location = New System.Drawing.Point(536, 244)
        Me.PicBxPlat6_11.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat6_11.Name = "PicBxPlat6_11"
        Me.PicBxPlat6_11.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat6_11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat6_11.TabIndex = 512
        Me.PicBxPlat6_11.TabStop = False
        '
        'PicBxPlat6_10
        '
        Me.PicBxPlat6_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat6_10.Location = New System.Drawing.Point(484, 244)
        Me.PicBxPlat6_10.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat6_10.Name = "PicBxPlat6_10"
        Me.PicBxPlat6_10.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat6_10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat6_10.TabIndex = 511
        Me.PicBxPlat6_10.TabStop = False
        '
        'PicBxPlat6_9
        '
        Me.PicBxPlat6_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat6_9.Location = New System.Drawing.Point(432, 244)
        Me.PicBxPlat6_9.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat6_9.Name = "PicBxPlat6_9"
        Me.PicBxPlat6_9.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat6_9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat6_9.TabIndex = 510
        Me.PicBxPlat6_9.TabStop = False
        '
        'PicBxPlat6_8
        '
        Me.PicBxPlat6_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat6_8.Location = New System.Drawing.Point(380, 244)
        Me.PicBxPlat6_8.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat6_8.Name = "PicBxPlat6_8"
        Me.PicBxPlat6_8.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat6_8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat6_8.TabIndex = 509
        Me.PicBxPlat6_8.TabStop = False
        '
        'PicBxPlat6_7
        '
        Me.PicBxPlat6_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat6_7.Location = New System.Drawing.Point(328, 244)
        Me.PicBxPlat6_7.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat6_7.Name = "PicBxPlat6_7"
        Me.PicBxPlat6_7.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat6_7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat6_7.TabIndex = 508
        Me.PicBxPlat6_7.TabStop = False
        '
        'PicBxPlat6_6
        '
        Me.PicBxPlat6_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat6_6.Location = New System.Drawing.Point(276, 244)
        Me.PicBxPlat6_6.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat6_6.Name = "PicBxPlat6_6"
        Me.PicBxPlat6_6.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat6_6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat6_6.TabIndex = 507
        Me.PicBxPlat6_6.TabStop = False
        '
        'PicBxPlat6_5
        '
        Me.PicBxPlat6_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat6_5.Location = New System.Drawing.Point(224, 244)
        Me.PicBxPlat6_5.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat6_5.Name = "PicBxPlat6_5"
        Me.PicBxPlat6_5.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat6_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat6_5.TabIndex = 506
        Me.PicBxPlat6_5.TabStop = False
        '
        'PicBxPlat6_4
        '
        Me.PicBxPlat6_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat6_4.Location = New System.Drawing.Point(172, 244)
        Me.PicBxPlat6_4.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat6_4.Name = "PicBxPlat6_4"
        Me.PicBxPlat6_4.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat6_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat6_4.TabIndex = 505
        Me.PicBxPlat6_4.TabStop = False
        '
        'PicBxPlat6_3
        '
        Me.PicBxPlat6_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat6_3.Location = New System.Drawing.Point(120, 244)
        Me.PicBxPlat6_3.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat6_3.Name = "PicBxPlat6_3"
        Me.PicBxPlat6_3.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat6_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat6_3.TabIndex = 504
        Me.PicBxPlat6_3.TabStop = False
        '
        'PicBxPlat6_2
        '
        Me.PicBxPlat6_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat6_2.Location = New System.Drawing.Point(68, 244)
        Me.PicBxPlat6_2.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat6_2.Name = "PicBxPlat6_2"
        Me.PicBxPlat6_2.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat6_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat6_2.TabIndex = 503
        Me.PicBxPlat6_2.TabStop = False
        '
        'PicBxPlat6_1
        '
        Me.PicBxPlat6_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat6_1.Location = New System.Drawing.Point(16, 244)
        Me.PicBxPlat6_1.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat6_1.Name = "PicBxPlat6_1"
        Me.PicBxPlat6_1.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat6_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat6_1.TabIndex = 502
        Me.PicBxPlat6_1.TabStop = False
        '
        'PicBxPlat5_20
        '
        Me.PicBxPlat5_20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat5_20.Location = New System.Drawing.Point(1004, 196)
        Me.PicBxPlat5_20.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat5_20.Name = "PicBxPlat5_20"
        Me.PicBxPlat5_20.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat5_20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat5_20.TabIndex = 501
        Me.PicBxPlat5_20.TabStop = False
        '
        'PicBxPlat5_19
        '
        Me.PicBxPlat5_19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat5_19.Location = New System.Drawing.Point(952, 196)
        Me.PicBxPlat5_19.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat5_19.Name = "PicBxPlat5_19"
        Me.PicBxPlat5_19.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat5_19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat5_19.TabIndex = 500
        Me.PicBxPlat5_19.TabStop = False
        '
        'PicBxPlat5_18
        '
        Me.PicBxPlat5_18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat5_18.Location = New System.Drawing.Point(900, 196)
        Me.PicBxPlat5_18.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat5_18.Name = "PicBxPlat5_18"
        Me.PicBxPlat5_18.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat5_18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat5_18.TabIndex = 499
        Me.PicBxPlat5_18.TabStop = False
        '
        'PicBxPlat5_17
        '
        Me.PicBxPlat5_17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat5_17.Location = New System.Drawing.Point(848, 196)
        Me.PicBxPlat5_17.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat5_17.Name = "PicBxPlat5_17"
        Me.PicBxPlat5_17.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat5_17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat5_17.TabIndex = 498
        Me.PicBxPlat5_17.TabStop = False
        '
        'PicBxPlat5_16
        '
        Me.PicBxPlat5_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat5_16.Location = New System.Drawing.Point(796, 196)
        Me.PicBxPlat5_16.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat5_16.Name = "PicBxPlat5_16"
        Me.PicBxPlat5_16.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat5_16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat5_16.TabIndex = 497
        Me.PicBxPlat5_16.TabStop = False
        '
        'PicBxPlat5_15
        '
        Me.PicBxPlat5_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat5_15.Location = New System.Drawing.Point(744, 196)
        Me.PicBxPlat5_15.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat5_15.Name = "PicBxPlat5_15"
        Me.PicBxPlat5_15.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat5_15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat5_15.TabIndex = 496
        Me.PicBxPlat5_15.TabStop = False
        '
        'PicBxPlat5_14
        '
        Me.PicBxPlat5_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat5_14.Location = New System.Drawing.Point(692, 196)
        Me.PicBxPlat5_14.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat5_14.Name = "PicBxPlat5_14"
        Me.PicBxPlat5_14.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat5_14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat5_14.TabIndex = 495
        Me.PicBxPlat5_14.TabStop = False
        '
        'PicBxPlat5_13
        '
        Me.PicBxPlat5_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat5_13.Location = New System.Drawing.Point(640, 196)
        Me.PicBxPlat5_13.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat5_13.Name = "PicBxPlat5_13"
        Me.PicBxPlat5_13.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat5_13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat5_13.TabIndex = 494
        Me.PicBxPlat5_13.TabStop = False
        '
        'PicBxPlat5_12
        '
        Me.PicBxPlat5_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat5_12.Location = New System.Drawing.Point(588, 196)
        Me.PicBxPlat5_12.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat5_12.Name = "PicBxPlat5_12"
        Me.PicBxPlat5_12.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat5_12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat5_12.TabIndex = 493
        Me.PicBxPlat5_12.TabStop = False
        '
        'PicBxPlat5_11
        '
        Me.PicBxPlat5_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat5_11.Location = New System.Drawing.Point(536, 196)
        Me.PicBxPlat5_11.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat5_11.Name = "PicBxPlat5_11"
        Me.PicBxPlat5_11.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat5_11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat5_11.TabIndex = 492
        Me.PicBxPlat5_11.TabStop = False
        '
        'PicBxPlat5_10
        '
        Me.PicBxPlat5_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat5_10.Location = New System.Drawing.Point(484, 196)
        Me.PicBxPlat5_10.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat5_10.Name = "PicBxPlat5_10"
        Me.PicBxPlat5_10.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat5_10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat5_10.TabIndex = 491
        Me.PicBxPlat5_10.TabStop = False
        '
        'PicBxPlat5_9
        '
        Me.PicBxPlat5_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat5_9.Location = New System.Drawing.Point(432, 196)
        Me.PicBxPlat5_9.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat5_9.Name = "PicBxPlat5_9"
        Me.PicBxPlat5_9.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat5_9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat5_9.TabIndex = 490
        Me.PicBxPlat5_9.TabStop = False
        '
        'PicBxPlat5_8
        '
        Me.PicBxPlat5_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat5_8.Location = New System.Drawing.Point(380, 196)
        Me.PicBxPlat5_8.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat5_8.Name = "PicBxPlat5_8"
        Me.PicBxPlat5_8.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat5_8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat5_8.TabIndex = 489
        Me.PicBxPlat5_8.TabStop = False
        '
        'PicBxPlat5_7
        '
        Me.PicBxPlat5_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat5_7.Location = New System.Drawing.Point(328, 196)
        Me.PicBxPlat5_7.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat5_7.Name = "PicBxPlat5_7"
        Me.PicBxPlat5_7.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat5_7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat5_7.TabIndex = 488
        Me.PicBxPlat5_7.TabStop = False
        '
        'PicBxPlat5_6
        '
        Me.PicBxPlat5_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat5_6.Location = New System.Drawing.Point(276, 196)
        Me.PicBxPlat5_6.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat5_6.Name = "PicBxPlat5_6"
        Me.PicBxPlat5_6.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat5_6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat5_6.TabIndex = 487
        Me.PicBxPlat5_6.TabStop = False
        '
        'PicBxPlat5_5
        '
        Me.PicBxPlat5_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat5_5.Location = New System.Drawing.Point(224, 196)
        Me.PicBxPlat5_5.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat5_5.Name = "PicBxPlat5_5"
        Me.PicBxPlat5_5.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat5_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat5_5.TabIndex = 486
        Me.PicBxPlat5_5.TabStop = False
        '
        'PicBxPlat5_4
        '
        Me.PicBxPlat5_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat5_4.Location = New System.Drawing.Point(172, 196)
        Me.PicBxPlat5_4.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat5_4.Name = "PicBxPlat5_4"
        Me.PicBxPlat5_4.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat5_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat5_4.TabIndex = 485
        Me.PicBxPlat5_4.TabStop = False
        '
        'PicBxPlat5_3
        '
        Me.PicBxPlat5_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat5_3.Location = New System.Drawing.Point(120, 196)
        Me.PicBxPlat5_3.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat5_3.Name = "PicBxPlat5_3"
        Me.PicBxPlat5_3.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat5_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat5_3.TabIndex = 484
        Me.PicBxPlat5_3.TabStop = False
        '
        'PicBxPlat5_2
        '
        Me.PicBxPlat5_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat5_2.Location = New System.Drawing.Point(68, 196)
        Me.PicBxPlat5_2.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat5_2.Name = "PicBxPlat5_2"
        Me.PicBxPlat5_2.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat5_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat5_2.TabIndex = 483
        Me.PicBxPlat5_2.TabStop = False
        '
        'PicBxPlat5_1
        '
        Me.PicBxPlat5_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat5_1.Location = New System.Drawing.Point(16, 196)
        Me.PicBxPlat5_1.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat5_1.Name = "PicBxPlat5_1"
        Me.PicBxPlat5_1.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat5_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat5_1.TabIndex = 482
        Me.PicBxPlat5_1.TabStop = False
        '
        'PicBxPlat4_20
        '
        Me.PicBxPlat4_20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat4_20.Location = New System.Drawing.Point(1004, 148)
        Me.PicBxPlat4_20.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat4_20.Name = "PicBxPlat4_20"
        Me.PicBxPlat4_20.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat4_20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat4_20.TabIndex = 481
        Me.PicBxPlat4_20.TabStop = False
        '
        'PicBxPlat4_19
        '
        Me.PicBxPlat4_19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat4_19.Location = New System.Drawing.Point(952, 148)
        Me.PicBxPlat4_19.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat4_19.Name = "PicBxPlat4_19"
        Me.PicBxPlat4_19.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat4_19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat4_19.TabIndex = 480
        Me.PicBxPlat4_19.TabStop = False
        '
        'PicBxPlat4_18
        '
        Me.PicBxPlat4_18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat4_18.Location = New System.Drawing.Point(900, 148)
        Me.PicBxPlat4_18.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat4_18.Name = "PicBxPlat4_18"
        Me.PicBxPlat4_18.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat4_18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat4_18.TabIndex = 479
        Me.PicBxPlat4_18.TabStop = False
        '
        'PicBxPlat4_17
        '
        Me.PicBxPlat4_17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat4_17.Location = New System.Drawing.Point(848, 148)
        Me.PicBxPlat4_17.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat4_17.Name = "PicBxPlat4_17"
        Me.PicBxPlat4_17.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat4_17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat4_17.TabIndex = 478
        Me.PicBxPlat4_17.TabStop = False
        '
        'PicBxPlat4_16
        '
        Me.PicBxPlat4_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat4_16.Location = New System.Drawing.Point(796, 148)
        Me.PicBxPlat4_16.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat4_16.Name = "PicBxPlat4_16"
        Me.PicBxPlat4_16.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat4_16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat4_16.TabIndex = 477
        Me.PicBxPlat4_16.TabStop = False
        '
        'PicBxPlat4_15
        '
        Me.PicBxPlat4_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat4_15.Location = New System.Drawing.Point(744, 148)
        Me.PicBxPlat4_15.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat4_15.Name = "PicBxPlat4_15"
        Me.PicBxPlat4_15.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat4_15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat4_15.TabIndex = 476
        Me.PicBxPlat4_15.TabStop = False
        '
        'PicBxPlat4_14
        '
        Me.PicBxPlat4_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat4_14.Location = New System.Drawing.Point(692, 148)
        Me.PicBxPlat4_14.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat4_14.Name = "PicBxPlat4_14"
        Me.PicBxPlat4_14.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat4_14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat4_14.TabIndex = 475
        Me.PicBxPlat4_14.TabStop = False
        '
        'PicBxPlat4_13
        '
        Me.PicBxPlat4_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat4_13.Location = New System.Drawing.Point(640, 148)
        Me.PicBxPlat4_13.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat4_13.Name = "PicBxPlat4_13"
        Me.PicBxPlat4_13.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat4_13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat4_13.TabIndex = 474
        Me.PicBxPlat4_13.TabStop = False
        '
        'PicBxPlat4_12
        '
        Me.PicBxPlat4_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat4_12.Location = New System.Drawing.Point(588, 148)
        Me.PicBxPlat4_12.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat4_12.Name = "PicBxPlat4_12"
        Me.PicBxPlat4_12.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat4_12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat4_12.TabIndex = 473
        Me.PicBxPlat4_12.TabStop = False
        '
        'PicBxPlat4_11
        '
        Me.PicBxPlat4_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat4_11.Location = New System.Drawing.Point(536, 148)
        Me.PicBxPlat4_11.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat4_11.Name = "PicBxPlat4_11"
        Me.PicBxPlat4_11.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat4_11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat4_11.TabIndex = 472
        Me.PicBxPlat4_11.TabStop = False
        '
        'PicBxPlat4_10
        '
        Me.PicBxPlat4_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat4_10.Location = New System.Drawing.Point(484, 148)
        Me.PicBxPlat4_10.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat4_10.Name = "PicBxPlat4_10"
        Me.PicBxPlat4_10.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat4_10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat4_10.TabIndex = 471
        Me.PicBxPlat4_10.TabStop = False
        '
        'PicBxPlat4_9
        '
        Me.PicBxPlat4_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat4_9.Location = New System.Drawing.Point(432, 148)
        Me.PicBxPlat4_9.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat4_9.Name = "PicBxPlat4_9"
        Me.PicBxPlat4_9.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat4_9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat4_9.TabIndex = 470
        Me.PicBxPlat4_9.TabStop = False
        '
        'PicBxPlat4_8
        '
        Me.PicBxPlat4_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat4_8.Location = New System.Drawing.Point(380, 148)
        Me.PicBxPlat4_8.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat4_8.Name = "PicBxPlat4_8"
        Me.PicBxPlat4_8.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat4_8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat4_8.TabIndex = 469
        Me.PicBxPlat4_8.TabStop = False
        '
        'PicBxPlat4_7
        '
        Me.PicBxPlat4_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat4_7.Location = New System.Drawing.Point(328, 148)
        Me.PicBxPlat4_7.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat4_7.Name = "PicBxPlat4_7"
        Me.PicBxPlat4_7.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat4_7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat4_7.TabIndex = 468
        Me.PicBxPlat4_7.TabStop = False
        '
        'PicBxPlat4_6
        '
        Me.PicBxPlat4_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat4_6.Location = New System.Drawing.Point(276, 148)
        Me.PicBxPlat4_6.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat4_6.Name = "PicBxPlat4_6"
        Me.PicBxPlat4_6.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat4_6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat4_6.TabIndex = 467
        Me.PicBxPlat4_6.TabStop = False
        '
        'PicBxPlat4_5
        '
        Me.PicBxPlat4_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat4_5.Location = New System.Drawing.Point(224, 148)
        Me.PicBxPlat4_5.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat4_5.Name = "PicBxPlat4_5"
        Me.PicBxPlat4_5.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat4_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat4_5.TabIndex = 466
        Me.PicBxPlat4_5.TabStop = False
        '
        'PicBxPlat4_4
        '
        Me.PicBxPlat4_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat4_4.Location = New System.Drawing.Point(172, 148)
        Me.PicBxPlat4_4.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat4_4.Name = "PicBxPlat4_4"
        Me.PicBxPlat4_4.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat4_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat4_4.TabIndex = 465
        Me.PicBxPlat4_4.TabStop = False
        '
        'PicBxPlat4_3
        '
        Me.PicBxPlat4_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat4_3.Location = New System.Drawing.Point(120, 148)
        Me.PicBxPlat4_3.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat4_3.Name = "PicBxPlat4_3"
        Me.PicBxPlat4_3.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat4_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat4_3.TabIndex = 464
        Me.PicBxPlat4_3.TabStop = False
        '
        'PicBxPlat4_2
        '
        Me.PicBxPlat4_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat4_2.Location = New System.Drawing.Point(68, 148)
        Me.PicBxPlat4_2.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat4_2.Name = "PicBxPlat4_2"
        Me.PicBxPlat4_2.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat4_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat4_2.TabIndex = 463
        Me.PicBxPlat4_2.TabStop = False
        '
        'PicBxPlat4_1
        '
        Me.PicBxPlat4_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat4_1.Location = New System.Drawing.Point(16, 148)
        Me.PicBxPlat4_1.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat4_1.Name = "PicBxPlat4_1"
        Me.PicBxPlat4_1.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat4_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat4_1.TabIndex = 462
        Me.PicBxPlat4_1.TabStop = False
        '
        'PicBxPlat3_20
        '
        Me.PicBxPlat3_20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat3_20.Location = New System.Drawing.Point(1004, 100)
        Me.PicBxPlat3_20.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat3_20.Name = "PicBxPlat3_20"
        Me.PicBxPlat3_20.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat3_20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat3_20.TabIndex = 461
        Me.PicBxPlat3_20.TabStop = False
        '
        'PicBxPlat3_19
        '
        Me.PicBxPlat3_19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat3_19.Location = New System.Drawing.Point(952, 100)
        Me.PicBxPlat3_19.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat3_19.Name = "PicBxPlat3_19"
        Me.PicBxPlat3_19.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat3_19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat3_19.TabIndex = 460
        Me.PicBxPlat3_19.TabStop = False
        '
        'PicBxPlat3_18
        '
        Me.PicBxPlat3_18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat3_18.Location = New System.Drawing.Point(900, 100)
        Me.PicBxPlat3_18.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat3_18.Name = "PicBxPlat3_18"
        Me.PicBxPlat3_18.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat3_18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat3_18.TabIndex = 459
        Me.PicBxPlat3_18.TabStop = False
        '
        'PicBxPlat3_17
        '
        Me.PicBxPlat3_17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat3_17.Location = New System.Drawing.Point(848, 100)
        Me.PicBxPlat3_17.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat3_17.Name = "PicBxPlat3_17"
        Me.PicBxPlat3_17.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat3_17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat3_17.TabIndex = 458
        Me.PicBxPlat3_17.TabStop = False
        '
        'PicBxPlat3_16
        '
        Me.PicBxPlat3_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat3_16.Location = New System.Drawing.Point(796, 100)
        Me.PicBxPlat3_16.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat3_16.Name = "PicBxPlat3_16"
        Me.PicBxPlat3_16.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat3_16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat3_16.TabIndex = 457
        Me.PicBxPlat3_16.TabStop = False
        '
        'PicBxPlat3_15
        '
        Me.PicBxPlat3_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat3_15.Location = New System.Drawing.Point(744, 100)
        Me.PicBxPlat3_15.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat3_15.Name = "PicBxPlat3_15"
        Me.PicBxPlat3_15.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat3_15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat3_15.TabIndex = 456
        Me.PicBxPlat3_15.TabStop = False
        '
        'PicBxPlat3_14
        '
        Me.PicBxPlat3_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat3_14.Location = New System.Drawing.Point(692, 100)
        Me.PicBxPlat3_14.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat3_14.Name = "PicBxPlat3_14"
        Me.PicBxPlat3_14.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat3_14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat3_14.TabIndex = 455
        Me.PicBxPlat3_14.TabStop = False
        '
        'PicBxPlat3_13
        '
        Me.PicBxPlat3_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat3_13.Location = New System.Drawing.Point(640, 100)
        Me.PicBxPlat3_13.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat3_13.Name = "PicBxPlat3_13"
        Me.PicBxPlat3_13.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat3_13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat3_13.TabIndex = 454
        Me.PicBxPlat3_13.TabStop = False
        '
        'PicBxPlat3_12
        '
        Me.PicBxPlat3_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat3_12.Location = New System.Drawing.Point(588, 100)
        Me.PicBxPlat3_12.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat3_12.Name = "PicBxPlat3_12"
        Me.PicBxPlat3_12.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat3_12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat3_12.TabIndex = 453
        Me.PicBxPlat3_12.TabStop = False
        '
        'PicBxPlat3_11
        '
        Me.PicBxPlat3_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat3_11.Location = New System.Drawing.Point(536, 100)
        Me.PicBxPlat3_11.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat3_11.Name = "PicBxPlat3_11"
        Me.PicBxPlat3_11.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat3_11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat3_11.TabIndex = 452
        Me.PicBxPlat3_11.TabStop = False
        '
        'PicBxPlat3_10
        '
        Me.PicBxPlat3_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat3_10.Location = New System.Drawing.Point(484, 100)
        Me.PicBxPlat3_10.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat3_10.Name = "PicBxPlat3_10"
        Me.PicBxPlat3_10.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat3_10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat3_10.TabIndex = 451
        Me.PicBxPlat3_10.TabStop = False
        '
        'PicBxPlat3_9
        '
        Me.PicBxPlat3_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat3_9.Location = New System.Drawing.Point(432, 100)
        Me.PicBxPlat3_9.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat3_9.Name = "PicBxPlat3_9"
        Me.PicBxPlat3_9.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat3_9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat3_9.TabIndex = 450
        Me.PicBxPlat3_9.TabStop = False
        '
        'PicBxPlat3_8
        '
        Me.PicBxPlat3_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat3_8.Location = New System.Drawing.Point(380, 100)
        Me.PicBxPlat3_8.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat3_8.Name = "PicBxPlat3_8"
        Me.PicBxPlat3_8.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat3_8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat3_8.TabIndex = 449
        Me.PicBxPlat3_8.TabStop = False
        '
        'PicBxPlat3_7
        '
        Me.PicBxPlat3_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat3_7.Location = New System.Drawing.Point(328, 100)
        Me.PicBxPlat3_7.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat3_7.Name = "PicBxPlat3_7"
        Me.PicBxPlat3_7.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat3_7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat3_7.TabIndex = 448
        Me.PicBxPlat3_7.TabStop = False
        '
        'PicBxPlat3_6
        '
        Me.PicBxPlat3_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat3_6.Location = New System.Drawing.Point(276, 100)
        Me.PicBxPlat3_6.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat3_6.Name = "PicBxPlat3_6"
        Me.PicBxPlat3_6.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat3_6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat3_6.TabIndex = 447
        Me.PicBxPlat3_6.TabStop = False
        '
        'PicBxPlat3_5
        '
        Me.PicBxPlat3_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat3_5.Location = New System.Drawing.Point(224, 100)
        Me.PicBxPlat3_5.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat3_5.Name = "PicBxPlat3_5"
        Me.PicBxPlat3_5.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat3_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat3_5.TabIndex = 446
        Me.PicBxPlat3_5.TabStop = False
        '
        'PicBxPlat3_4
        '
        Me.PicBxPlat3_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat3_4.Location = New System.Drawing.Point(172, 100)
        Me.PicBxPlat3_4.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat3_4.Name = "PicBxPlat3_4"
        Me.PicBxPlat3_4.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat3_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat3_4.TabIndex = 445
        Me.PicBxPlat3_4.TabStop = False
        '
        'PicBxPlat3_3
        '
        Me.PicBxPlat3_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat3_3.Location = New System.Drawing.Point(120, 100)
        Me.PicBxPlat3_3.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat3_3.Name = "PicBxPlat3_3"
        Me.PicBxPlat3_3.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat3_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat3_3.TabIndex = 444
        Me.PicBxPlat3_3.TabStop = False
        '
        'PicBxPlat3_2
        '
        Me.PicBxPlat3_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat3_2.Location = New System.Drawing.Point(68, 100)
        Me.PicBxPlat3_2.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat3_2.Name = "PicBxPlat3_2"
        Me.PicBxPlat3_2.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat3_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat3_2.TabIndex = 443
        Me.PicBxPlat3_2.TabStop = False
        '
        'PicBxPlat3_1
        '
        Me.PicBxPlat3_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat3_1.Location = New System.Drawing.Point(16, 100)
        Me.PicBxPlat3_1.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat3_1.Name = "PicBxPlat3_1"
        Me.PicBxPlat3_1.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat3_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat3_1.TabIndex = 442
        Me.PicBxPlat3_1.TabStop = False
        '
        'PicBxPlat2_20
        '
        Me.PicBxPlat2_20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat2_20.Location = New System.Drawing.Point(1004, 54)
        Me.PicBxPlat2_20.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat2_20.Name = "PicBxPlat2_20"
        Me.PicBxPlat2_20.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat2_20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat2_20.TabIndex = 441
        Me.PicBxPlat2_20.TabStop = False
        '
        'PicBxPlat2_19
        '
        Me.PicBxPlat2_19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat2_19.Location = New System.Drawing.Point(952, 54)
        Me.PicBxPlat2_19.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat2_19.Name = "PicBxPlat2_19"
        Me.PicBxPlat2_19.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat2_19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat2_19.TabIndex = 440
        Me.PicBxPlat2_19.TabStop = False
        '
        'PicBxPlat2_18
        '
        Me.PicBxPlat2_18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat2_18.Location = New System.Drawing.Point(900, 54)
        Me.PicBxPlat2_18.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat2_18.Name = "PicBxPlat2_18"
        Me.PicBxPlat2_18.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat2_18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat2_18.TabIndex = 439
        Me.PicBxPlat2_18.TabStop = False
        '
        'PicBxPlat2_17
        '
        Me.PicBxPlat2_17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat2_17.Location = New System.Drawing.Point(848, 54)
        Me.PicBxPlat2_17.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat2_17.Name = "PicBxPlat2_17"
        Me.PicBxPlat2_17.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat2_17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat2_17.TabIndex = 438
        Me.PicBxPlat2_17.TabStop = False
        '
        'PicBxPlat2_16
        '
        Me.PicBxPlat2_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat2_16.Location = New System.Drawing.Point(796, 54)
        Me.PicBxPlat2_16.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat2_16.Name = "PicBxPlat2_16"
        Me.PicBxPlat2_16.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat2_16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat2_16.TabIndex = 437
        Me.PicBxPlat2_16.TabStop = False
        '
        'PicBxPlat2_15
        '
        Me.PicBxPlat2_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat2_15.Location = New System.Drawing.Point(744, 54)
        Me.PicBxPlat2_15.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat2_15.Name = "PicBxPlat2_15"
        Me.PicBxPlat2_15.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat2_15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat2_15.TabIndex = 436
        Me.PicBxPlat2_15.TabStop = False
        '
        'PicBxPlat2_14
        '
        Me.PicBxPlat2_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat2_14.Location = New System.Drawing.Point(692, 54)
        Me.PicBxPlat2_14.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat2_14.Name = "PicBxPlat2_14"
        Me.PicBxPlat2_14.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat2_14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat2_14.TabIndex = 435
        Me.PicBxPlat2_14.TabStop = False
        '
        'PicBxPlat2_13
        '
        Me.PicBxPlat2_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat2_13.Location = New System.Drawing.Point(640, 54)
        Me.PicBxPlat2_13.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat2_13.Name = "PicBxPlat2_13"
        Me.PicBxPlat2_13.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat2_13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat2_13.TabIndex = 434
        Me.PicBxPlat2_13.TabStop = False
        '
        'PicBxPlat2_12
        '
        Me.PicBxPlat2_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat2_12.Location = New System.Drawing.Point(588, 54)
        Me.PicBxPlat2_12.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat2_12.Name = "PicBxPlat2_12"
        Me.PicBxPlat2_12.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat2_12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat2_12.TabIndex = 433
        Me.PicBxPlat2_12.TabStop = False
        '
        'PicBxPlat2_11
        '
        Me.PicBxPlat2_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat2_11.Location = New System.Drawing.Point(536, 54)
        Me.PicBxPlat2_11.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat2_11.Name = "PicBxPlat2_11"
        Me.PicBxPlat2_11.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat2_11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat2_11.TabIndex = 432
        Me.PicBxPlat2_11.TabStop = False
        '
        'PicBxPlat2_10
        '
        Me.PicBxPlat2_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat2_10.Location = New System.Drawing.Point(484, 54)
        Me.PicBxPlat2_10.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat2_10.Name = "PicBxPlat2_10"
        Me.PicBxPlat2_10.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat2_10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat2_10.TabIndex = 431
        Me.PicBxPlat2_10.TabStop = False
        '
        'PicBxPlat2_9
        '
        Me.PicBxPlat2_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat2_9.Location = New System.Drawing.Point(432, 54)
        Me.PicBxPlat2_9.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat2_9.Name = "PicBxPlat2_9"
        Me.PicBxPlat2_9.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat2_9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat2_9.TabIndex = 430
        Me.PicBxPlat2_9.TabStop = False
        '
        'PicBxPlat2_8
        '
        Me.PicBxPlat2_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat2_8.Location = New System.Drawing.Point(380, 54)
        Me.PicBxPlat2_8.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat2_8.Name = "PicBxPlat2_8"
        Me.PicBxPlat2_8.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat2_8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat2_8.TabIndex = 429
        Me.PicBxPlat2_8.TabStop = False
        '
        'PicBxPlat2_7
        '
        Me.PicBxPlat2_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat2_7.Location = New System.Drawing.Point(328, 54)
        Me.PicBxPlat2_7.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat2_7.Name = "PicBxPlat2_7"
        Me.PicBxPlat2_7.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat2_7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat2_7.TabIndex = 428
        Me.PicBxPlat2_7.TabStop = False
        '
        'PicBxPlat2_6
        '
        Me.PicBxPlat2_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat2_6.Location = New System.Drawing.Point(276, 54)
        Me.PicBxPlat2_6.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat2_6.Name = "PicBxPlat2_6"
        Me.PicBxPlat2_6.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat2_6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat2_6.TabIndex = 427
        Me.PicBxPlat2_6.TabStop = False
        '
        'PicBxPlat2_5
        '
        Me.PicBxPlat2_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat2_5.Location = New System.Drawing.Point(224, 54)
        Me.PicBxPlat2_5.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat2_5.Name = "PicBxPlat2_5"
        Me.PicBxPlat2_5.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat2_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat2_5.TabIndex = 426
        Me.PicBxPlat2_5.TabStop = False
        '
        'PicBxPlat2_4
        '
        Me.PicBxPlat2_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat2_4.Location = New System.Drawing.Point(172, 54)
        Me.PicBxPlat2_4.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat2_4.Name = "PicBxPlat2_4"
        Me.PicBxPlat2_4.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat2_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat2_4.TabIndex = 425
        Me.PicBxPlat2_4.TabStop = False
        '
        'PicBxPlat2_3
        '
        Me.PicBxPlat2_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat2_3.Location = New System.Drawing.Point(120, 54)
        Me.PicBxPlat2_3.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat2_3.Name = "PicBxPlat2_3"
        Me.PicBxPlat2_3.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat2_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat2_3.TabIndex = 424
        Me.PicBxPlat2_3.TabStop = False
        '
        'PicBxPlat2_2
        '
        Me.PicBxPlat2_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat2_2.Location = New System.Drawing.Point(68, 54)
        Me.PicBxPlat2_2.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat2_2.Name = "PicBxPlat2_2"
        Me.PicBxPlat2_2.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat2_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat2_2.TabIndex = 423
        Me.PicBxPlat2_2.TabStop = False
        '
        'PicBxPlat2_1
        '
        Me.PicBxPlat2_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat2_1.Location = New System.Drawing.Point(16, 54)
        Me.PicBxPlat2_1.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat2_1.Name = "PicBxPlat2_1"
        Me.PicBxPlat2_1.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat2_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat2_1.TabIndex = 422
        Me.PicBxPlat2_1.TabStop = False
        '
        'PicBxPlat1_20
        '
        Me.PicBxPlat1_20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat1_20.Location = New System.Drawing.Point(1004, 6)
        Me.PicBxPlat1_20.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat1_20.Name = "PicBxPlat1_20"
        Me.PicBxPlat1_20.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat1_20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat1_20.TabIndex = 421
        Me.PicBxPlat1_20.TabStop = False
        '
        'PicBxPlat1_19
        '
        Me.PicBxPlat1_19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat1_19.Location = New System.Drawing.Point(952, 6)
        Me.PicBxPlat1_19.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat1_19.Name = "PicBxPlat1_19"
        Me.PicBxPlat1_19.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat1_19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat1_19.TabIndex = 420
        Me.PicBxPlat1_19.TabStop = False
        '
        'PicBxPlat1_18
        '
        Me.PicBxPlat1_18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat1_18.Location = New System.Drawing.Point(900, 6)
        Me.PicBxPlat1_18.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat1_18.Name = "PicBxPlat1_18"
        Me.PicBxPlat1_18.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat1_18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat1_18.TabIndex = 419
        Me.PicBxPlat1_18.TabStop = False
        '
        'PicBxPlat1_17
        '
        Me.PicBxPlat1_17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat1_17.Location = New System.Drawing.Point(848, 6)
        Me.PicBxPlat1_17.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat1_17.Name = "PicBxPlat1_17"
        Me.PicBxPlat1_17.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat1_17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat1_17.TabIndex = 418
        Me.PicBxPlat1_17.TabStop = False
        '
        'PicBxPlat1_16
        '
        Me.PicBxPlat1_16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat1_16.Location = New System.Drawing.Point(796, 6)
        Me.PicBxPlat1_16.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat1_16.Name = "PicBxPlat1_16"
        Me.PicBxPlat1_16.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat1_16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat1_16.TabIndex = 417
        Me.PicBxPlat1_16.TabStop = False
        '
        'PicBxPlat1_15
        '
        Me.PicBxPlat1_15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat1_15.Location = New System.Drawing.Point(744, 6)
        Me.PicBxPlat1_15.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat1_15.Name = "PicBxPlat1_15"
        Me.PicBxPlat1_15.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat1_15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat1_15.TabIndex = 416
        Me.PicBxPlat1_15.TabStop = False
        '
        'PicBxPlat1_14
        '
        Me.PicBxPlat1_14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat1_14.Location = New System.Drawing.Point(692, 6)
        Me.PicBxPlat1_14.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat1_14.Name = "PicBxPlat1_14"
        Me.PicBxPlat1_14.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat1_14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat1_14.TabIndex = 415
        Me.PicBxPlat1_14.TabStop = False
        '
        'PicBxPlat1_13
        '
        Me.PicBxPlat1_13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat1_13.Location = New System.Drawing.Point(640, 6)
        Me.PicBxPlat1_13.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat1_13.Name = "PicBxPlat1_13"
        Me.PicBxPlat1_13.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat1_13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat1_13.TabIndex = 414
        Me.PicBxPlat1_13.TabStop = False
        '
        'PicBxPlat1_12
        '
        Me.PicBxPlat1_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat1_12.Location = New System.Drawing.Point(588, 6)
        Me.PicBxPlat1_12.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat1_12.Name = "PicBxPlat1_12"
        Me.PicBxPlat1_12.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat1_12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat1_12.TabIndex = 413
        Me.PicBxPlat1_12.TabStop = False
        '
        'PicBxPlat1_11
        '
        Me.PicBxPlat1_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat1_11.Location = New System.Drawing.Point(536, 6)
        Me.PicBxPlat1_11.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat1_11.Name = "PicBxPlat1_11"
        Me.PicBxPlat1_11.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat1_11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat1_11.TabIndex = 412
        Me.PicBxPlat1_11.TabStop = False
        '
        'PicBxPlat1_10
        '
        Me.PicBxPlat1_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat1_10.Location = New System.Drawing.Point(484, 6)
        Me.PicBxPlat1_10.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat1_10.Name = "PicBxPlat1_10"
        Me.PicBxPlat1_10.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat1_10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat1_10.TabIndex = 411
        Me.PicBxPlat1_10.TabStop = False
        '
        'PicBxPlat1_9
        '
        Me.PicBxPlat1_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat1_9.Location = New System.Drawing.Point(432, 6)
        Me.PicBxPlat1_9.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat1_9.Name = "PicBxPlat1_9"
        Me.PicBxPlat1_9.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat1_9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat1_9.TabIndex = 410
        Me.PicBxPlat1_9.TabStop = False
        '
        'PicBxPlat1_8
        '
        Me.PicBxPlat1_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat1_8.Location = New System.Drawing.Point(380, 6)
        Me.PicBxPlat1_8.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat1_8.Name = "PicBxPlat1_8"
        Me.PicBxPlat1_8.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat1_8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat1_8.TabIndex = 409
        Me.PicBxPlat1_8.TabStop = False
        '
        'PicBxPlat1_7
        '
        Me.PicBxPlat1_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat1_7.Location = New System.Drawing.Point(328, 6)
        Me.PicBxPlat1_7.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat1_7.Name = "PicBxPlat1_7"
        Me.PicBxPlat1_7.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat1_7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat1_7.TabIndex = 408
        Me.PicBxPlat1_7.TabStop = False
        '
        'PicBxPlat1_6
        '
        Me.PicBxPlat1_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat1_6.Location = New System.Drawing.Point(276, 6)
        Me.PicBxPlat1_6.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat1_6.Name = "PicBxPlat1_6"
        Me.PicBxPlat1_6.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat1_6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat1_6.TabIndex = 407
        Me.PicBxPlat1_6.TabStop = False
        '
        'PicBxPlat1_5
        '
        Me.PicBxPlat1_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat1_5.Location = New System.Drawing.Point(224, 6)
        Me.PicBxPlat1_5.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat1_5.Name = "PicBxPlat1_5"
        Me.PicBxPlat1_5.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat1_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat1_5.TabIndex = 406
        Me.PicBxPlat1_5.TabStop = False
        '
        'PicBxPlat1_4
        '
        Me.PicBxPlat1_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat1_4.Location = New System.Drawing.Point(172, 6)
        Me.PicBxPlat1_4.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat1_4.Name = "PicBxPlat1_4"
        Me.PicBxPlat1_4.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat1_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat1_4.TabIndex = 405
        Me.PicBxPlat1_4.TabStop = False
        '
        'PicBxPlat1_3
        '
        Me.PicBxPlat1_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat1_3.Location = New System.Drawing.Point(120, 6)
        Me.PicBxPlat1_3.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat1_3.Name = "PicBxPlat1_3"
        Me.PicBxPlat1_3.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat1_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat1_3.TabIndex = 404
        Me.PicBxPlat1_3.TabStop = False
        '
        'PicBxPlat1_2
        '
        Me.PicBxPlat1_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat1_2.Location = New System.Drawing.Point(68, 6)
        Me.PicBxPlat1_2.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat1_2.Name = "PicBxPlat1_2"
        Me.PicBxPlat1_2.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat1_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat1_2.TabIndex = 403
        Me.PicBxPlat1_2.TabStop = False
        '
        'PicBxPlat1_1
        '
        Me.PicBxPlat1_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PicBxPlat1_1.Location = New System.Drawing.Point(16, 6)
        Me.PicBxPlat1_1.Margin = New System.Windows.Forms.Padding(4)
        Me.PicBxPlat1_1.Name = "PicBxPlat1_1"
        Me.PicBxPlat1_1.Size = New System.Drawing.Size(53, 49)
        Me.PicBxPlat1_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBxPlat1_1.TabIndex = 402
        Me.PicBxPlat1_1.TabStop = False
        '
        'PctBx_Pioche
        '
        Me.PctBx_Pioche.BackColor = System.Drawing.Color.Transparent
        Me.PctBx_Pioche.BackgroundImage = Global.Qwirkle.My.Resources.Resources.BoutonPioche_Plateau
        Me.PctBx_Pioche.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PctBx_Pioche.Location = New System.Drawing.Point(1377, 802)
        Me.PctBx_Pioche.Margin = New System.Windows.Forms.Padding(4)
        Me.PctBx_Pioche.Name = "PctBx_Pioche"
        Me.PctBx_Pioche.Size = New System.Drawing.Size(106, 98)
        Me.PctBx_Pioche.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PctBx_Pioche.TabIndex = 21
        Me.PctBx_Pioche.TabStop = False
        '
        'Lbl_PionRest
        '
        Me.Lbl_PionRest.AutoSize = True
        Me.Lbl_PionRest.BackColor = System.Drawing.Color.Transparent
        Me.Lbl_PionRest.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_PionRest.Location = New System.Drawing.Point(1653, 496)
        Me.Lbl_PionRest.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Lbl_PionRest.Name = "Lbl_PionRest"
        Me.Lbl_PionRest.Size = New System.Drawing.Size(26, 29)
        Me.Lbl_PionRest.TabIndex = 45
        Me.Lbl_PionRest.Text = "0"
        '
        'Lbl_JTour
        '
        Me.Lbl_JTour.AutoSize = True
        Me.Lbl_JTour.BackColor = System.Drawing.Color.Transparent
        Me.Lbl_JTour.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_JTour.Location = New System.Drawing.Point(1653, 564)
        Me.Lbl_JTour.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Lbl_JTour.Name = "Lbl_JTour"
        Me.Lbl_JTour.Size = New System.Drawing.Size(106, 29)
        Me.Lbl_JTour.TabIndex = 44
        Me.Lbl_JTour.Text = "Joueur 1"
        '
        'Lbl_tourTxt
        '
        Me.Lbl_tourTxt.AutoSize = True
        Me.Lbl_tourTxt.BackColor = System.Drawing.Color.Transparent
        Me.Lbl_tourTxt.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_tourTxt.Location = New System.Drawing.Point(1424, 564)
        Me.Lbl_tourTxt.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Lbl_tourTxt.Name = "Lbl_tourTxt"
        Me.Lbl_tourTxt.Size = New System.Drawing.Size(76, 29)
        Me.Lbl_tourTxt.TabIndex = 43
        Me.Lbl_tourTxt.Text = "Tour :"
        '
        'Lbl_PionrestTxt
        '
        Me.Lbl_PionrestTxt.AutoSize = True
        Me.Lbl_PionrestTxt.BackColor = System.Drawing.Color.Transparent
        Me.Lbl_PionrestTxt.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_PionrestTxt.Location = New System.Drawing.Point(1379, 496)
        Me.Lbl_PionrestTxt.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Lbl_PionrestTxt.Name = "Lbl_PionrestTxt"
        Me.Lbl_PionrestTxt.Size = New System.Drawing.Size(176, 29)
        Me.Lbl_PionrestTxt.TabIndex = 42
        Me.Lbl_PionrestTxt.Text = "Pions restants :"
        '
        'Lbl_ScJ4
        '
        Me.Lbl_ScJ4.AutoSize = True
        Me.Lbl_ScJ4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_ScJ4.Location = New System.Drawing.Point(175, 91)
        Me.Lbl_ScJ4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Lbl_ScJ4.Name = "Lbl_ScJ4"
        Me.Lbl_ScJ4.Size = New System.Drawing.Size(26, 29)
        Me.Lbl_ScJ4.TabIndex = 41
        Me.Lbl_ScJ4.Text = "0"
        '
        'Lbl_txtSJ4
        '
        Me.Lbl_txtSJ4.AutoSize = True
        Me.Lbl_txtSJ4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_txtSJ4.Location = New System.Drawing.Point(47, 91)
        Me.Lbl_txtSJ4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Lbl_txtSJ4.Name = "Lbl_txtSJ4"
        Me.Lbl_txtSJ4.Size = New System.Drawing.Size(89, 29)
        Me.Lbl_txtSJ4.TabIndex = 40
        Me.Lbl_txtSJ4.Text = "Score :"
        '
        'Lbl_NomJ4
        '
        Me.Lbl_NomJ4.AutoSize = True
        Me.Lbl_NomJ4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_NomJ4.Location = New System.Drawing.Point(117, 26)
        Me.Lbl_NomJ4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Lbl_NomJ4.Name = "Lbl_NomJ4"
        Me.Lbl_NomJ4.Size = New System.Drawing.Size(106, 29)
        Me.Lbl_NomJ4.TabIndex = 39
        Me.Lbl_NomJ4.Text = "Joueur 4"
        '
        'Lbl_ScJ3
        '
        Me.Lbl_ScJ3.AutoSize = True
        Me.Lbl_ScJ3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_ScJ3.Location = New System.Drawing.Point(163, 85)
        Me.Lbl_ScJ3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Lbl_ScJ3.Name = "Lbl_ScJ3"
        Me.Lbl_ScJ3.Size = New System.Drawing.Size(26, 29)
        Me.Lbl_ScJ3.TabIndex = 38
        Me.Lbl_ScJ3.Text = "0"
        '
        'Lbl_txtSJ3
        '
        Me.Lbl_txtSJ3.AutoSize = True
        Me.Lbl_txtSJ3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_txtSJ3.Location = New System.Drawing.Point(35, 85)
        Me.Lbl_txtSJ3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Lbl_txtSJ3.Name = "Lbl_txtSJ3"
        Me.Lbl_txtSJ3.Size = New System.Drawing.Size(89, 29)
        Me.Lbl_txtSJ3.TabIndex = 37
        Me.Lbl_txtSJ3.Text = "Score :"
        '
        'Lbl_NomJ3
        '
        Me.Lbl_NomJ3.AutoSize = True
        Me.Lbl_NomJ3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_NomJ3.Location = New System.Drawing.Point(105, 20)
        Me.Lbl_NomJ3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Lbl_NomJ3.Name = "Lbl_NomJ3"
        Me.Lbl_NomJ3.Size = New System.Drawing.Size(106, 29)
        Me.Lbl_NomJ3.TabIndex = 36
        Me.Lbl_NomJ3.Text = "Joueur 3"
        '
        'Lbl_ScJ2
        '
        Me.Lbl_ScJ2.AutoSize = True
        Me.Lbl_ScJ2.BackColor = System.Drawing.Color.Transparent
        Me.Lbl_ScJ2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_ScJ2.Location = New System.Drawing.Point(1740, 212)
        Me.Lbl_ScJ2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Lbl_ScJ2.Name = "Lbl_ScJ2"
        Me.Lbl_ScJ2.Size = New System.Drawing.Size(26, 29)
        Me.Lbl_ScJ2.TabIndex = 35
        Me.Lbl_ScJ2.Text = "0"
        '
        'Lbl_txtSJ2
        '
        Me.Lbl_txtSJ2.AutoSize = True
        Me.Lbl_txtSJ2.BackColor = System.Drawing.Color.Transparent
        Me.Lbl_txtSJ2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_txtSJ2.Location = New System.Drawing.Point(1612, 212)
        Me.Lbl_txtSJ2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Lbl_txtSJ2.Name = "Lbl_txtSJ2"
        Me.Lbl_txtSJ2.Size = New System.Drawing.Size(89, 29)
        Me.Lbl_txtSJ2.TabIndex = 34
        Me.Lbl_txtSJ2.Text = "Score :"
        '
        'Lbl_NomJ2
        '
        Me.Lbl_NomJ2.AutoSize = True
        Me.Lbl_NomJ2.BackColor = System.Drawing.Color.Transparent
        Me.Lbl_NomJ2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_NomJ2.Location = New System.Drawing.Point(1683, 146)
        Me.Lbl_NomJ2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Lbl_NomJ2.Name = "Lbl_NomJ2"
        Me.Lbl_NomJ2.Size = New System.Drawing.Size(106, 29)
        Me.Lbl_NomJ2.TabIndex = 33
        Me.Lbl_NomJ2.Text = "Joueur 2"
        '
        'Lbl_ScJ1
        '
        Me.Lbl_ScJ1.AutoSize = True
        Me.Lbl_ScJ1.BackColor = System.Drawing.Color.Transparent
        Me.Lbl_ScJ1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_ScJ1.Location = New System.Drawing.Point(1404, 212)
        Me.Lbl_ScJ1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Lbl_ScJ1.Name = "Lbl_ScJ1"
        Me.Lbl_ScJ1.Size = New System.Drawing.Size(26, 29)
        Me.Lbl_ScJ1.TabIndex = 32
        Me.Lbl_ScJ1.Text = "0"
        '
        'Lbl_txtSJ1
        '
        Me.Lbl_txtSJ1.AutoSize = True
        Me.Lbl_txtSJ1.BackColor = System.Drawing.Color.Transparent
        Me.Lbl_txtSJ1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_txtSJ1.Location = New System.Drawing.Point(1276, 212)
        Me.Lbl_txtSJ1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Lbl_txtSJ1.Name = "Lbl_txtSJ1"
        Me.Lbl_txtSJ1.Size = New System.Drawing.Size(89, 29)
        Me.Lbl_txtSJ1.TabIndex = 31
        Me.Lbl_txtSJ1.Text = "Score :"
        '
        'Lbl_NomJ1
        '
        Me.Lbl_NomJ1.AutoSize = True
        Me.Lbl_NomJ1.BackColor = System.Drawing.Color.Transparent
        Me.Lbl_NomJ1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_NomJ1.Location = New System.Drawing.Point(1347, 146)
        Me.Lbl_NomJ1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Lbl_NomJ1.Name = "Lbl_NomJ1"
        Me.Lbl_NomJ1.Size = New System.Drawing.Size(106, 29)
        Me.Lbl_NomJ1.TabIndex = 30
        Me.Lbl_NomJ1.Text = "Joueur 1"
        '
        'Btn_Menu
        '
        Me.Btn_Menu.Image = Global.Qwirkle.My.Resources.Resources.BoutonMenu_Plateau
        Me.Btn_Menu.Location = New System.Drawing.Point(1352, 41)
        Me.Btn_Menu.Margin = New System.Windows.Forms.Padding(4)
        Me.Btn_Menu.Name = "Btn_Menu"
        Me.Btn_Menu.Size = New System.Drawing.Size(107, 37)
        Me.Btn_Menu.TabIndex = 29
        Me.Btn_Menu.UseVisualStyleBackColor = True
        '
        'Btn_Quitter
        '
        Me.Btn_Quitter.Image = Global.Qwirkle.My.Resources.Resources.BoutonQuitter_Plateau
        Me.Btn_Quitter.Location = New System.Drawing.Point(1617, 41)
        Me.Btn_Quitter.Margin = New System.Windows.Forms.Padding(4)
        Me.Btn_Quitter.Name = "Btn_Quitter"
        Me.Btn_Quitter.Size = New System.Drawing.Size(107, 37)
        Me.Btn_Quitter.TabIndex = 28
        Me.Btn_Quitter.UseVisualStyleBackColor = True
        '
        'Pnl_J3
        '
        Me.Pnl_J3.BackColor = System.Drawing.Color.Transparent
        Me.Pnl_J3.Controls.Add(Me.Lbl_NomJ3)
        Me.Pnl_J3.Controls.Add(Me.Lbl_txtSJ3)
        Me.Pnl_J3.Controls.Add(Me.Lbl_ScJ3)
        Me.Pnl_J3.Location = New System.Drawing.Point(1237, 284)
        Me.Pnl_J3.Margin = New System.Windows.Forms.Padding(4)
        Me.Pnl_J3.Name = "Pnl_J3"
        Me.Pnl_J3.Size = New System.Drawing.Size(267, 123)
        Me.Pnl_J3.TabIndex = 46
        '
        'Pnl_J4
        '
        Me.Pnl_J4.BackColor = System.Drawing.Color.Transparent
        Me.Pnl_J4.Controls.Add(Me.Lbl_NomJ4)
        Me.Pnl_J4.Controls.Add(Me.Lbl_txtSJ4)
        Me.Pnl_J4.Controls.Add(Me.Lbl_ScJ4)
        Me.Pnl_J4.Location = New System.Drawing.Point(1569, 284)
        Me.Pnl_J4.Margin = New System.Windows.Forms.Padding(4)
        Me.Pnl_J4.Name = "Pnl_J4"
        Me.Pnl_J4.Size = New System.Drawing.Size(267, 123)
        Me.Pnl_J4.TabIndex = 47
        '
        'Plateau
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.Qwirkle.My.Resources.Resources.BackgroundQwirkle
        Me.ClientSize = New System.Drawing.Size(1896, 1012)
        Me.Controls.Add(Me.Pnl_J4)
        Me.Controls.Add(Me.Pnl_J3)
        Me.Controls.Add(Me.Lbl_PionRest)
        Me.Controls.Add(Me.Lbl_JTour)
        Me.Controls.Add(Me.Lbl_tourTxt)
        Me.Controls.Add(Me.Lbl_PionrestTxt)
        Me.Controls.Add(Me.Lbl_ScJ2)
        Me.Controls.Add(Me.Lbl_txtSJ2)
        Me.Controls.Add(Me.Lbl_NomJ2)
        Me.Controls.Add(Me.Lbl_ScJ1)
        Me.Controls.Add(Me.Lbl_txtSJ1)
        Me.Controls.Add(Me.Lbl_NomJ1)
        Me.Controls.Add(Me.Btn_Menu)
        Me.Controls.Add(Me.Btn_Quitter)
        Me.Controls.Add(Me.GpBx_Plateau)
        Me.Controls.Add(Me.Btn_Valid)
        Me.Controls.Add(Me.Btn_Retour)
        Me.Controls.Add(Me.PctBx_Pioche)
        Me.Controls.Add(Me.Gp_MainJ)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "Plateau"
        Me.Text = "Plateau"
        Me.Gp_MainJ.ResumeLayout(False)
        CType(Me.PctBx_TuileMJ6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PctBx_TuileMJ5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PctBx_TuileMJ4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PctBx_TuileMJ2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PctBx_TuileMJ3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PctBx_TuileMJ1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GpBx_Plateau.ResumeLayout(False)
        CType(Me.PicBxPlat20_20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat20_19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat20_18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat20_17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat20_16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat20_15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat20_14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat20_13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat20_12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat20_11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat20_10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat20_9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat20_8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat20_7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat20_6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat20_5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat20_4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat20_3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat20_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat20_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat19_20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat19_19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat19_18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat19_17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat19_16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat19_15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat19_14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat19_13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat19_12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat19_11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat19_10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat19_9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat19_8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat19_7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat19_6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat19_5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat19_4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat19_3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat19_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat19_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat18_20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat18_19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat18_18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat18_17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat18_16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat18_15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat18_14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat18_13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat18_12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat18_11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat18_10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat18_9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat18_8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat18_7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat18_6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat18_5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat18_4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat18_3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat18_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat18_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat17_20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat17_19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat17_18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat17_17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat17_16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat17_15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat17_14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat17_13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat17_12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat17_11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat17_10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat17_9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat17_8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat17_7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat17_6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat17_5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat17_4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat17_3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat17_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat17_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat16_20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat16_19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat16_18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat16_17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat16_16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat16_15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat16_14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat16_13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat16_12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat16_11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat16_10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat16_9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat16_8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat16_7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat16_6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat16_5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat16_4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat16_3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat16_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat16_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat15_20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat15_19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat15_18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat15_17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat15_16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat15_15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat15_14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat15_13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat15_12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat15_11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat15_10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat15_9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat15_8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat15_7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat15_6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat15_5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat15_4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat15_3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat15_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat15_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat14_20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat14_19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat14_18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat14_17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat14_16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat14_15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat14_14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat14_13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat14_12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat14_11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat14_10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat14_9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat14_8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat14_7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat14_6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat14_5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat14_4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat14_3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat14_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat14_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat13_20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat13_19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat13_18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat13_17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat13_16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat13_15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat13_14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat13_13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat13_12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat13_11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat13_10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat13_9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat13_8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat13_7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat13_6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat13_5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat13_4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat13_3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat13_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat13_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat12_20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat12_19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat12_18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat12_17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat12_16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat12_15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat12_14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat12_13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat12_12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat12_11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat12_10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat12_9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat12_8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat12_7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat12_6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat12_5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat12_4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat12_3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat12_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat12_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat11_20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat11_19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat11_18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat11_17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat11_16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat11_15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat11_14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat11_13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat11_12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat11_11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat11_10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat11_9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat11_8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat11_7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat11_6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat11_5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat11_4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat11_3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat11_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat11_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat10_20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat10_19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat10_18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat10_17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat10_16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat10_15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat10_14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat10_13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat10_12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat10_11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat10_10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat10_9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat10_8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat10_7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat10_6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat10_5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat10_4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat10_3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat10_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat10_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat9_20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat9_19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat9_18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat9_17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat9_16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat9_15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat9_14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat9_13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat9_12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat9_11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat9_10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat9_9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat9_8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat9_7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat9_6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat9_5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat9_4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat9_3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat9_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat9_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat8_20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat8_19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat8_18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat8_17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat8_16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat8_15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat8_14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat8_13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat8_12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat8_11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat8_10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat8_9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat8_8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat8_7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat8_6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat8_5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat8_4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat8_3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat8_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat8_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat7_20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat7_19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat7_18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat7_17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat7_16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat7_15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat7_14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat7_13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat7_12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat7_11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat7_10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat7_9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat7_8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat7_7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat7_6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat7_5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat7_4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat7_3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat7_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat7_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat6_20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat6_19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat6_18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat6_17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat6_16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat6_15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat6_14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat6_13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat6_12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat6_11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat6_10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat6_9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat6_8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat6_7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat6_6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat6_5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat6_4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat6_3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat6_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat6_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat5_20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat5_19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat5_18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat5_17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat5_16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat5_15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat5_14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat5_13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat5_12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat5_11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat5_10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat5_9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat5_8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat5_7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat5_6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat5_5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat5_4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat5_3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat5_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat5_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat4_20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat4_19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat4_18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat4_17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat4_16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat4_15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat4_14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat4_13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat4_12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat4_11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat4_10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat4_9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat4_8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat4_7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat4_6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat4_5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat4_4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat4_3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat4_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat4_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat3_20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat3_19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat3_18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat3_17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat3_16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat3_15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat3_14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat3_13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat3_12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat3_11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat3_10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat3_9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat3_8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat3_7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat3_6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat3_5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat3_4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat3_3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat3_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat3_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat2_20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat2_19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat2_18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat2_17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat2_16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat2_15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat2_14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat2_13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat2_12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat2_11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat2_10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat2_9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat2_8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat2_7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat2_6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat2_5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat2_4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat2_3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat2_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat2_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat1_20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat1_19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat1_18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat1_17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat1_16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat1_15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat1_14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat1_13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat1_12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat1_11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat1_10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat1_9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat1_8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat1_7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat1_6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat1_5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat1_4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat1_3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat1_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBxPlat1_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PctBx_Pioche, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Pnl_J3.ResumeLayout(False)
        Me.Pnl_J3.PerformLayout()
        Me.Pnl_J4.ResumeLayout(False)
        Me.Pnl_J4.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BindingSource1 As BindingSource
    Friend WithEvents Gp_MainJ As GroupBox
    Friend WithEvents PctBx_Pioche As PictureBox
    Friend WithEvents Btn_Retour As Button
    Friend WithEvents Btn_Valid As Button
    Friend WithEvents GpBx_Plateau As GroupBox
    Friend WithEvents PctBx_TuileMJ6 As PictureBox
    Friend WithEvents PctBx_TuileMJ5 As PictureBox
    Friend WithEvents PctBx_TuileMJ4 As PictureBox
    Friend WithEvents PctBx_TuileMJ2 As PictureBox
    Friend WithEvents PctBx_TuileMJ3 As PictureBox
    Friend WithEvents PctBx_TuileMJ1 As PictureBox
    Friend WithEvents PicBxPlat4_20 As PictureBox
    Friend WithEvents PicBxPlat4_19 As PictureBox
    Friend WithEvents PicBxPlat4_18 As PictureBox
    Friend WithEvents PicBxPlat4_17 As PictureBox
    Friend WithEvents PicBxPlat4_16 As PictureBox
    Friend WithEvents PicBxPlat4_15 As PictureBox
    Friend WithEvents PicBxPlat4_14 As PictureBox
    Friend WithEvents PicBxPlat4_13 As PictureBox
    Friend WithEvents PicBxPlat4_12 As PictureBox
    Friend WithEvents PicBxPlat4_11 As PictureBox
    Friend WithEvents PicBxPlat4_10 As PictureBox
    Friend WithEvents PicBxPlat4_9 As PictureBox
    Friend WithEvents PicBxPlat4_8 As PictureBox
    Friend WithEvents PicBxPlat4_7 As PictureBox
    Friend WithEvents PicBxPlat4_6 As PictureBox
    Friend WithEvents PicBxPlat4_5 As PictureBox
    Friend WithEvents PicBxPlat4_4 As PictureBox
    Friend WithEvents PicBxPlat4_3 As PictureBox
    Friend WithEvents PicBxPlat4_2 As PictureBox
    Friend WithEvents PicBxPlat4_1 As PictureBox
    Friend WithEvents PicBxPlat3_20 As PictureBox
    Friend WithEvents PicBxPlat3_19 As PictureBox
    Friend WithEvents PicBxPlat3_18 As PictureBox
    Friend WithEvents PicBxPlat3_17 As PictureBox
    Friend WithEvents PicBxPlat3_16 As PictureBox
    Friend WithEvents PicBxPlat3_15 As PictureBox
    Friend WithEvents PicBxPlat3_14 As PictureBox
    Friend WithEvents PicBxPlat3_13 As PictureBox
    Friend WithEvents PicBxPlat3_12 As PictureBox
    Friend WithEvents PicBxPlat3_11 As PictureBox
    Friend WithEvents PicBxPlat3_10 As PictureBox
    Friend WithEvents PicBxPlat3_9 As PictureBox
    Friend WithEvents PicBxPlat3_8 As PictureBox
    Friend WithEvents PicBxPlat3_7 As PictureBox
    Friend WithEvents PicBxPlat3_6 As PictureBox
    Friend WithEvents PicBxPlat3_5 As PictureBox
    Friend WithEvents PicBxPlat3_4 As PictureBox
    Friend WithEvents PicBxPlat3_3 As PictureBox
    Friend WithEvents PicBxPlat3_2 As PictureBox
    Friend WithEvents PicBxPlat3_1 As PictureBox
    Friend WithEvents PicBxPlat2_20 As PictureBox
    Friend WithEvents PicBxPlat2_19 As PictureBox
    Friend WithEvents PicBxPlat2_18 As PictureBox
    Friend WithEvents PicBxPlat2_17 As PictureBox
    Friend WithEvents PicBxPlat2_16 As PictureBox
    Friend WithEvents PicBxPlat2_15 As PictureBox
    Friend WithEvents PicBxPlat2_14 As PictureBox
    Friend WithEvents PicBxPlat2_13 As PictureBox
    Friend WithEvents PicBxPlat2_12 As PictureBox
    Friend WithEvents PicBxPlat2_11 As PictureBox
    Friend WithEvents PicBxPlat2_10 As PictureBox
    Friend WithEvents PicBxPlat2_9 As PictureBox
    Friend WithEvents PicBxPlat2_8 As PictureBox
    Friend WithEvents PicBxPlat2_7 As PictureBox
    Friend WithEvents PicBxPlat2_6 As PictureBox
    Friend WithEvents PicBxPlat2_5 As PictureBox
    Friend WithEvents PicBxPlat2_4 As PictureBox
    Friend WithEvents PicBxPlat2_3 As PictureBox
    Friend WithEvents PicBxPlat2_2 As PictureBox
    Friend WithEvents PicBxPlat2_1 As PictureBox
    Friend WithEvents PicBxPlat1_20 As PictureBox
    Friend WithEvents PicBxPlat1_19 As PictureBox
    Friend WithEvents PicBxPlat1_18 As PictureBox
    Friend WithEvents PicBxPlat1_17 As PictureBox
    Friend WithEvents PicBxPlat1_16 As PictureBox
    Friend WithEvents PicBxPlat1_15 As PictureBox
    Friend WithEvents PicBxPlat1_14 As PictureBox
    Friend WithEvents PicBxPlat1_13 As PictureBox
    Friend WithEvents PicBxPlat1_12 As PictureBox
    Friend WithEvents PicBxPlat1_11 As PictureBox
    Friend WithEvents PicBxPlat1_10 As PictureBox
    Friend WithEvents PicBxPlat1_9 As PictureBox
    Friend WithEvents PicBxPlat1_8 As PictureBox
    Friend WithEvents PicBxPlat1_7 As PictureBox
    Friend WithEvents PicBxPlat1_6 As PictureBox
    Friend WithEvents PicBxPlat1_5 As PictureBox
    Friend WithEvents PicBxPlat1_4 As PictureBox
    Friend WithEvents PicBxPlat1_3 As PictureBox
    Friend WithEvents PicBxPlat1_2 As PictureBox
    Friend WithEvents PicBxPlat1_1 As PictureBox
    Friend WithEvents PicBxPlat6_20 As PictureBox
    Friend WithEvents PicBxPlat6_19 As PictureBox
    Friend WithEvents PicBxPlat6_18 As PictureBox
    Friend WithEvents PicBxPlat6_17 As PictureBox
    Friend WithEvents PicBxPlat6_16 As PictureBox
    Friend WithEvents PicBxPlat6_15 As PictureBox
    Friend WithEvents PicBxPlat6_14 As PictureBox
    Friend WithEvents PicBxPlat6_13 As PictureBox
    Friend WithEvents PicBxPlat6_12 As PictureBox
    Friend WithEvents PicBxPlat6_11 As PictureBox
    Friend WithEvents PicBxPlat6_10 As PictureBox
    Friend WithEvents PicBxPlat6_9 As PictureBox
    Friend WithEvents PicBxPlat6_8 As PictureBox
    Friend WithEvents PicBxPlat6_7 As PictureBox
    Friend WithEvents PicBxPlat6_6 As PictureBox
    Friend WithEvents PicBxPlat6_5 As PictureBox
    Friend WithEvents PicBxPlat6_4 As PictureBox
    Friend WithEvents PicBxPlat6_3 As PictureBox
    Friend WithEvents PicBxPlat6_2 As PictureBox
    Friend WithEvents PicBxPlat6_1 As PictureBox
    Friend WithEvents PicBxPlat5_20 As PictureBox
    Friend WithEvents PicBxPlat5_19 As PictureBox
    Friend WithEvents PicBxPlat5_18 As PictureBox
    Friend WithEvents PicBxPlat5_17 As PictureBox
    Friend WithEvents PicBxPlat5_16 As PictureBox
    Friend WithEvents PicBxPlat5_15 As PictureBox
    Friend WithEvents PicBxPlat5_14 As PictureBox
    Friend WithEvents PicBxPlat5_13 As PictureBox
    Friend WithEvents PicBxPlat5_12 As PictureBox
    Friend WithEvents PicBxPlat5_11 As PictureBox
    Friend WithEvents PicBxPlat5_10 As PictureBox
    Friend WithEvents PicBxPlat5_9 As PictureBox
    Friend WithEvents PicBxPlat5_8 As PictureBox
    Friend WithEvents PicBxPlat5_7 As PictureBox
    Friend WithEvents PicBxPlat5_6 As PictureBox
    Friend WithEvents PicBxPlat5_5 As PictureBox
    Friend WithEvents PicBxPlat5_4 As PictureBox
    Friend WithEvents PicBxPlat5_3 As PictureBox
    Friend WithEvents PicBxPlat5_2 As PictureBox
    Friend WithEvents PicBxPlat5_1 As PictureBox
    Friend WithEvents PicBxPlat10_20 As PictureBox
    Friend WithEvents PicBxPlat10_19 As PictureBox
    Friend WithEvents PicBxPlat10_18 As PictureBox
    Friend WithEvents PicBxPlat10_17 As PictureBox
    Friend WithEvents PicBxPlat10_16 As PictureBox
    Friend WithEvents PicBxPlat10_15 As PictureBox
    Friend WithEvents PicBxPlat10_14 As PictureBox
    Friend WithEvents PicBxPlat10_13 As PictureBox
    Friend WithEvents PicBxPlat10_12 As PictureBox
    Friend WithEvents PicBxPlat10_11 As PictureBox
    Friend WithEvents PicBxPlat10_10 As PictureBox
    Friend WithEvents PicBxPlat10_9 As PictureBox
    Friend WithEvents PicBxPlat10_8 As PictureBox
    Friend WithEvents PicBxPlat10_7 As PictureBox
    Friend WithEvents PicBxPlat10_6 As PictureBox
    Friend WithEvents PicBxPlat10_5 As PictureBox
    Friend WithEvents PicBxPlat10_4 As PictureBox
    Friend WithEvents PicBxPlat10_3 As PictureBox
    Friend WithEvents PicBxPlat10_2 As PictureBox
    Friend WithEvents PicBxPlat10_1 As PictureBox
    Friend WithEvents PicBxPlat9_20 As PictureBox
    Friend WithEvents PicBxPlat9_19 As PictureBox
    Friend WithEvents PicBxPlat9_18 As PictureBox
    Friend WithEvents PicBxPlat9_17 As PictureBox
    Friend WithEvents PicBxPlat9_16 As PictureBox
    Friend WithEvents PicBxPlat9_15 As PictureBox
    Friend WithEvents PicBxPlat9_14 As PictureBox
    Friend WithEvents PicBxPlat9_13 As PictureBox
    Friend WithEvents PicBxPlat9_12 As PictureBox
    Friend WithEvents PicBxPlat9_11 As PictureBox
    Friend WithEvents PicBxPlat9_10 As PictureBox
    Friend WithEvents PicBxPlat9_9 As PictureBox
    Friend WithEvents PicBxPlat9_8 As PictureBox
    Friend WithEvents PicBxPlat9_7 As PictureBox
    Friend WithEvents PicBxPlat9_6 As PictureBox
    Friend WithEvents PicBxPlat9_5 As PictureBox
    Friend WithEvents PicBxPlat9_4 As PictureBox
    Friend WithEvents PicBxPlat9_3 As PictureBox
    Friend WithEvents PicBxPlat9_2 As PictureBox
    Friend WithEvents PicBxPlat9_1 As PictureBox
    Friend WithEvents PicBxPlat8_20 As PictureBox
    Friend WithEvents PicBxPlat8_19 As PictureBox
    Friend WithEvents PicBxPlat8_18 As PictureBox
    Friend WithEvents PicBxPlat8_17 As PictureBox
    Friend WithEvents PicBxPlat8_16 As PictureBox
    Friend WithEvents PicBxPlat8_15 As PictureBox
    Friend WithEvents PicBxPlat8_14 As PictureBox
    Friend WithEvents PicBxPlat8_13 As PictureBox
    Friend WithEvents PicBxPlat8_12 As PictureBox
    Friend WithEvents PicBxPlat8_11 As PictureBox
    Friend WithEvents PicBxPlat8_10 As PictureBox
    Friend WithEvents PicBxPlat8_9 As PictureBox
    Friend WithEvents PicBxPlat8_8 As PictureBox
    Friend WithEvents PicBxPlat8_7 As PictureBox
    Friend WithEvents PicBxPlat8_6 As PictureBox
    Friend WithEvents PicBxPlat8_5 As PictureBox
    Friend WithEvents PicBxPlat8_4 As PictureBox
    Friend WithEvents PicBxPlat8_3 As PictureBox
    Friend WithEvents PicBxPlat8_2 As PictureBox
    Friend WithEvents PicBxPlat8_1 As PictureBox
    Friend WithEvents PicBxPlat7_20 As PictureBox
    Friend WithEvents PicBxPlat7_19 As PictureBox
    Friend WithEvents PicBxPlat7_18 As PictureBox
    Friend WithEvents PicBxPlat7_17 As PictureBox
    Friend WithEvents PicBxPlat7_16 As PictureBox
    Friend WithEvents PicBxPlat7_15 As PictureBox
    Friend WithEvents PicBxPlat7_14 As PictureBox
    Friend WithEvents PicBxPlat7_13 As PictureBox
    Friend WithEvents PicBxPlat7_12 As PictureBox
    Friend WithEvents PicBxPlat7_11 As PictureBox
    Friend WithEvents PicBxPlat7_10 As PictureBox
    Friend WithEvents PicBxPlat7_9 As PictureBox
    Friend WithEvents PicBxPlat7_8 As PictureBox
    Friend WithEvents PicBxPlat7_7 As PictureBox
    Friend WithEvents PicBxPlat7_6 As PictureBox
    Friend WithEvents PicBxPlat7_5 As PictureBox
    Friend WithEvents PicBxPlat7_4 As PictureBox
    Friend WithEvents PicBxPlat7_3 As PictureBox
    Friend WithEvents PicBxPlat7_2 As PictureBox
    Friend WithEvents PicBxPlat7_1 As PictureBox
    Friend WithEvents PicBxPlat12_20 As PictureBox
    Friend WithEvents PicBxPlat12_19 As PictureBox
    Friend WithEvents PicBxPlat12_18 As PictureBox
    Friend WithEvents PicBxPlat12_17 As PictureBox
    Friend WithEvents PicBxPlat12_16 As PictureBox
    Friend WithEvents PicBxPlat12_15 As PictureBox
    Friend WithEvents PicBxPlat12_14 As PictureBox
    Friend WithEvents PicBxPlat12_13 As PictureBox
    Friend WithEvents PicBxPlat12_12 As PictureBox
    Friend WithEvents PicBxPlat12_11 As PictureBox
    Friend WithEvents PicBxPlat12_10 As PictureBox
    Friend WithEvents PicBxPlat12_9 As PictureBox
    Friend WithEvents PicBxPlat12_8 As PictureBox
    Friend WithEvents PicBxPlat12_7 As PictureBox
    Friend WithEvents PicBxPlat12_6 As PictureBox
    Friend WithEvents PicBxPlat12_5 As PictureBox
    Friend WithEvents PicBxPlat12_4 As PictureBox
    Friend WithEvents PicBxPlat12_3 As PictureBox
    Friend WithEvents PicBxPlat12_2 As PictureBox
    Friend WithEvents PicBxPlat12_1 As PictureBox
    Friend WithEvents PicBxPlat11_20 As PictureBox
    Friend WithEvents PicBxPlat11_19 As PictureBox
    Friend WithEvents PicBxPlat11_18 As PictureBox
    Friend WithEvents PicBxPlat11_17 As PictureBox
    Friend WithEvents PicBxPlat11_16 As PictureBox
    Friend WithEvents PicBxPlat11_15 As PictureBox
    Friend WithEvents PicBxPlat11_14 As PictureBox
    Friend WithEvents PicBxPlat11_13 As PictureBox
    Friend WithEvents PicBxPlat11_12 As PictureBox
    Friend WithEvents PicBxPlat11_11 As PictureBox
    Friend WithEvents PicBxPlat11_10 As PictureBox
    Friend WithEvents PicBxPlat11_9 As PictureBox
    Friend WithEvents PicBxPlat11_8 As PictureBox
    Friend WithEvents PicBxPlat11_7 As PictureBox
    Friend WithEvents PicBxPlat11_6 As PictureBox
    Friend WithEvents PicBxPlat11_5 As PictureBox
    Friend WithEvents PicBxPlat11_4 As PictureBox
    Friend WithEvents PicBxPlat11_3 As PictureBox
    Friend WithEvents PicBxPlat11_2 As PictureBox
    Friend WithEvents PicBxPlat11_1 As PictureBox
    Friend WithEvents PicBxPlat14_20 As PictureBox
    Friend WithEvents PicBxPlat14_19 As PictureBox
    Friend WithEvents PicBxPlat14_18 As PictureBox
    Friend WithEvents PicBxPlat14_17 As PictureBox
    Friend WithEvents PicBxPlat14_16 As PictureBox
    Friend WithEvents PicBxPlat14_15 As PictureBox
    Friend WithEvents PicBxPlat14_14 As PictureBox
    Friend WithEvents PicBxPlat14_13 As PictureBox
    Friend WithEvents PicBxPlat14_12 As PictureBox
    Friend WithEvents PicBxPlat14_11 As PictureBox
    Friend WithEvents PicBxPlat14_10 As PictureBox
    Friend WithEvents PicBxPlat14_9 As PictureBox
    Friend WithEvents PicBxPlat14_8 As PictureBox
    Friend WithEvents PicBxPlat14_7 As PictureBox
    Friend WithEvents PicBxPlat14_6 As PictureBox
    Friend WithEvents PicBxPlat14_5 As PictureBox
    Friend WithEvents PicBxPlat14_4 As PictureBox
    Friend WithEvents PicBxPlat14_3 As PictureBox
    Friend WithEvents PicBxPlat14_2 As PictureBox
    Friend WithEvents PicBxPlat14_1 As PictureBox
    Friend WithEvents PicBxPlat13_20 As PictureBox
    Friend WithEvents PicBxPlat13_19 As PictureBox
    Friend WithEvents PicBxPlat13_18 As PictureBox
    Friend WithEvents PicBxPlat13_17 As PictureBox
    Friend WithEvents PicBxPlat13_16 As PictureBox
    Friend WithEvents PicBxPlat13_15 As PictureBox
    Friend WithEvents PicBxPlat13_14 As PictureBox
    Friend WithEvents PicBxPlat13_13 As PictureBox
    Friend WithEvents PicBxPlat13_12 As PictureBox
    Friend WithEvents PicBxPlat13_11 As PictureBox
    Friend WithEvents PicBxPlat13_10 As PictureBox
    Friend WithEvents PicBxPlat13_9 As PictureBox
    Friend WithEvents PicBxPlat13_8 As PictureBox
    Friend WithEvents PicBxPlat13_7 As PictureBox
    Friend WithEvents PicBxPlat13_6 As PictureBox
    Friend WithEvents PicBxPlat13_5 As PictureBox
    Friend WithEvents PicBxPlat13_4 As PictureBox
    Friend WithEvents PicBxPlat13_3 As PictureBox
    Friend WithEvents PicBxPlat13_2 As PictureBox
    Friend WithEvents PicBxPlat13_1 As PictureBox
    Friend WithEvents PicBxPlat16_20 As PictureBox
    Friend WithEvents PicBxPlat16_19 As PictureBox
    Friend WithEvents PicBxPlat16_18 As PictureBox
    Friend WithEvents PicBxPlat16_17 As PictureBox
    Friend WithEvents PicBxPlat16_16 As PictureBox
    Friend WithEvents PicBxPlat16_15 As PictureBox
    Friend WithEvents PicBxPlat16_14 As PictureBox
    Friend WithEvents PicBxPlat16_13 As PictureBox
    Friend WithEvents PicBxPlat16_12 As PictureBox
    Friend WithEvents PicBxPlat16_11 As PictureBox
    Friend WithEvents PicBxPlat16_10 As PictureBox
    Friend WithEvents PicBxPlat16_9 As PictureBox
    Friend WithEvents PicBxPlat16_8 As PictureBox
    Friend WithEvents PicBxPlat16_7 As PictureBox
    Friend WithEvents PicBxPlat16_6 As PictureBox
    Friend WithEvents PicBxPlat16_5 As PictureBox
    Friend WithEvents PicBxPlat16_4 As PictureBox
    Friend WithEvents PicBxPlat16_3 As PictureBox
    Friend WithEvents PicBxPlat16_2 As PictureBox
    Friend WithEvents PicBxPlat16_1 As PictureBox
    Friend WithEvents PicBxPlat15_20 As PictureBox
    Friend WithEvents PicBxPlat15_19 As PictureBox
    Friend WithEvents PicBxPlat15_18 As PictureBox
    Friend WithEvents PicBxPlat15_17 As PictureBox
    Friend WithEvents PicBxPlat15_16 As PictureBox
    Friend WithEvents PicBxPlat15_15 As PictureBox
    Friend WithEvents PicBxPlat15_14 As PictureBox
    Friend WithEvents PicBxPlat15_13 As PictureBox
    Friend WithEvents PicBxPlat15_12 As PictureBox
    Friend WithEvents PicBxPlat15_11 As PictureBox
    Friend WithEvents PicBxPlat15_10 As PictureBox
    Friend WithEvents PicBxPlat15_9 As PictureBox
    Friend WithEvents PicBxPlat15_8 As PictureBox
    Friend WithEvents PicBxPlat15_7 As PictureBox
    Friend WithEvents PicBxPlat15_6 As PictureBox
    Friend WithEvents PicBxPlat15_5 As PictureBox
    Friend WithEvents PicBxPlat15_4 As PictureBox
    Friend WithEvents PicBxPlat15_3 As PictureBox
    Friend WithEvents PicBxPlat15_2 As PictureBox
    Friend WithEvents PicBxPlat15_1 As PictureBox
    Friend WithEvents PicBxPlat18_20 As PictureBox
    Friend WithEvents PicBxPlat18_19 As PictureBox
    Friend WithEvents PicBxPlat18_18 As PictureBox
    Friend WithEvents PicBxPlat18_17 As PictureBox
    Friend WithEvents PicBxPlat18_16 As PictureBox
    Friend WithEvents PicBxPlat18_15 As PictureBox
    Friend WithEvents PicBxPlat18_14 As PictureBox
    Friend WithEvents PicBxPlat18_13 As PictureBox
    Friend WithEvents PicBxPlat18_12 As PictureBox
    Friend WithEvents PicBxPlat18_11 As PictureBox
    Friend WithEvents PicBxPlat18_10 As PictureBox
    Friend WithEvents PicBxPlat18_9 As PictureBox
    Friend WithEvents PicBxPlat18_8 As PictureBox
    Friend WithEvents PicBxPlat18_7 As PictureBox
    Friend WithEvents PicBxPlat18_6 As PictureBox
    Friend WithEvents PicBxPlat18_5 As PictureBox
    Friend WithEvents PicBxPlat18_4 As PictureBox
    Friend WithEvents PicBxPlat18_3 As PictureBox
    Friend WithEvents PicBxPlat18_2 As PictureBox
    Friend WithEvents PicBxPlat18_1 As PictureBox
    Friend WithEvents PicBxPlat17_20 As PictureBox
    Friend WithEvents PicBxPlat17_19 As PictureBox
    Friend WithEvents PicBxPlat17_18 As PictureBox
    Friend WithEvents PicBxPlat17_17 As PictureBox
    Friend WithEvents PicBxPlat17_16 As PictureBox
    Friend WithEvents PicBxPlat17_15 As PictureBox
    Friend WithEvents PicBxPlat17_14 As PictureBox
    Friend WithEvents PicBxPlat17_13 As PictureBox
    Friend WithEvents PicBxPlat17_12 As PictureBox
    Friend WithEvents PicBxPlat17_11 As PictureBox
    Friend WithEvents PicBxPlat17_10 As PictureBox
    Friend WithEvents PicBxPlat17_9 As PictureBox
    Friend WithEvents PicBxPlat17_8 As PictureBox
    Friend WithEvents PicBxPlat17_7 As PictureBox
    Friend WithEvents PicBxPlat17_6 As PictureBox
    Friend WithEvents PicBxPlat17_5 As PictureBox
    Friend WithEvents PicBxPlat17_4 As PictureBox
    Friend WithEvents PicBxPlat17_3 As PictureBox
    Friend WithEvents PicBxPlat17_2 As PictureBox
    Friend WithEvents PicBxPlat17_1 As PictureBox
    Friend WithEvents PicBxPlat20_20 As PictureBox
    Friend WithEvents PicBxPlat20_19 As PictureBox
    Friend WithEvents PicBxPlat20_18 As PictureBox
    Friend WithEvents PicBxPlat20_17 As PictureBox
    Friend WithEvents PicBxPlat20_16 As PictureBox
    Friend WithEvents PicBxPlat20_15 As PictureBox
    Friend WithEvents PicBxPlat20_14 As PictureBox
    Friend WithEvents PicBxPlat20_13 As PictureBox
    Friend WithEvents PicBxPlat20_12 As PictureBox
    Friend WithEvents PicBxPlat20_11 As PictureBox
    Friend WithEvents PicBxPlat20_10 As PictureBox
    Friend WithEvents PicBxPlat20_9 As PictureBox
    Friend WithEvents PicBxPlat20_8 As PictureBox
    Friend WithEvents PicBxPlat20_7 As PictureBox
    Friend WithEvents PicBxPlat20_6 As PictureBox
    Friend WithEvents PicBxPlat20_5 As PictureBox
    Friend WithEvents PicBxPlat20_4 As PictureBox
    Friend WithEvents PicBxPlat20_3 As PictureBox
    Friend WithEvents PicBxPlat20_2 As PictureBox
    Friend WithEvents PicBxPlat20_1 As PictureBox
    Friend WithEvents PicBxPlat19_20 As PictureBox
    Friend WithEvents PicBxPlat19_19 As PictureBox
    Friend WithEvents PicBxPlat19_18 As PictureBox
    Friend WithEvents PicBxPlat19_17 As PictureBox
    Friend WithEvents PicBxPlat19_16 As PictureBox
    Friend WithEvents PicBxPlat19_15 As PictureBox
    Friend WithEvents PicBxPlat19_14 As PictureBox
    Friend WithEvents PicBxPlat19_13 As PictureBox
    Friend WithEvents PicBxPlat19_12 As PictureBox
    Friend WithEvents PicBxPlat19_11 As PictureBox
    Friend WithEvents PicBxPlat19_10 As PictureBox
    Friend WithEvents PicBxPlat19_9 As PictureBox
    Friend WithEvents PicBxPlat19_8 As PictureBox
    Friend WithEvents PicBxPlat19_7 As PictureBox
    Friend WithEvents PicBxPlat19_6 As PictureBox
    Friend WithEvents PicBxPlat19_5 As PictureBox
    Friend WithEvents PicBxPlat19_4 As PictureBox
    Friend WithEvents PicBxPlat19_3 As PictureBox
    Friend WithEvents PicBxPlat19_2 As PictureBox
    Friend WithEvents PicBxPlat19_1 As PictureBox
    Friend WithEvents Lbl_PionRest As Label
    Friend WithEvents Lbl_JTour As Label
    Friend WithEvents Lbl_tourTxt As Label
    Friend WithEvents Lbl_PionrestTxt As Label
    Friend WithEvents Lbl_ScJ4 As Label
    Friend WithEvents Lbl_txtSJ4 As Label
    Friend WithEvents Lbl_NomJ4 As Label
    Friend WithEvents Lbl_ScJ3 As Label
    Friend WithEvents Lbl_txtSJ3 As Label
    Friend WithEvents Lbl_NomJ3 As Label
    Friend WithEvents Lbl_ScJ2 As Label
    Friend WithEvents Lbl_txtSJ2 As Label
    Friend WithEvents Lbl_NomJ2 As Label
    Friend WithEvents Lbl_ScJ1 As Label
    Friend WithEvents Lbl_txtSJ1 As Label
    Friend WithEvents Lbl_NomJ1 As Label
    Friend WithEvents Btn_Menu As Button
    Friend WithEvents Btn_Quitter As Button
    Friend WithEvents Pnl_J3 As Panel
    Friend WithEvents Pnl_J4 As Panel
End Class
