﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Paramètre
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Gp_selection = New System.Windows.Forms.GroupBox()
        Me.GB_2J = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.RBtn_PrioJ2 = New System.Windows.Forms.RadioButton()
        Me.RBtn_PrioJ1 = New System.Windows.Forms.RadioButton()
        Me.Btn_play = New System.Windows.Forms.Button()
        Me.Panel_4J = New System.Windows.Forms.Panel()
        Me.RBtn_PrioJ4 = New System.Windows.Forms.RadioButton()
        Me.TxtBx_NomJ4 = New System.Windows.Forms.TextBox()
        Me.Panel_3J = New System.Windows.Forms.Panel()
        Me.RBtn_PrioJ3 = New System.Windows.Forms.RadioButton()
        Me.TxtBx_NomJ3 = New System.Windows.Forms.TextBox()
        Me.TxtBx_NomJ2 = New System.Windows.Forms.TextBox()
        Me.TxtBx_NomJ1 = New System.Windows.Forms.TextBox()
        Me.Lbl_NomJ = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Btn_4J = New System.Windows.Forms.RadioButton()
        Me.Btn_3J = New System.Windows.Forms.RadioButton()
        Me.Btn_2J = New System.Windows.Forms.RadioButton()
        Me.Lbl_Titre = New System.Windows.Forms.Label()
        Me.Btn_Menu = New System.Windows.Forms.Button()
        Me.Gp_selection.SuspendLayout()
        Me.GB_2J.SuspendLayout()
        Me.Panel_4J.SuspendLayout()
        Me.Panel_3J.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Gp_selection
        '
        Me.Gp_selection.Controls.Add(Me.GB_2J)
        Me.Gp_selection.Controls.Add(Me.Panel1)
        Me.Gp_selection.Location = New System.Drawing.Point(160, 100)
        Me.Gp_selection.Name = "Gp_selection"
        Me.Gp_selection.Size = New System.Drawing.Size(500, 346)
        Me.Gp_selection.TabIndex = 5
        Me.Gp_selection.TabStop = False
        '
        'GB_2J
        '
        Me.GB_2J.Controls.Add(Me.Label1)
        Me.GB_2J.Controls.Add(Me.RBtn_PrioJ2)
        Me.GB_2J.Controls.Add(Me.RBtn_PrioJ1)
        Me.GB_2J.Controls.Add(Me.Btn_play)
        Me.GB_2J.Controls.Add(Me.Panel_4J)
        Me.GB_2J.Controls.Add(Me.Panel_3J)
        Me.GB_2J.Controls.Add(Me.TxtBx_NomJ2)
        Me.GB_2J.Controls.Add(Me.TxtBx_NomJ1)
        Me.GB_2J.Controls.Add(Me.Lbl_NomJ)
        Me.GB_2J.Enabled = False
        Me.GB_2J.Location = New System.Drawing.Point(46, 91)
        Me.GB_2J.Name = "GB_2J"
        Me.GB_2J.Size = New System.Drawing.Size(404, 247)
        Me.GB_2J.TabIndex = 32
        Me.GB_2J.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(224, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(119, 13)
        Me.Label1.TabIndex = 20
        Me.Label1.Text = "Joueur ayant la priorité :"
        '
        'RBtn_PrioJ2
        '
        Me.RBtn_PrioJ2.AutoSize = True
        Me.RBtn_PrioJ2.Location = New System.Drawing.Point(283, 69)
        Me.RBtn_PrioJ2.Name = "RBtn_PrioJ2"
        Me.RBtn_PrioJ2.Size = New System.Drawing.Size(14, 13)
        Me.RBtn_PrioJ2.TabIndex = 19
        Me.RBtn_PrioJ2.TabStop = True
        Me.RBtn_PrioJ2.UseVisualStyleBackColor = True
        '
        'RBtn_PrioJ1
        '
        Me.RBtn_PrioJ1.AutoSize = True
        Me.RBtn_PrioJ1.Location = New System.Drawing.Point(283, 47)
        Me.RBtn_PrioJ1.Name = "RBtn_PrioJ1"
        Me.RBtn_PrioJ1.Size = New System.Drawing.Size(14, 13)
        Me.RBtn_PrioJ1.TabIndex = 18
        Me.RBtn_PrioJ1.TabStop = True
        Me.RBtn_PrioJ1.UseVisualStyleBackColor = True
        '
        'Btn_play
        '
        Me.Btn_play.Location = New System.Drawing.Point(136, 195)
        Me.Btn_play.Name = "Btn_play"
        Me.Btn_play.Size = New System.Drawing.Size(123, 46)
        Me.Btn_play.TabIndex = 17
        Me.Btn_play.Text = "Jouer"
        Me.Btn_play.UseVisualStyleBackColor = True
        '
        'Panel_4J
        '
        Me.Panel_4J.Controls.Add(Me.RBtn_PrioJ4)
        Me.Panel_4J.Controls.Add(Me.TxtBx_NomJ4)
        Me.Panel_4J.Enabled = False
        Me.Panel_4J.Location = New System.Drawing.Point(74, 120)
        Me.Panel_4J.Name = "Panel_4J"
        Me.Panel_4J.Size = New System.Drawing.Size(282, 31)
        Me.Panel_4J.TabIndex = 16
        '
        'RBtn_PrioJ4
        '
        Me.RBtn_PrioJ4.AutoSize = True
        Me.RBtn_PrioJ4.Location = New System.Drawing.Point(209, 6)
        Me.RBtn_PrioJ4.Name = "RBtn_PrioJ4"
        Me.RBtn_PrioJ4.Size = New System.Drawing.Size(14, 13)
        Me.RBtn_PrioJ4.TabIndex = 21
        Me.RBtn_PrioJ4.TabStop = True
        Me.RBtn_PrioJ4.UseVisualStyleBackColor = True
        '
        'TxtBx_NomJ4
        '
        Me.TxtBx_NomJ4.Location = New System.Drawing.Point(25, 2)
        Me.TxtBx_NomJ4.Name = "TxtBx_NomJ4"
        Me.TxtBx_NomJ4.Size = New System.Drawing.Size(100, 20)
        Me.TxtBx_NomJ4.TabIndex = 7
        Me.TxtBx_NomJ4.Text = "Nom du joueur 4"
        '
        'Panel_3J
        '
        Me.Panel_3J.Controls.Add(Me.RBtn_PrioJ3)
        Me.Panel_3J.Controls.Add(Me.TxtBx_NomJ3)
        Me.Panel_3J.Enabled = False
        Me.Panel_3J.Location = New System.Drawing.Point(74, 93)
        Me.Panel_3J.Name = "Panel_3J"
        Me.Panel_3J.Size = New System.Drawing.Size(282, 31)
        Me.Panel_3J.TabIndex = 15
        '
        'RBtn_PrioJ3
        '
        Me.RBtn_PrioJ3.AutoSize = True
        Me.RBtn_PrioJ3.Location = New System.Drawing.Point(209, 6)
        Me.RBtn_PrioJ3.Name = "RBtn_PrioJ3"
        Me.RBtn_PrioJ3.Size = New System.Drawing.Size(14, 13)
        Me.RBtn_PrioJ3.TabIndex = 20
        Me.RBtn_PrioJ3.TabStop = True
        Me.RBtn_PrioJ3.UseVisualStyleBackColor = True
        '
        'TxtBx_NomJ3
        '
        Me.TxtBx_NomJ3.Location = New System.Drawing.Point(25, 3)
        Me.TxtBx_NomJ3.Name = "TxtBx_NomJ3"
        Me.TxtBx_NomJ3.Size = New System.Drawing.Size(100, 20)
        Me.TxtBx_NomJ3.TabIndex = 7
        Me.TxtBx_NomJ3.Text = "Nom du joueur 3"
        '
        'TxtBx_NomJ2
        '
        Me.TxtBx_NomJ2.Location = New System.Drawing.Point(99, 66)
        Me.TxtBx_NomJ2.Name = "TxtBx_NomJ2"
        Me.TxtBx_NomJ2.Size = New System.Drawing.Size(100, 20)
        Me.TxtBx_NomJ2.TabIndex = 6
        Me.TxtBx_NomJ2.Text = "Nom du joueur 2"
        '
        'TxtBx_NomJ1
        '
        Me.TxtBx_NomJ1.Location = New System.Drawing.Point(99, 40)
        Me.TxtBx_NomJ1.Name = "TxtBx_NomJ1"
        Me.TxtBx_NomJ1.Size = New System.Drawing.Size(100, 20)
        Me.TxtBx_NomJ1.TabIndex = 5
        Me.TxtBx_NomJ1.Text = "Nom du joueur 1"
        '
        'Lbl_NomJ
        '
        Me.Lbl_NomJ.AutoSize = True
        Me.Lbl_NomJ.Location = New System.Drawing.Point(107, 16)
        Me.Lbl_NomJ.Name = "Lbl_NomJ"
        Me.Lbl_NomJ.Size = New System.Drawing.Size(92, 13)
        Me.Lbl_NomJ.TabIndex = 14
        Me.Lbl_NomJ.Text = "Nom des joueurs :"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Btn_4J)
        Me.Panel1.Controls.Add(Me.Btn_3J)
        Me.Panel1.Controls.Add(Me.Btn_2J)
        Me.Panel1.Location = New System.Drawing.Point(46, 44)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(404, 41)
        Me.Panel1.TabIndex = 31
        '
        'Btn_4J
        '
        Me.Btn_4J.Appearance = System.Windows.Forms.Appearance.Button
        Me.Btn_4J.AutoSize = True
        Me.Btn_4J.Location = New System.Drawing.Point(302, 14)
        Me.Btn_4J.Name = "Btn_4J"
        Me.Btn_4J.Size = New System.Drawing.Size(63, 23)
        Me.Btn_4J.TabIndex = 21
        Me.Btn_4J.TabStop = True
        Me.Btn_4J.Text = "4 Joueurs"
        Me.Btn_4J.UseVisualStyleBackColor = True
        '
        'Btn_3J
        '
        Me.Btn_3J.Appearance = System.Windows.Forms.Appearance.Button
        Me.Btn_3J.AutoSize = True
        Me.Btn_3J.Location = New System.Drawing.Point(159, 14)
        Me.Btn_3J.Name = "Btn_3J"
        Me.Btn_3J.Size = New System.Drawing.Size(63, 23)
        Me.Btn_3J.TabIndex = 20
        Me.Btn_3J.TabStop = True
        Me.Btn_3J.Text = "3 Joueurs"
        Me.Btn_3J.UseVisualStyleBackColor = True
        '
        'Btn_2J
        '
        Me.Btn_2J.Appearance = System.Windows.Forms.Appearance.Button
        Me.Btn_2J.AutoSize = True
        Me.Btn_2J.Location = New System.Drawing.Point(34, 14)
        Me.Btn_2J.Name = "Btn_2J"
        Me.Btn_2J.Size = New System.Drawing.Size(63, 23)
        Me.Btn_2J.TabIndex = 19
        Me.Btn_2J.TabStop = True
        Me.Btn_2J.Text = "2 Joueurs"
        Me.Btn_2J.UseVisualStyleBackColor = True
        '
        'Lbl_Titre
        '
        Me.Lbl_Titre.AutoSize = True
        Me.Lbl_Titre.Font = New System.Drawing.Font("Microsoft Sans Serif", 32.0!)
        Me.Lbl_Titre.ForeColor = System.Drawing.Color.Red
        Me.Lbl_Titre.Location = New System.Drawing.Point(320, 40)
        Me.Lbl_Titre.Name = "Lbl_Titre"
        Me.Lbl_Titre.Size = New System.Drawing.Size(166, 51)
        Me.Lbl_Titre.TabIndex = 6
        Me.Lbl_Titre.Text = "Qwirkle"
        '
        'Btn_Menu
        '
        Me.Btn_Menu.Location = New System.Drawing.Point(39, 29)
        Me.Btn_Menu.Name = "Btn_Menu"
        Me.Btn_Menu.Size = New System.Drawing.Size(95, 23)
        Me.Btn_Menu.TabIndex = 7
        Me.Btn_Menu.Text = "Retour au menu"
        Me.Btn_Menu.UseVisualStyleBackColor = True
        '
        'Paramètre
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.Btn_Menu)
        Me.Controls.Add(Me.Lbl_Titre)
        Me.Controls.Add(Me.Gp_selection)
        Me.Name = "Paramètre"
        Me.Text = "Paramètre"
        Me.Gp_selection.ResumeLayout(False)
        Me.GB_2J.ResumeLayout(False)
        Me.GB_2J.PerformLayout()
        Me.Panel_4J.ResumeLayout(False)
        Me.Panel_4J.PerformLayout()
        Me.Panel_3J.ResumeLayout(False)
        Me.Panel_3J.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Gp_selection As GroupBox
    Friend WithEvents Lbl_Titre As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Btn_4J As RadioButton
    Friend WithEvents Btn_3J As RadioButton
    Friend WithEvents Btn_2J As RadioButton
    Friend WithEvents GB_2J As GroupBox
    Friend WithEvents Panel_4J As Panel
    Friend WithEvents TxtBx_NomJ4 As TextBox
    Friend WithEvents Panel_3J As Panel
    Friend WithEvents TxtBx_NomJ3 As TextBox
    Friend WithEvents TxtBx_NomJ2 As TextBox
    Friend WithEvents TxtBx_NomJ1 As TextBox
    Friend WithEvents Lbl_NomJ As Label
    Friend WithEvents Btn_play As Button
    Friend WithEvents Btn_Menu As Button
    Friend WithEvents RBtn_PrioJ2 As RadioButton
    Friend WithEvents RBtn_PrioJ1 As RadioButton
    Friend WithEvents RBtn_PrioJ4 As RadioButton
    Friend WithEvents RBtn_PrioJ3 As RadioButton
    Friend WithEvents Label1 As Label
End Class
