﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Partie
    {/* Cette classe contient des méthodes destiné à simplifier le développement VB du déroulement de la partie. */
        public void Quicommence2j(Joueur j1, Joueur j2) /* Cette méthode permet de choisir dans une partie de 2 joueurs qui joue en 1er selon les règles du Qwirkle. */
        {
            int indice, testcolor=0, testforme, couleur1=0,couleur2=0,couleur3=0,couleur4=0,couleur5=0,couleur6=0,forme1=0,forme2=0,forme3=0,forme4=0,forme5=0,forme6=0;
            int maxj1=0,maxj2=0;
            /* Le joueur qui peux jouer en 1er est celui qui peut faire le plus long enchainement de tuile, donc celui qui possède le plus de tuile de la même forme et/ou couleur. */
            /* On utilise donc un Switch case dans une boucle for pour faire la somme de chaque tuile de même forme et couleur dans la main du joueur. */
            for (indice = 1; indice <= 6; indice++)
            {
                testcolor = j1.Getmainpos(indice).Getcouleur();
                switch (testcolor)
                {
                    case 10:
                        {
                            couleur1++;
                            break;
                        }
                    case 20:
                        {
                            couleur2++;
                            break;
                        }
                    case 30:
                        {
                            couleur3++;
                            break;
                        }
                    case 40:
                        {
                            couleur4++;
                            break;
                        }
                    case 50:
                        {
                            couleur5++;
                            break;
                        }
                    case 60:
                        {
                            couleur6++;
                            break;
                        }
                    default:
                        {
                            throw new Exception("Couleur de la tuile non valide( Doit être une dizaine entre 10 et 60 inclus)");
                        }
                }
                testforme = j1.Getmainpos(indice).Getforme();
                switch (testforme)
                {
                    case 1:
                        {
                            forme1++;
                            break;
                        }
                    case 2:
                        {
                            forme2++;
                            break;
                        }
                    case 3:
                        {
                            forme3++;
                            break;
                        }
                    case 4:
                        {
                            forme4++;
                            break;
                        }
                    case 5:
                        {
                            forme5++;
                            break;
                        }
                    case 6:
                        {
                            forme6++;
                            break;
                        }
                    default:
                        {
                            throw new Exception("Forme de la tuile non valide (doit être un entier entre 1 et 6 inclus)");
                        }

                }
            }
            /* On range ces sommes dans une liste et on extrait le maximum, qui correspond à la longueur du plus long enchainement de tuile que le joueur peux faire. */
            List<int> listej1 = new List<int>();
            listej1.Add(couleur1);
            listej1.Add(couleur2);
            listej1.Add(couleur3);
            listej1.Add(couleur4);
            listej1.Add(couleur5);
            listej1.Add(couleur6);
            listej1.Add(forme1);
            listej1.Add(forme2);
            listej1.Add(forme3);
            listej1.Add(forme4);
            listej1.Add(forme5);
            listej1.Add(forme6);
            maxj1 = listej1.Max();

            couleur1 = 0; couleur2 = 0; couleur3 = 0; couleur4 = 0; couleur5 = 0; couleur6 = 0; forme1 = 0; forme2 = 0; forme3 = 0; forme4 = 0; forme5 = 0; forme6 = 0;
            /* On remet les valeurs à zéro pour faire la même chose pour le joueur suivant. */
            for (indice = 1; indice <= 6; indice++)
            {
                testcolor = j2.Getmainpos(indice).Getcouleur();
                switch (testcolor)
                {
                    case 10:
                        {
                            couleur1++;
                            break;
                        }
                    case 20:
                        {
                            couleur2++;
                            break;
                        }
                    case 30:
                        {
                            couleur3++;
                            break;
                        }
                    case 40:
                        {
                            couleur4++;
                            break;
                        }
                    case 50:
                        {
                            couleur5++;
                            break;
                        }
                    case 60:
                        {
                            couleur6++;
                            break;
                        }
                    default:
                        {
                            throw new Exception("Couleur de la tuile non valide( Doit être une dizaine entre 10 et 60 inclus)");
                        }
                }
                testforme = j2.Getmainpos(indice).Getforme();
                switch (testforme)
                {
                    case 1:
                        {
                            forme1++;
                            break;
                        }
                    case 2:
                        {
                            forme2++;
                            break;
                        }
                    case 3:
                        {
                            forme3++;
                            break;
                        }
                    case 4:
                        {
                            forme4++;
                            break;
                        }
                    case 5:
                        {
                            forme5++;
                            break;
                        }
                    case 6:
                        {
                            forme6++;
                            break;
                        }
                    default:
                        {
                            throw new Exception("Forme de la tuile non valide (doit être un entier entre 1 et 6 inclus)");
                        }
                }
            }
            
            List<int> listej2 = new List<int>();
            listej2.Add(couleur1);
            listej2.Add(couleur2);
            listej2.Add(couleur3);
            listej2.Add(couleur4);
            listej2.Add(couleur5);
            listej2.Add(couleur6);
            listej2.Add(forme1);
            listej2.Add(forme2);
            listej2.Add(forme3);
            listej2.Add(forme4);
            listej2.Add(forme5);
            listej2.Add(forme6);
            maxj2 = listej2.Max();
            /* On compare les maximums des 2 joueurs pour définir celui qui joue en premier et définir les priorité en fonction. */
            if (maxj1 > maxj2)
            {
                j1.SetPriorite(1);
                j2.SetPriorite(2);
            }
            else
            {
                j1.SetPriorite(2);
                j2.SetPriorite(1);
            }
        }

        /* Les 2 méthodes suivantes fonctionnent comme la précédente mais avec une boucle FOR pour chaque joueur en plus et un système de comparaison différent pour éviter d'avoir trop de IF ELSE. */
        public void Quicommence3j(Joueur j1, Joueur j2, Joueur j3) /* Cette méthode permet de choisir dans une partie de 3 joueurs qui joue en 1er selon les règles du Qwirkle. */
        {
            int indice, testcolor = 0, testforme, couleur1 = 0, couleur2 = 0, couleur3 = 0, couleur4 = 0, couleur5 = 0, couleur6 = 0, forme1 = 0, forme2 = 0, forme3 = 0, forme4 = 0, forme5 = 0, forme6 = 0;
            int maxj1 = 0, maxj2 = 0, maxj3 = 0;
            for (indice = 1; indice <= 6; indice++)
            {
                testcolor = j1.Getmainpos(indice).Getcouleur();
                switch (testcolor)
                {
                    case 10:
                        {
                            couleur1++;
                            break;
                        }
                    case 20:
                        {
                            couleur2++;
                            break;
                        }
                    case 30:
                        {
                            couleur3++;
                            break;
                        }
                    case 40:
                        {
                            couleur4++;
                            break;
                        }
                    case 50:
                        {
                            couleur5++;
                            break;
                        }
                    case 60:
                        {
                            couleur6++;
                            break;
                        }
                    default:
                        {
                            throw new Exception("Couleur de la tuile non valide( Doit être une dizaine entre 10 et 60 inclus)");
                        }
                }
                testforme = j1.Getmainpos(indice).Getforme();
                switch (testforme)
                {
                    case 1:
                        {
                            forme1++;
                            break;
                        }
                    case 2:
                        {
                            forme2++;
                            break;
                        }
                    case 3:
                        {
                            forme3++;
                            break;
                        }
                    case 4:
                        {
                            forme4++;
                            break;
                        }
                    case 5:
                        {
                            forme5++;
                            break;
                        }
                    case 6:
                        {
                            forme6++;
                            break;
                        }
                    default:
                        {
                            throw new Exception("Forme de la tuile non valide (doit être un entier entre 1 et 6 inclus)");
                        }

                }
            }
            List<int> listej1 = new List<int>();
            listej1.Add(couleur1);
            listej1.Add(couleur2);
            listej1.Add(couleur3);
            listej1.Add(couleur4);
            listej1.Add(couleur5);
            listej1.Add(couleur6);
            listej1.Add(forme1);
            listej1.Add(forme2);
            listej1.Add(forme3);
            listej1.Add(forme4);
            listej1.Add(forme5);
            listej1.Add(forme6);
            maxj1 = listej1.Max();

            couleur1 = 0; couleur2 = 0; couleur3 = 0; couleur4 = 0; couleur5 = 0; couleur6 = 0; forme1 = 0; forme2 = 0; forme3 = 0; forme4 = 0; forme5 = 0; forme6 = 0;

            for (indice = 1; indice <= 6; indice++)
            {
                testcolor = j2.Getmainpos(indice).Getcouleur();
                switch (testcolor)
                {
                    case 10:
                        {
                            couleur1++;
                            break;
                        }
                    case 20:
                        {
                            couleur2++;
                            break;
                        }
                    case 30:
                        {
                            couleur3++;
                            break;
                        }
                    case 40:
                        {
                            couleur4++;
                            break;
                        }
                    case 50:
                        {
                            couleur5++;
                            break;
                        }
                    case 60:
                        {
                            couleur6++;
                            break;
                        }
                    default:
                        {
                            throw new Exception("Couleur de la tuile non valide( Doit être une dizaine entre 10 et 60 inclus)");
                        }
                }
                testforme = j2.Getmainpos(indice).Getforme();
                switch (testforme)
                {
                    case 1:
                        {
                            forme1++;
                            break;
                        }
                    case 2:
                        {
                            forme2++;
                            break;
                        }
                    case 3:
                        {
                            forme3++;
                            break;
                        }
                    case 4:
                        {
                            forme4++;
                            break;
                        }
                    case 5:
                        {
                            forme5++;
                            break;
                        }
                    case 6:
                        {
                            forme6++;
                            break;
                        }
                    default:
                        {
                            throw new Exception("Forme de la tuile non valide (doit être un entier entre 1 et 6 inclus)");
                        }
                }
            }

            List<int> listej2 = new List<int>();
            listej2.Add(couleur1);
            listej2.Add(couleur2);
            listej2.Add(couleur3);
            listej2.Add(couleur4);
            listej2.Add(couleur5);
            listej2.Add(couleur6);
            listej2.Add(forme1);
            listej2.Add(forme2);
            listej2.Add(forme3);
            listej2.Add(forme4);
            listej2.Add(forme5);
            listej2.Add(forme6);
            maxj2 = listej2.Max();

            couleur1 = 0; couleur2 = 0; couleur3 = 0; couleur4 = 0; couleur5 = 0; couleur6 = 0; forme1 = 0; forme2 = 0; forme3 = 0; forme4 = 0; forme5 = 0; forme6 = 0;

            for (indice = 1; indice <= 6; indice++)
            {
                testcolor = j3.Getmainpos(indice).Getcouleur();
                switch (testcolor)
                {
                    case 10:
                        {
                            couleur1++;
                            break;
                        }
                    case 20:
                        {
                            couleur2++;
                            break;
                        }
                    case 30:
                        {
                            couleur3++;
                            break;
                        }
                    case 40:
                        {
                            couleur4++;
                            break;
                        }
                    case 50:
                        {
                            couleur5++;
                            break;
                        }
                    case 60:
                        {
                            couleur6++;
                            break;
                        }
                    default:
                        {
                            throw new Exception("Couleur de la tuile non valide( Doit être une dizaine entre 10 et 60 inclus)");
                        }
                }
                testforme = j3.Getmainpos(indice).Getforme();
                switch (testforme)
                {
                    case 1:
                        {
                            forme1++;
                            break;
                        }
                    case 2:
                        {
                            forme2++;
                            break;
                        }
                    case 3:
                        {
                            forme3++;
                            break;
                        }
                    case 4:
                        {
                            forme4++;
                            break;
                        }
                    case 5:
                        {
                            forme5++;
                            break;
                        }
                    case 6:
                        {
                            forme6++;
                            break;
                        }
                    default:
                        {
                            throw new Exception("Forme de la tuile non valide (doit être un entier entre 1 et 6 inclus)");
                        }
                }
            }
            
            List<int> listej3 = new List<int>();
            listej3.Add(couleur1);
            listej3.Add(couleur2);
            listej3.Add(couleur3);
            listej3.Add(couleur4);
            listej3.Add(couleur5);
            listej3.Add(couleur6);
            listej3.Add(forme1);
            listej3.Add(forme2);
            listej3.Add(forme3);
            listej3.Add(forme4);
            listej3.Add(forme5);
            listej3.Add(forme6);
            maxj3 = listej3.Max();

            int max = maxj1;

            j1.SetPriorite(1);
            j2.SetPriorite(2);
            j3.SetPriorite(3);


            if (max<maxj2)
            {
                j1.SetPriorite(3);
                j2.SetPriorite(1);
                j3.SetPriorite(2);
            }
            if (max<maxj3)
            {
                j1.SetPriorite(2);
                j2.SetPriorite(3);
                j3.SetPriorite(1);
            }

        }
        public void Quicommence4j(Joueur j1, Joueur j2, Joueur j3, Joueur j4) /* Cette méthode permet de choisir dans une partie de 4 joueurs qui joue en 1er selon les règles du Qwirkle. */
        {
            int indice, testcolor = 0, testforme, couleur1 = 0, couleur2 = 0, couleur3 = 0, couleur4 = 0, couleur5 = 0, couleur6 = 0, forme1 = 0, forme2 = 0, forme3 = 0, forme4 = 0, forme5 = 0, forme6 = 0;
            int maxj1 = 0, maxj2 = 0, maxj3 = 0, maxj4 = 0;
            for (indice = 1; indice <= 6; indice++)
            {
                testcolor = j1.Getmainpos(indice).Getcouleur();
                switch (testcolor)
                {
                    case 10:
                        {
                            couleur1++;
                            break;
                        }
                    case 20:
                        {
                            couleur2++;
                            break;
                        }
                    case 30:
                        {
                            couleur3++;
                            break;
                        }
                    case 40:
                        {
                            couleur4++;
                            break;
                        }
                    case 50:
                        {
                            couleur5++;
                            break;
                        }
                    case 60:
                        {
                            couleur6++;
                            break;
                        }
                    default:
                        {
                            throw new Exception("Couleur de la tuile non valide( Doit être une dizaine entre 10 et 60 inclus)");
                        }
                }
                testforme = j1.Getmainpos(indice).Getforme();
                switch (testforme)
                {
                    case 1:
                        {
                            forme1++;
                            break;
                        }
                    case 2:
                        {
                            forme2++;
                            break;
                        }
                    case 3:
                        {
                            forme3++;
                            break;
                        }
                    case 4:
                        {
                            forme4++;
                            break;
                        }
                    case 5:
                        {
                            forme5++;
                            break;
                        }
                    case 6:
                        {
                            forme6++;
                            break;
                        }
                    default:
                        {
                            throw new Exception("Forme de la tuile non valide (doit être un entier entre 1 et 6 inclus)");
                        }

                }
            }
            List<int> listej1 = new List<int>();
            listej1.Add(couleur1);
            listej1.Add(couleur2);
            listej1.Add(couleur3);
            listej1.Add(couleur4);
            listej1.Add(couleur5);
            listej1.Add(couleur6);
            listej1.Add(forme1);
            listej1.Add(forme2);
            listej1.Add(forme3);
            listej1.Add(forme4);
            listej1.Add(forme5);
            listej1.Add(forme6);
            maxj1 = listej1.Max();

            couleur1 = 0; couleur2 = 0; couleur3 = 0; couleur4 = 0; couleur5 = 0; couleur6 = 0; forme1 = 0; forme2 = 0; forme3 = 0; forme4 = 0; forme5 = 0; forme6 = 0;

            for (indice = 1; indice <= 6; indice++)
            {
                testcolor = j2.Getmainpos(indice).Getcouleur();
                switch (testcolor)
                {
                    case 10:
                        {
                            couleur1++;
                            break;
                        }
                    case 20:
                        {
                            couleur2++;
                            break;
                        }
                    case 30:
                        {
                            couleur3++;
                            break;
                        }
                    case 40:
                        {
                            couleur4++;
                            break;
                        }
                    case 50:
                        {
                            couleur5++;
                            break;
                        }
                    case 60:
                        {
                            couleur6++;
                            break;
                        }
                    default:
                        {
                            throw new Exception("Couleur de la tuile non valide( Doit être une dizaine entre 10 et 60 inclus)");
                        }
                }
                testforme = j2.Getmainpos(indice).Getforme();
                switch (testforme)
                {
                    case 1:
                        {
                            forme1++;
                            break;
                        }
                    case 2:
                        {
                            forme2++;
                            break;
                        }
                    case 3:
                        {
                            forme3++;
                            break;
                        }
                    case 4:
                        {
                            forme4++;
                            break;
                        }
                    case 5:
                        {
                            forme5++;
                            break;
                        }
                    case 6:
                        {
                            forme6++;
                            break;
                        }
                    default:
                        {
                            throw new Exception("Forme de la tuile non valide (doit être un entier entre 1 et 6 inclus)");
                        }
                }
            }

            List<int> listej2 = new List<int>();
            listej2.Add(couleur1);
            listej2.Add(couleur2);
            listej2.Add(couleur3);
            listej2.Add(couleur4);
            listej2.Add(couleur5);
            listej2.Add(couleur6);
            listej2.Add(forme1);
            listej2.Add(forme2);
            listej2.Add(forme3);
            listej2.Add(forme4);
            listej2.Add(forme5);
            listej2.Add(forme6);
            maxj2 = listej2.Max();

            couleur1 = 0; couleur2 = 0; couleur3 = 0; couleur4 = 0; couleur5 = 0; couleur6 = 0; forme1 = 0; forme2 = 0; forme3 = 0; forme4 = 0; forme5 = 0; forme6 = 0;

            for (indice = 1; indice <= 6; indice++)
            {
                testcolor = j3.Getmainpos(indice).Getcouleur();
                switch (testcolor)
                {
                    case 10:
                        {
                            couleur1++;
                            break;
                        }
                    case 20:
                        {
                            couleur2++;
                            break;
                        }
                    case 30:
                        {
                            couleur3++;
                            break;
                        }
                    case 40:
                        {
                            couleur4++;
                            break;
                        }
                    case 50:
                        {
                            couleur5++;
                            break;
                        }
                    case 60:
                        {
                            couleur6++;
                            break;
                        }
                    default:
                        {
                            throw new Exception("Couleur de la tuile non valide( Doit être une dizaine entre 10 et 60 inclus)");
                        }
                }
                testforme = j3.Getmainpos(indice).Getforme();
                switch (testforme)
                {
                    case 1:
                        {
                            forme1++;
                            break;
                        }
                    case 2:
                        {
                            forme2++;
                            break;
                        }
                    case 3:
                        {
                            forme3++;
                            break;
                        }
                    case 4:
                        {
                            forme4++;
                            break;
                        }
                    case 5:
                        {
                            forme5++;
                            break;
                        }
                    case 6:
                        {
                            forme6++;
                            break;
                        }
                    default:
                        {
                            throw new Exception("Forme de la tuile non valide (doit être un entier entre 1 et 6 inclus)");
                        }
                }
            }

            List<int> listej3 = new List<int>();
            listej3.Add(couleur1);
            listej3.Add(couleur2);
            listej3.Add(couleur3);
            listej3.Add(couleur4);
            listej3.Add(couleur5);
            listej3.Add(couleur6);
            listej3.Add(forme1);
            listej3.Add(forme2);
            listej3.Add(forme3);
            listej3.Add(forme4);
            listej3.Add(forme5);
            listej3.Add(forme6);
            maxj3 = listej3.Max();

            couleur1 = 0; couleur2 = 0; couleur3 = 0; couleur4 = 0; couleur5 = 0; couleur6 = 0; forme1 = 0; forme2 = 0; forme3 = 0; forme4 = 0; forme5 = 0; forme6 = 0;

            for (indice = 1; indice <= 6; indice++)
            {
                testcolor = j4.Getmainpos(indice).Getcouleur();
                switch (testcolor)
                {
                    case 10:
                        {
                            couleur1++;
                            break;
                        }
                    case 20:
                        {
                            couleur2++;
                            break;
                        }
                    case 30:
                        {
                            couleur3++;
                            break;
                        }
                    case 40:
                        {
                            couleur4++;
                            break;
                        }
                    case 50:
                        {
                            couleur5++;
                            break;
                        }
                    case 60:
                        {
                            couleur6++;
                            break;
                        }
                    default:
                        {
                            throw new Exception("Couleur de la tuile non valide( Doit être une dizaine entre 10 et 60 inclus)");
                        }
                }
                testforme = j4.Getmainpos(indice).Getforme();
                switch (testforme)
                {
                    case 1:
                        {
                            forme1++;
                            break;
                        }
                    case 2:
                        {
                            forme2++;
                            break;
                        }
                    case 3:
                        {
                            forme3++;
                            break;
                        }
                    case 4:
                        {
                            forme4++;
                            break;
                        }
                    case 5:
                        {
                            forme5++;
                            break;
                        }
                    case 6:
                        {
                            forme6++;
                            break;
                        }
                    default:
                        {
                            throw new Exception("Forme de la tuile non valide (doit être un entier entre 1 et 6 inclus)");
                        }
                }
            }

            List<int> listej4 = new List<int>();
            listej4.Add(couleur1);
            listej4.Add(couleur2);
            listej4.Add(couleur3);
            listej4.Add(couleur4);
            listej4.Add(couleur5);
            listej4.Add(couleur6);
            listej4.Add(forme1);
            listej4.Add(forme2);
            listej4.Add(forme3);
            listej4.Add(forme4);
            listej4.Add(forme5);
            listej4.Add(forme6);
            maxj4 = listej4.Max();

            int max;
            max = maxj1;
            j1.SetPriorite(1);
            j2.SetPriorite(2);
            j3.SetPriorite(3);
            j4.SetPriorite(4);

            if (max < maxj2)
            {
                j1.SetPriorite(4);
                j2.SetPriorite(1);
                j3.SetPriorite(2);
                j4.SetPriorite(3);
            }

            if (max < maxj3)
            {
                j1.SetPriorite(3);
                j2.SetPriorite(4);
                j3.SetPriorite(1);
                j4.SetPriorite(2);
            }

            if (max < maxj4)
            {
                j1.SetPriorite(2);
                j2.SetPriorite(3);
                j3.SetPriorite(4);
                j4.SetPriorite(1);
            }
        }
    }
}
