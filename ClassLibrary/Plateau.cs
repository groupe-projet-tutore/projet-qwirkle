﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Plateau
    {
        private Tuile[,] tabtuil;
        private int TailleTableau;

        public Plateau()
        {
            TailleTableau = 20;
            for (int cpt = 0; cpt < TailleTableau; cpt++)
            {
                for (int cpt2 = 0; cpt2 < TailleTableau; cpt2++)
                {
                    tabtuil[cpt, cpt2] = null;
                }
            }
        }

        public Plateau(Tuile[,] tabtuil)
        {
            TailleTableau = (tabtuil.Length / 10);
            this.tabtuil = new Tuile[TailleTableau, TailleTableau];
        }

        /* GetTuile permet d'avoir la tuile qui se trouve à la position "ligne" et "colonne" (deux entiers) qui ont été rentrés en paramètre à l'appel de la fonction
         */

        public Tuile GetTuile(int ligne, int colonne)
        {
            return tabtuil[ligne, colonne];
        }

        /* cette méthode fixe la variable "TailleTableau" */

        public void SetTailleTableau(int taille)
        {
            TailleTableau = taille;
        }

        /* Cette fonction permet d'agrandir le tableau */

        public void TabAgrandir()
        {
            Tuile[,] tab = this.tabtuil;
            this.tabtuil = new Tuile[TailleTableau + 1, TailleTableau + 1];
            int cpt, cpt2;
            for (cpt = 0; cpt < this.TailleTableau; cpt++)
            {
                for (cpt2 = 0; cpt2 < this.TailleTableau; cpt2++)
                {
                    if (cpt >= 10)
                    {
                        break;
                    }
                    else
                    {
                        this.tabtuil[cpt, cpt2] = tab[cpt, cpt2];
                    }
                }
            }
            this.TailleTableau++;
        }

        /* appelé lors du placement des tuiles dans un tableau, cette fonction renvoie un booléen à true ou false si, oui ou non, la tuile a bien été posée */

        public bool placement(Tuile tuile, int x, int y)
        {
            if (this.tabtuil[x, y] == null)
            {
                tabtuil[x, y] = tuile;
                return true;
            }
            else
            {
                return false;
            }
        }

        public int GetTailletab()
        {
            return TailleTableau;
        }

        /*
         GetIDTuile permet d'avoir l'ID d'une tuile qui se trouve à une certaine position, dont les valeur x et y, ici nommées ligne et colonne, sont
         rentrée en paramètre
         */

        public int GetIDtuile(int ligne, int colonne)
        {
            int iD = '\0';
            if (tabtuil[ligne, colonne] != null)
            {
                iD = tabtuil[ligne, colonne].GetID();
            }
            return iD;
        }

        public Tuile[,] GetTab()
        {
            return tabtuil;
        }


    }
}