﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Pioche : Liste
    {
        private int tuilesrestantes;

        public Pioche(List<Tuile> list) : base(list)
        {

        }

        /* En suivant la création du laboratoire, le constructeur de la classe pioche sera donc un constructeur où l'on rentrera un tableau de tuile
         ce tableau sera ensuite convertit dans la liste héritée de la classe mère "Liste" et les tuiles restantes seront incrémentées de 1 pour chaque tuiles ajoutées
         */

        public Pioche(Tuile[] tabt) : base(tabt)
        {
            tabtuil = new List<Tuile>();
            this.tuilesrestantes = 0;
            foreach (Tuile tuile in tabt)
            {
                this.tabtuil.Add(tuile);
                tuilesrestantes++;
            }
        }

        /* Cette méthode permet de "Mélanger" la pioche. Mélanger est un bien grand mot puisque, étant une liste importante de tuiles, on a fait beaucoup plus simplement :
        avec les listes, il existe une méthode "InsertRange" qui est une méthode permettant de rentrer des instances de ce que doit contenir la liste (ici des tuiles) en
        fonction d'une valeur entière entrée en paramètre */

        public void MelangePioche(List<Tuile> renvoyee)
        {
            Random alea = new Random();
            int cpt;
            this.tuilesrestantes += renvoyee.Count;
            for (int cpt2 = 0; cpt2 < renvoyee.Count; cpt2++)
            {
                cpt = alea.Next(tuilesrestantes - 1);
                tabtuil.Insert(cpt, renvoyee[cpt2]);
            }

        }

        /* Cette méthode n'est appellée qu'au début de la partie, pour pouvoir mélanger ce qui est sortit du laboratoire, puisque tout est trié dans un ordre très précis
        nous définissons un nombre aléatoire et nous mettons dans une variable la taille de la liste
        Pour pouvoir mélanger, nous utilisons une instance de Tuile qui va prendre la valeur d'une des cases de la liste, choisis avec l'aléatoire. Ensuite cette même case va prendre la valeur maximale
        et la valeur maximale va prendre la valeur de l'instance de Tuile*/

        public void MelangeCreaPioche()
        {
            Random rdm = new Random();
            int cptmax = tabtuil.Count - 1;
            foreach (Tuile tuile in tabtuil)
            {
                int cpt = rdm.Next(cptmax);
                Tuile repos = tabtuil[cpt];
                tabtuil[cpt] = tabtuil[cptmax];
                tabtuil[cptmax] = repos;
                cptmax--;
            }
        }

        /* Non très difficile à comprendre son but, cette méthode permet de piocher des tuiles de la liste de tuile. Il n'y a pas un choix de la tuile à prendre dans cette liste
         puisque, bien inconnue de tous, les tuiles qui y sont placées au début de la partie sont mises aléatoirement */

        public List<Tuile> Piocher(List<Tuile> ListTuil)
        {
            int tuiles = 6 - ListTuil.Count;
            List<Tuile> tab3 = new List<Tuile>();
            for (int cpt = 0; cpt < tuiles; cpt++)
            {
                tab3.Add(tabtuil[tuilesrestantes - 1]);
                tabtuil.Remove(tabtuil[tuilesrestantes - 1]);
                this.tuilesrestantes--;
            }
            return tab3;
        }


        /*
        Retourne combien il reste de tuile dans la pioche
         */
        public int Nbtuilespioche()
        {
            return this.tuilesrestantes;
        }
    }
}
