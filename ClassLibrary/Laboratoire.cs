﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Laboratoire
    {
        public static Tuile CreateTuile(int ID) /* Permet de créer une tuile et de définir ses attributs à partir de son ID */
        {
            string Strid = string.Format("{0}", ID); /* Récupère l'ID(un int) et le transforme en chaine de charactère "Strid" */
            /* Puis il découpe Strid en 3 et retransforme ces sous-chaines en entier et effectue des multiplications pour récupérer les attributs forme, couleur et backgroundcolor. */
            int.TryParse(Strid.Substring(2, 1), out int forme);
            int.TryParse(Strid.Substring(1, 1), out int couleur);
            int.TryParse(Strid.Substring(0, 1), out int backgroundcolor);
            couleur = couleur * 10;
            backgroundcolor = backgroundcolor * 100;

            return new Tuile(forme, couleur, backgroundcolor, ID);
        }
        public static Tuile[] CreateAll() /* Créer toutes les tuiles d'un coup et les range dans la liste sacoche. */
        {
            int centaine, dizaine, unite, ID;
            Tuile[] Sacoche = new Tuile[367];
            for (centaine = 100; centaine <= 300; centaine = centaine + 100)    /* Les valeurs des attributs des objets tuiles deviennent les indices des 3 boucles POUR. */
            {
                for (dizaine = 10; dizaine <= 60; dizaine = dizaine + 10)
                {
                    for (unite = 1; unite <= 6; unite++)
                    {
                        ID = centaine + dizaine + unite;
                        Sacoche[ID] = new Tuile(unite, dizaine, centaine, ID);
                    }

                }
            }
            return Sacoche; /* On retourne la liste qui contient toutes les tuiles dont on aura besoin dans la partie. */

        }
    }
}
