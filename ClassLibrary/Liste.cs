﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public abstract class Liste
    {
        protected List<Tuile> tabtuil;

        public Liste(Tuile[] tabtuile)
        {

        }

        public Liste(List<Tuile> tab)
        {
            this.tabtuil = tab;
        }

        public List<Tuile> Getlist()
        {
            return tabtuil;
        }

        public void Setlist(List<Tuile> tab)
        {
            this.tabtuil = tab;
        }


        /*
         Getcoortuile permet d'avoir la coordonnée de la tuile entrée en paramètre
         */

        public int getcoortuile(Tuile tuile)
        {
            int cpt;
            cpt = tabtuil.Count;
            int resultat = 0;
            for (int cpt1 = 0; cpt1 < cpt; cpt1++)
            {
                if (tabtuil[cpt1] == tuile)
                {
                    resultat = cpt1;
                }

            }
            return resultat;
        }

        /*
         SetTuile permet de rajouter des tuiles d'une liste de tuile dans une autre liste
         */

        public void Settuile(List<Tuile> tab)
        {
            foreach (Tuile tuile in tab)
            {
                tabtuil.Add(tuile);
            }
        }
    }
}
