﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Joueur
    {
        /* La classe Joueur correspond aux joueurs du Qwirkle, elle possède 4 attributs : */
        private int Priorite;   /* La priorité définit qui est le joueur qui joue en 1er, en respectant les règles du Qwirkle. */ 
        private int PtJoueur;   /* Correspond au nombre du points du Joueur. */
        private string nom;     /* Permet au jouer de définir leur pseudo. */
        private MainJ Mainj;    /* Chaque joueur possède une main composé de maximum 6 tuiles. */

        public Joueur(int Priorite, int PtJoueur, string nom, List<Tuile> Main) /* Constructeur de la classe. */
        {
            this.Priorite = Priorite;
            this.PtJoueur = PtJoueur;
            Mainj = new MainJ(Main);
            this.nom = nom;
        }

        public void SetPtJoueur(int PtJoueur)
        {
            this.PtJoueur = PtJoueur;
        }

        public int GetPtJoueur()
        {
            return this.PtJoueur;
        }

        public void SetPriorite(int Priorite)
        {
            this.Priorite = Priorite;
        }

        public int GetPriorite()
        {
            return this.Priorite;
        }
        public string Getnom()
        {
            return this.nom;
        }
        public void Setnom(string nom)
        {
            this.nom = nom;
        }
        /* La Main du joueur est une liste composé d'objet de type tuile. */
        public void SetMain(List<Tuile> Main) 
        {
            Mainj = new MainJ(Main);
        }

        public List<Tuile> GetMain()
        {
            return Mainj.Getlist();
        }

        public Tuile Getmainpos(int pos) /* Permet de récupérer la tuile situé à la position pos dans la main. */
        {
            List<Tuile> list;
            list = Mainj.Getlist();         /* On créer une liste intermédiaire que l'on manipulera. */
            Tuile tuile = list[(pos - 1)];  /* Pour le joueur, sa main dispose de 6 cases (de 1 à 6), or une liste dispode de 6 cases mais qui vont de 0 à 5, on rajoute donc "-1" pour faire la conversion. */ 
            return tuile;                   /* On renvoit la tuile souhaité. */
        }
        public void Setmainpos(int pos, Tuile tuile) /* Fonctionne comme Getmainpos sauf que l'on écrase la tuile à l'emplacement souhaité, donc on ne retourne rien. */
        {
            List<Tuile> list = Mainj.Getlist();
            list.Insert(pos - 1, tuile);
            Mainj = new MainJ(list);
        }
        public void Newpoints(int turnpoints, Joueur joueur) /* Prend le score du joueur et y ajoute les points effectué ce tour pour calculer son nouveau score. */
        {
            int oldpoints = joueur.GetPtJoueur();
            joueur.SetPtJoueur(oldpoints + turnpoints);
        }

    }
}
