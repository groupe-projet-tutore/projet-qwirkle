﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Tuile
    {
        /* Cette classe correspond aux tuiles qui seront manipuler tous au long du jeu. Elle possède 4 attributs :  */
        private int forme;      /* Représente la forme sur la tuile par un entier entre 1 et 6 (Voir notre rapport pour les correspondances). */
        private int couleur;    /* Représente la couleur sur la tuile par un entier entre 1 et 6 multiplié par 10. */
        private int backgroundcolor;    /* Représente la couleur de fond de la tuile par un entier entre 1 et 6 multiplié par 100. */
        private int ID;         /* Nous permet d'identifier une tuile et de récupérer facilement ses attributs. ID = forme + couleur + backgroundcolor */

        public Tuile(int forme, int couleur, int backgroundcolor, int ID) /* Constructeur de la classe */
        {
            this.forme = forme;
            this.couleur = couleur;
            this.backgroundcolor = backgroundcolor;
            this.ID = ID;
        }

        public int Getforme()
        {
            return this.forme;
        }
        public void Setforme(int forme)
        {
            this.forme = forme;
        }
        public int Getcouleur()
        {
            return this.couleur;
        }
        public void Setcouleur(int couleur)
        {
            this.couleur = couleur;
        }
        public int Getbackgroundcolor()
        {
            return this.backgroundcolor;
        }
        public void Setbackgroundcolor(int backgroundcolor)
        {
            this.backgroundcolor = backgroundcolor;
        }
        public int GetID()
        {
            return this.ID;
        }
        public void SetID(int ID)
        {
            this.ID = ID;
        }
    }
}
