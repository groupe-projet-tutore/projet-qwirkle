﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class MainJ : Pioche
    {
        private List<Tuile> Tabrenvoie;
        private int TuilesManquantes;

        /*
         ce constructeur permet de créer, à partir d'un tableau une instance de la classe MainJ
         le constructeur renvoie la création de la liste de tuiel à la pioche
         */

        public MainJ(List<Tuile> liste) : base(liste)
        {
            this.Tabrenvoie = new List<Tuile>();
        }

        public MainJ(Tuile[] tabtuil) : base(tabtuil)
        {
            this.Tabrenvoie = new List<Tuile>();
        }

        // Comme son nom l'indique,cela permet d'ajouter une tuile à la liste Tabrenvoie qui va être utilisée pour renvoyer les tuiles non voulues

        public void Augment_tabrenvoie(Tuile tuile)
        {
            Tabrenvoie.Add(tuile);
        }

        /* 
        Renvoie_tuiles permet, avec une pioche en paramètre, de renvoyer un tableau de tuile préalablement rempli. On fait aussi appelle à la méthode "MelangePioche"
        de la classe pioche qui permet de rajouter les tuiles que l'on a envoyé à la pioche. On supprime ensuite au préalable les tuiles de tabrenvoie et de tabtuil
        */

        public void Renvoie_tuiles(Pioche pioche)
        {
            TuilesManquantes = Tabrenvoie.Count;
            pioche.MelangePioche(Tabrenvoie);
            foreach (Tuile tuile in Tabrenvoie)
            {
                tabtuil.Remove(tuile);
                Tabrenvoie.Remove(tuile);
            }
        }

        /* 
         Cette méthode, Recup_tuiles, permet, tout simplement, de piocher pour récupérer le nombre de tuiles qui nous manque 
         */

        public void Recup_tuiles(Pioche pioche)
        {
            this.Tabrenvoie = pioche.Piocher(tabtuil);
            foreach (Tuile tuile in Tabrenvoie)
            {
                this.tabtuil.Add(tuile);
            }
            TuilesManquantes = 0;
        }

        /*
        La fonction Placement_Tuile permet de palcer une tuile sur le plateau de jeu. En faite, elle fait appel à la méthode "Placement" de plateau qui renvoie un booléen
        Ce booléen est à True si le placement est fait sur le plateau
        Il est à false si cela n'est pas possible
        Et si c'est possible, nous retirons la tuile de la liste
        */

        public void Placement_Tuile(Plateau plateau, Tuile tuile, int x, int y)
        {
            bool test;
            test = plateau.placement(tuile, x, y);
            if (test == true)
            {
                this.tabtuil.Remove(tuile);
            }
        }

        /* retourne tout simplement la tuile à l'entier rentré en paramètre */

        public Tuile GetTuile(int colonne)
        {
            return tabtuil[colonne];
        }

        public List<Tuile> GetTabRenvoie()
        {
            return Tabrenvoie;
        }
    }
}
