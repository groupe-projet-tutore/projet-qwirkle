﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ClassLibrary;
using System.Collections.Generic;
using System.Text;

namespace QwirkleTest
{
    [TestClass]
    public class TestJoueur
    {
        [TestMethod]
        public void TestConstructeurJoueur()
        {
            Tuile t122 = new Tuile(2, 20, 100, 122);
            Tuile t234 = new Tuile(4, 30, 200, 234);
            List<Tuile> tab = new List<Tuile> { t122, t234 };
            Joueur j1 = new Joueur(1, 0,"Yohann", tab);

            Assert.AreEqual(1, j1.GetPriorite());
            Assert.AreEqual(0, j1.GetPtJoueur());
            Assert.AreEqual(tab, j1.GetMain());
        }

        [TestMethod]
        public void TestsetPtJoueur()
        {
            Tuile t122 = new Tuile(2, 20, 100, 122);
            Tuile t234 = new Tuile(4, 30, 200, 234);
            List<Tuile> tab = new List<Tuile> { t122, t234 };
            Joueur j1 = new Joueur(1, 0,"Yohann", tab);
            j1.SetPtJoueur(80);
            Assert.AreNotEqual(0, j1.GetPtJoueur());
            Assert.AreEqual(80, j1.GetPtJoueur());
        }

        [TestMethod]
        public void TestsetPriorite()
        {
            Tuile t122 = new Tuile(2, 20, 100, 122);
            Tuile t234 = new Tuile(4, 30, 200, 234);
            List<Tuile> tab = new List<Tuile> { t122, t234 };
            Joueur j1 = new Joueur(2, 0,"Yohann", tab);
            j1.SetPriorite(1);
            Assert.AreEqual(1, j1.GetPriorite());
        }

        [TestMethod]
        public void TestSetnom()
        {
            Tuile t122 = new Tuile(2, 20, 100, 122);
            Tuile t234 = new Tuile(4, 30, 200, 234);
            List<Tuile> tab = new List<Tuile> { t122, t234 };
            Joueur j1 = new Joueur(2, 0,"Yohann", tab);
            j1.Setnom("Louis");
            Assert.AreEqual("Louis", j1.Getnom());
            Assert.AreNotEqual("Yohann", j1.Getnom());
        }

        [TestMethod]
        public void TestsetMain()
        {
            Tuile t122 = new Tuile(2, 20, 100, 122);
            Tuile t234 = new Tuile(4, 30, 200, 234);
            Tuile t121 = new Tuile(1, 20, 100, 121);
            List<Tuile> tab = new List<Tuile> { t122, t234 };
            Joueur j1 = new Joueur(1, 0,"Yohann", tab);
            Assert.AreEqual(tab, j1.GetMain());
            List<Tuile> tab2 = new List<Tuile> { t122, t234, t121 };
            j1.SetMain(tab2);
            Assert.AreEqual(tab2, j1.GetMain());
        }

        [TestMethod]
        public void TestGetmainpos()
        {
            Tuile t121 = new Tuile(1, 20, 100, 121);
            Tuile t122 = new Tuile(2, 20, 100, 122);
            List<Tuile> list = new List<Tuile>();
            list.Add(t121);
            list.Add(t122);
            Joueur j1 = new Joueur(1, 0, "Moi",list);

            Assert.AreEqual(t122.GetID(),j1.Getmainpos(2).GetID());
        }

        [TestMethod]
        public void TestSetmainpos()
        {
            Tuile t121 = new Tuile(1, 20, 100, 121);
            Tuile t122 = new Tuile(2, 20, 100, 122);
            Tuile t123 = new Tuile(3, 20, 100, 123);
            List<Tuile> list = new List<Tuile>();
            list.Add(t121);
            list.Add(t122);
            Joueur j1 = new Joueur(1, 0, "Moi", list);
            j1.Setmainpos(3, t123);
            Assert.AreEqual(t123.GetID(), j1.Getmainpos(3).GetID());
            j1.Setmainpos(2, t123);
            Assert.AreEqual(t123.GetID(), j1.Getmainpos(2).GetID());
        }
        [TestMethod]
        public void TestNewscore()
        {
            Tuile t121 = new Tuile(1, 20, 100, 121);
            Tuile t122 = new Tuile(2, 20, 100, 122);
            List<Tuile> list = new List<Tuile>();
            list.Add(t121);
            list.Add(t122);
            Joueur j1 = new Joueur(1, 0, "Moi", list);
            int turnpoints = 5;
            j1.Newpoints(turnpoints, j1);
            Assert.AreEqual(5, j1.GetPtJoueur());
            turnpoints = 10;
            j1.Newpoints(turnpoints,j1);
            Assert.AreEqual(15, j1.GetPtJoueur());
            Assert.AreNotEqual(5, j1.GetPtJoueur());
        }
    }
}
