﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ClassLibrary;
namespace QwirkleTest
{
    /// <summary>
    /// Description résumée pour Testtuiles
    /// </summary>
    [TestClass]
    public class Testtuiles
    {
        [TestMethod]
        public void TestTuile()
        {
            Tuile t122 = new Tuile(2, 20, 100, 122);
            Tuile t234 = new Tuile(4, 30, 200, 234);

            Assert.AreEqual(2, t122.Getforme());
            Assert.AreEqual(20, t122.Getcouleur());
            Assert.AreEqual(100, t122.Getbackgroundcolor());
            Assert.AreEqual(122, t122.GetID());
            Assert.AreNotEqual(5, t122.Getforme());
            Assert.AreNotEqual(1, t122.Getcouleur());
            Assert.AreNotEqual(3000, t122.Getbackgroundcolor());
            Assert.AreNotEqual(0, t122.GetID());
            Assert.AreEqual(4, t234.Getforme());
            Assert.AreEqual(30, t234.Getcouleur());
            Assert.AreEqual(200, t234.Getbackgroundcolor());
            Assert.AreEqual(234, t234.GetID());
        }
        [TestMethod]
        public void TestCreateTuile()
        {
            Tuile t122 = Laboratoire.CreateTuile(122);
            Tuile t165 = Laboratoire.CreateTuile(165);
            Tuile t251 = Laboratoire.CreateTuile(251);
            Tuile t312 = Laboratoire.CreateTuile(312);

            Assert.AreEqual(2, t122.Getforme());
            Assert.AreEqual(20, t122.Getcouleur());
            Assert.AreEqual(100, t122.Getbackgroundcolor());
            Assert.AreEqual(122, t122.GetID());
            Assert.AreNotEqual(5, t122.Getforme());
            Assert.AreNotEqual(1, t122.Getcouleur());
            Assert.AreNotEqual(3000, t122.Getbackgroundcolor());
            Assert.AreNotEqual(0, t122.GetID());
            Assert.AreEqual(5, t165.Getforme());
            Assert.AreEqual(60, t165.Getcouleur());
            Assert.AreEqual(100, t165.Getbackgroundcolor());
            Assert.AreEqual(165, t165.GetID());
            Assert.AreEqual(1, t251.Getforme());
            Assert.AreEqual(50, t251.Getcouleur());
            Assert.AreEqual(200, t251.Getbackgroundcolor());
            Assert.AreEqual(251, t251.GetID());
            Assert.AreEqual(2, t312.Getforme());
            Assert.AreEqual(10, t312.Getcouleur());
            Assert.AreEqual(300, t312.Getbackgroundcolor());
            Assert.AreEqual(312, t312.GetID());
        }
        [TestMethod]
        public void TestCreateAll()
        {
            Tuile[] Sacoche = Laboratoire.CreateAll();

            /* Il faut aller chercher ou est stocké t122 dans la liste*/

            Assert.AreEqual(366, Sacoche[366].GetID());
            Assert.AreNotEqual(365, Sacoche[366]);
            bool test = false;
            try
            {
                Sacoche[477].Getbackgroundcolor();
            } catch (Exception e)
            {
                if (e.GetType().IsInstanceOfType(new IndexOutOfRangeException()))
                {
                    test = true;
                }
            } finally
            {
                if (test == false)
                {
                    throw new AssertFailedException();
                }
            }

            Assert.AreEqual(366, Sacoche[366].GetID());
            Assert.AreEqual(2, Sacoche[122].Getforme());
            Assert.AreEqual(20, Sacoche[122].Getcouleur());
            Assert.AreEqual(100, Sacoche[122].Getbackgroundcolor());
            Assert.AreEqual(122, Sacoche[122].GetID());
            Assert.AreNotEqual(5, Sacoche[122].Getforme());
            Assert.AreNotEqual(1, Sacoche[122].Getcouleur());
            Assert.AreNotEqual(3000, Sacoche[122].Getbackgroundcolor());
            Assert.AreNotEqual(0, Sacoche[122].GetID());
        }
        [TestMethod]
        public void TestSetforme()
        {
            Tuile t122 = new Tuile(2, 200, 100, 122);
            t122.Setforme(3);

            Assert.AreEqual(3, t122.Getforme());
            Assert.AreNotEqual(4, t122.Getforme());
        }
        [TestMethod]
        public void TestSetcouleur()
        {
            Tuile t122 = new Tuile(2, 20, 100, 20);
            t122.Setcouleur(60);

            Assert.AreEqual(60, t122.Getcouleur());
            Assert.AreNotEqual(20, t122.Getcouleur());
        }
        [TestMethod]
        public void TestSetbackgroundcolor()
        {
            Tuile t122 = new Tuile(2, 20, 100, 122);
            t122.Setbackgroundcolor(200);

            Assert.AreEqual(200, t122.Getbackgroundcolor());
            Assert.AreNotEqual(100, t122.Getbackgroundcolor());
        }
        [TestMethod]
        public void TestSetID()
        {
            Tuile t122 = new Tuile(2, 20, 100, 122);
            t122.SetID(333);

            Assert.AreEqual(333, t122.GetID());
            Assert.AreNotEqual(3333, t122.GetID());
        }
    }
}
