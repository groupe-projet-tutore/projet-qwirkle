﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ClassLibrary; 
using System.Collections.Generic;

namespace QwirkleTest
{
    [TestClass]
    public class TestPartie
    {
        [TestMethod]
        public void TestQuicommence2j()
        {
            Partie P1 = new Partie();
            Tuile t122 = new Tuile(2, 20, 100, 122);
            Tuile t132 = new Tuile(2, 30, 100, 132);
            Tuile t112 = new Tuile(2, 10, 100, 112);
            Tuile t142 = new Tuile(2, 40, 100, 142);
            Tuile t152 = new Tuile(2, 50, 100, 152);
            Tuile t162 = new Tuile(2, 60, 100, 162);
            List<Tuile> tabj1 = new List<Tuile> { t122, t132,t112,t142,t152,t162 };
            Joueur j1 = new Joueur(3, 0, "Yohann", tabj1);
            Tuile t121 = new Tuile(1, 20, 100, 121);
            Tuile t131 = new Tuile(1, 30, 100, 131);
            Tuile t111 = new Tuile(1, 10, 100, 111);
            Tuile t145 = new Tuile(5, 40, 100, 145);
            Tuile t153 = new Tuile(3, 50, 100, 153);
            Tuile t163 = new Tuile(3, 60, 100, 163);
            List<Tuile> tabj2 = new List<Tuile> { t121, t131, t111, t145, t153, t163 };
            Joueur j2 = new Joueur(75, 0, "Clément", tabj2);
            P1.Quicommence2j(j1,j2);
            Assert.AreEqual(1, j1.GetPriorite());
            Assert.AreEqual(2, j2.GetPriorite());
            Assert.AreNotEqual(75, j2.GetPriorite());
        }
        [TestMethod]
        public void TestQuicommence3j()
        {
            Partie P1 = new Partie();
            Tuile t122 = new Tuile(2, 20, 100, 122);
            Tuile t132 = new Tuile(2, 30, 100, 132);
            Tuile t112 = new Tuile(2, 10, 100, 112);
            Tuile t142 = new Tuile(2, 40, 100, 142);
            Tuile t152 = new Tuile(2, 50, 100, 152);
            Tuile t162 = new Tuile(2, 60, 100, 162);
            List<Tuile> tabj1 = new List<Tuile> { t122, t132, t112, t142, t152, t162 };
            Joueur j1 = new Joueur(3, 0, "Yohann", tabj1);
            Tuile t121 = new Tuile(1, 20, 100, 121);
            Tuile t131 = new Tuile(1, 30, 100, 131);
            Tuile t111 = new Tuile(1, 10, 100, 111);
            Tuile t145 = new Tuile(5, 40, 100, 145);
            Tuile t153 = new Tuile(3, 50, 100, 153);
            Tuile t163 = new Tuile(3, 60, 100, 163);
            List<Tuile> tabj2 = new List<Tuile> { t121, t131, t111, t145, t153, t163 };
            Joueur j2 = new Joueur(75, 0, "Clément", tabj2);
            Tuile t124 = new Tuile(4, 20, 100, 121);
            Tuile t134 = new Tuile(4, 30, 100, 131);
            Tuile t116 = new Tuile(6, 10, 100, 111);
            Tuile t144 = new Tuile(4, 40, 100, 145);
            Tuile t154 = new Tuile(4, 50, 100, 153);
            Tuile t161 = new Tuile(1, 60, 100, 163);
            List<Tuile> tabj3 = new List<Tuile> { t124, t134, t116, t144, t154, t161 };
            Joueur j3 = new Joueur(75, 0, "Louis", tabj3);
            P1.Quicommence3j(j1, j2, j3);
            Assert.AreEqual(1, j1.GetPriorite());
            Assert.AreEqual(2, j2.GetPriorite());
            Assert.AreEqual(3, j3.GetPriorite());
            Assert.AreNotEqual(75, j2.GetPriorite());
        }
        [TestMethod]
        public void TestQuicommence4j()
        {
            Partie P1 = new Partie();
            Tuile t122 = new Tuile(2, 20, 100, 122);
            Tuile t132 = new Tuile(2, 30, 100, 132);
            Tuile t112 = new Tuile(2, 10, 100, 112);
            Tuile t142 = new Tuile(2, 40, 100, 142);
            Tuile t152 = new Tuile(2, 50, 100, 152);
            Tuile t162 = new Tuile(2, 60, 100, 162);
            List<Tuile> tabj1 = new List<Tuile> { t122, t132, t112, t142, t152, t162 };
            Joueur j1 = new Joueur(3, 0, "Yohann", tabj1);
            Tuile t121 = new Tuile(1, 20, 100, 121);
            Tuile t131 = new Tuile(1, 30, 100, 131);
            Tuile t111 = new Tuile(1, 10, 100, 111);
            Tuile t145 = new Tuile(5, 40, 100, 145);
            Tuile t153 = new Tuile(3, 50, 100, 153);
            Tuile t163 = new Tuile(3, 60, 100, 163);
            List<Tuile> tabj2 = new List<Tuile> { t121, t131, t111, t145, t153, t163 };
            Joueur j2 = new Joueur(75, 0, "Clément", tabj2);
            Tuile t124 = new Tuile(4, 20, 100, 121);
            Tuile t134 = new Tuile(4, 30, 100, 131);
            Tuile t116 = new Tuile(6, 10, 100, 111);
            Tuile t144 = new Tuile(4, 40, 100, 145);
            Tuile t154 = new Tuile(4, 50, 100, 153);
            Tuile t161 = new Tuile(1, 60, 100, 163);
            List<Tuile> tabj3 = new List<Tuile> { t124, t134, t116, t144, t154, t161 };
            Joueur j3 = new Joueur(1, 0, "Louis", tabj3);
            Tuile t126 = new Tuile(6, 20, 100, 121);
            Tuile t133 = new Tuile(3, 30, 100, 131);
            Tuile t113 = new Tuile(3, 10, 100, 111);
            Tuile t141 = new Tuile(1, 40, 100, 145);
            Tuile t151 = new Tuile(1, 50, 100, 153);
            Tuile t166 = new Tuile(6, 60, 100, 163);
            List<Tuile> tabj4 = new List<Tuile> { t126, t133, t113, t141, t151, t166 };
            Joueur j4 = new Joueur(0, 0, "Théo", tabj4);
            P1.Quicommence4j(j1, j2, j3, j4);
            Assert.AreEqual(1, j1.GetPriorite());
            Assert.AreEqual(2, j2.GetPriorite());
            Assert.AreEqual(3, j3.GetPriorite());
            Assert.AreEqual(4, j4.GetPriorite());
            Assert.AreNotEqual(75, j2.GetPriorite());
        }
    }
}
