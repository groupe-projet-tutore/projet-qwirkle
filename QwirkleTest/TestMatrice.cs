﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ClassLibrary;
using System.Collections.Generic;

namespace QwirkleTest
{
    [TestClass]
    public class TestMatrice
    {
        [TestMethod]
        public void TestCréaMatrice()
        {
            Tuile t121 = new Tuile(1, 20, 100, 121);
            List<Tuile> tab = new List<Tuile>();
            tab.Add(t121);
            Liste mat = new MainJ(tab);
            Assert.AreEqual(tab, mat.Getlist());
        }

        [TestMethod]
        public void TestSetMatrice()
        {
            Tuile t121 = new Tuile(1, 20, 100, 121);
            Tuile t122 = new Tuile(2, 20, 100, 122);
            Tuile t123 = new Tuile(3, 20, 100, 123);
            List<Tuile> tab = new List<Tuile>();
            tab.Add(t121);
            tab.Add(t122);
            Liste mat = new Pioche(tab);
            List<Tuile> tab2 = new List<Tuile>();
            tab2.Add(t123);
            mat.Setlist(tab2);
            Assert.AreEqual(tab2, mat.Getlist());
        }

        [TestMethod]
        public void TestGetCoorTuiles()
        {
            Tuile t121 = new Tuile(1, 20, 100, 121);
            Tuile t122 = new Tuile(2, 20, 100, 122);
            List<Tuile> tab = new List<Tuile>();
            tab.Add(t121);
            tab.Add(t122);
            Liste mat = new MainJ(tab);
            Assert.AreEqual(1, mat.getcoortuile(t122));
        }

        [TestMethod]
        public void TestSetTuile()
        {
            Tuile t121 = new Tuile(1, 20, 100, 121);
            Tuile t122 = new Tuile(2, 20, 100, 122);
            Tuile t123 = new Tuile(3, 20, 100, 123);
            List<Tuile> tab = new List<Tuile>();
            tab.Add(t121);
            tab.Add(t122);
            Liste pioche = new Pioche(tab);
            List<Tuile> tab2 = new List<Tuile>();
            tab2.Add(t123);
            pioche.Settuile(tab2);
            List<Tuile> list = new List<Tuile>();
            list.Add(t121);
            list.Add(t122);
            list.Add(t123);
            Pioche resu = new Pioche(list);
            Assert.AreEqual(resu, pioche);
        }

        [TestMethod]
        public void Testtabagrandir()
        {
            Tuile[,] tab = new Tuile[10, 10];

            Plateau plateau = new Plateau(tab);
            plateau.TabAgrandir();
            Assert.AreEqual(11, plateau.GetTailletab());
        }

        [TestMethod]
        public void TestPiocher()
        {
            Tuile t121 = new Tuile(1, 20, 100, 121);
            Tuile t122 = new Tuile(2, 20, 100, 122);
            Tuile t123 = new Tuile(3, 20, 100, 123);
            Tuile t124 = new Tuile(1, 20, 100, 124);
            Tuile t125 = new Tuile(2, 20, 100, 125);
            Tuile t126 = new Tuile(3, 20, 100, 126);
            List<Tuile> tab = new List<Tuile>();
            tab.Add(t121);
            tab.Add(t122);
            tab.Add(t123);
            tab.Add(t124);
            tab.Add(t125);
            MainJ main = new MainJ(tab);
            List<Tuile> pioch = new List<Tuile>();
            pioch.Add(t126);

            List<Tuile> resu = new List<Tuile>();
            resu.Add(t121);
            resu.Add(t122);
            resu.Add(t123);
            resu.Add(t124);
            resu.Add(t125);
            resu.Add(t126);
            MainJ resul = new MainJ(resu);
            Pioche pioche = new Pioche(pioch);
            main.Recup_tuiles(pioche);
            Assert.AreEqual(resul.Getlist(), main.Getlist());
        }

        [TestMethod]
        public void TestNbrTuilesPioche()
        {
            Tuile t121 = new Tuile(1, 20, 100, 121);
            Tuile t122 = new Tuile(2, 20, 100, 122);
            List<Tuile> tab = new List<Tuile>();
            tab.Add(t121);
            tab.Add(t122);

            //Pioche pioche2 = new Pioche(tab);
            Pioche pioche = new Pioche(tab);
            //pioche2.Piocher(1);
            Assert.AreEqual(2, pioche.Nbtuilespioche());
            //Assert.AreEqual(1, pioche2.Nbtuilespioche());
        }

        [TestMethod]
        public void TestMelangePioche()
        {
            Tuile t121 = new Tuile(1, 20, 100, 121);
            Tuile t122 = new Tuile(2, 20, 100, 122);
            Tuile t123 = new Tuile(3, 20, 100, 123);
            List<Tuile> rajout = new List<Tuile>();
            rajout.Add(t121);
            rajout.Add(t122);
            List<Tuile> tab = new List<Tuile>();
            tab.Add(t123);

            Pioche pioche = new Pioche(tab);
            pioche.MelangePioche(rajout);
            Assert.AreEqual(3, pioche.Nbtuilespioche());
        }

        [TestMethod]
        public void TestRenvoie_Tuiles()
        {
            Tuile t121 = new Tuile(1, 20, 100, 121);
            Tuile t122 = new Tuile(2, 20, 100, 122);
            Tuile t123 = new Tuile(3, 20, 100, 123);
            List<Tuile> tab2 = new List<Tuile>();
            List<Tuile> tab = new List<Tuile>();
            List<Tuile> resu = new List<Tuile>();
            tab.Add(t121);
            tab.Add(t122);
            tab.Add(t123);
            resu = tab;
            Pioche pioche = new Pioche(tab2);
            MainJ mainj = new MainJ(tab);
            mainj.Augment_tabrenvoie(t121);
            mainj.Renvoie_tuiles(pioche);
            Assert.AreEqual(resu, mainj.Getlist());

        }

        [TestMethod]
        public void TestRecup_Tuiles()
        {
            Tuile t121 = new Tuile(1, 20, 100, 121);
            Tuile t122 = new Tuile(2, 20, 100, 122);
            Tuile t123 = new Tuile(3, 20, 100, 123);
            Tuile t124 = new Tuile(4, 20, 100, 124);
            Tuile t125 = new Tuile(5, 20, 100, 125);
            Tuile t126 = new Tuile(6, 20, 100, 126);
            List<Tuile> tab = new List<Tuile>();
            tab.Add(t121);
            tab.Add(t122);
            tab.Add(t123);
            MainJ mainj = new MainJ(tab);
            List<Tuile> resul = new List<Tuile>();
            resul.Add(t121);
            resul.Add(t122);
            resul.Add(t123);
            resul.Add(t124);
            resul.Add(t125);
            resul.Add(t126);
            MainJ resu = new MainJ(resul);
            Pioche pioche = new Pioche(resul);


            mainj.Recup_tuiles(pioche);
            Assert.AreEqual(resu.Getlist(), mainj.Getlist());
        }

        [TestMethod]
        public void TestPlacement()
        {
            Tuile t121 = new Tuile(1, 20, 100, 121);
            Tuile t122 = new Tuile(2, 20, 100, 122);
            Tuile t123 = new Tuile(3, 20, 100, 123);
            Tuile[,] tab = new Tuile[10, 10];
            Plateau plateau = new Plateau(tab);
            List<Tuile> list = new List<Tuile>();
            list.Add(t121);
            list.Add(t122);
            list.Add(t123);
            MainJ main = new MainJ(list);
            main.Placement_Tuile(plateau, t121, 1, 1);
            main.Placement_Tuile(plateau, t122, 5, 5);
            main.Placement_Tuile(plateau, t123, 1, 1);
            Tuile[,] resu = new Tuile[10, 10];
            resu[1, 1] = t121;
            resu[5, 5] = t122;


            Assert.AreEqual(resu, plateau.GetTab());
        }

        [TestMethod]
        public void TestGetCoordTuilePlateau()
        {
            Tuile[,] tab = new Tuile[10, 10];
            Plateau plateau = new Plateau(tab);
            Tuile t121 = new Tuile(1, 20, 100, 121);
            List<Tuile> list = new List<Tuile>();
            list.Add(t121);
            MainJ main = new MainJ(list);
            main.Placement_Tuile(plateau, t121, 2, 2);
            int[] tabl = { 121 };

            Assert.AreEqual(tabl[0], plateau.GetIDtuile(t121));
        }

    }
}
